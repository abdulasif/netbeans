/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package dao;

import java.sql.Connection;
import model.Customer;

/**
 *
 * @author bas200190
 */
public interface CustomerDAO {

    public void addCustomer(Connection connection , Customer customer);

    public void updateCustomer(Connection connection);

    public void viewAllCustomer(Connection connection);

    public void viewParticularCustomer(Connection connection);

    public  int setCustomerId(Connection connection);

}
