
public class StringProblems {

    static int countWords(String str) {
        int count = 1;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == ' ') {
                count++;
            }
        }
        return count;
    }

    static int calculator(int a, char oper, int b) {
        int ans = 0;
        if (oper == '+') {
            ans = a + b;
        } else if (oper == '-') {
            ans = a - b;
        } else if (oper == '*') {
            ans = a * b;
        } else if (oper == '/') {
            ans = a / b;
        } else {
            System.out.println("please enter the correct operator like '+' , '-' , '*' , '/' ");

        }
        return ans;
    }

    static boolean checkEnding(String str1, String str2) {
        boolean ans = true;
        int j = 0;
        for (int i = (str1.length() - str2.length()); i < str1.length(); i++) {
            if (str1.charAt(i) != str2.charAt(j)) {
                ans = false;
            }
            j++;
        }
        return ans;
    }

    static String nameString(String str) {
        String result = str + "programming";
        return result;
    }

    static String nameShuffle(String str) {
        String first_name = "";
        String last_name = "";
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == ' ') {
                for (int j = 0; j < i; j++) {
                    first_name = first_name + str.charAt(j);
                }
                for (int k = i + 1; k < str.length(); k++) {
                    last_name = last_name + str.charAt(k);
                }
            }
        }
        String ans = last_name + " " + first_name;
        return ans;
    }

    static String fizzBuzz(int n) {
        String ans = "";
        if (n % 15 == 0) {
            ans = "FizzBuzz";
        } else if (n % 3 == 0) {
            ans = "Fizz";
        } else if (n % 5 == 0) {
            ans = "Buzz";
        } else {
            ans = "" + n;
        }
        return ans;
    }

    static String flipEndChars(String str) {
        String ans = "";
        String temp = "";
        if (str.length() > 2) {
            if (str.charAt(0) != str.charAt(str.length() - 1)) {
                temp = temp + str.charAt(str.length() - 1);
                for (int i = 1; i < str.length() - 1; i++) {

                    temp = temp + str.charAt(i);
                }
                temp = temp + str.charAt(0);
                ans = temp;
            } else {
                ans = "Two's a pair.";
            }
        } else {
            ans = "Incompatible.";
        }
        return ans;
    }

    static boolean isInOrder(String str) {
        boolean ans = true;
        for (int i = 1; i < str.length(); i++) {
            if ((str.charAt(i) - str.charAt(i - 1) < 0)) {
                ans = false;
            }
        }
        return ans;
    }
    
 /* static int asciiProblems(String str1)
    {
      int ans = str1.charAt(0)*str1.charAt(1);
      return ans;
    }*/

    public static void main(String[] args) {
        System.out.println(asciiProblems("ah"));
    }
}
