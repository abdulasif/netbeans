/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package daoMysql;

import dao.KhataBookDAO;

/**
 *
 * @author bas200190
 */
public class MySQLKhataBookDAOimpl implements KhataBookDAO{
    @Override
    public CustomerDAOimpl getCustomerDAOimpl(){
        return new CustomerDAOimpl();
    }
    
    @Override
    public ProductDAOimpl getProductDAOimpl(){
        return new ProductDAOimpl();
    }
    
    @Override
    public ItemListDAOimpl getItemListDAOimpl(){
        return new ItemListDAOimpl();
    }
    
    @Override
    public OrdersDAOimpl getOrdersDAOimpl(){
        return new OrdersDAOimpl();
    }
    
    @Override
    public PaymentDAOimpl getPaymentDAOimpl(){
        return new PaymentDAOimpl();
    }
    
    
    @Override
    public StatisticsDAOimpl getStatisticsDAOimpl(){
        return new StatisticsDAOimpl();
    }
}
