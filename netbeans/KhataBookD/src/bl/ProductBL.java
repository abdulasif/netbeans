/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bl;

import java.sql.Connection;
import dao.KhataBookDAO;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import model.Product;

/**
 *
 * @author bas200190
 */
public class ProductBL {

    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public static Product addProduct(KhataBookDAO kbd, Connection connection) {
        System.out.println("*****Enter Product Details*****");
        Product product = null;
        try {
            System.out.println("Enter Product ID:");
            int product_id = kbd.getProductDAOimpl().setProductId(connection);
            System.out.println("Enter Product Name:");
            String product_name = br.readLine();
            System.out.println("Enter Product Weight:");
            String product_weight = br.readLine();
            System.out.println("Enter Product Buying Price:");
            double product_buying_price = Double.parseDouble(br.readLine());
            System.out.println("Enter Product Selling Price:");
            double product_selling_price = Double.parseDouble(br.readLine());
            System.out.println("Enter Product Brand:");
            String product_brand = br.readLine();
            System.out.println("Enter Product Quantity:");
            int product_quantity = Integer.parseInt(br.readLine());

            product = new Product(product_id, product_name, product_weight, product_buying_price, product_selling_price, product_brand, product_quantity);
        } catch (IOException | NumberFormatException e) {

        }

        return product;

    }
}
