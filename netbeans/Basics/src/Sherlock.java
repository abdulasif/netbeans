
public class Sherlock {

    public static void main(String[] args) {
        String a = "aceaababcdcddabdbb";
        char arr[] = new char[a.length()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = a.charAt(i);
        }

        for (int i = 0; i < arr.length; i++) {      //printing char array
            System.out.print(arr[i] + " ");
        }
        System.out.println("");

        for (int i = 0;
                i < arr.length; i++) {         //sorting using ascii
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] > arr[j]) {
                    char temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }

        for (int i = 0; i < arr.length; i++) {           //printing sorted char array
            System.out.print(arr[i] + " ");
        }
        System.out.println("");

        int[] count = new int[0];
        int i = 0;
        int j = 0;
        //int chCount = 0;
        for (; i < arr.length; i = j) {
            int[] temp = count;
            count = new int[count.length + 1];
            for (int k = 0; k < temp.length; k++) {
                count[k] = temp[k];
            }

            for (j = i; j < arr.length; j++) {

                if (arr[i] == arr[j]) {

                    count[count.length - 1]++;
                } else {

                    break;
                }
            }

        }

        System.out.println("Printing the counts :");
        int n = 0;
        for (int k = 0; k < count.length; k++) {
            System.out.println(arr[n] + ": " + count[k]);
            n = n + count[k];
        }
        System.out.println("");

        int m = 0;
        for (int k = 0; k < count.length; k++) {
            System.out.println(arr[m] + ": " + count[k]);
            m = m + count[k];
        }
        System.out.println("");

//        for (int i = 0; i < count.length; i++) {           //printing sorted char array
//            System.out.print(count[i]+" ");
//        }
//        System.out.println("");
    }
}
