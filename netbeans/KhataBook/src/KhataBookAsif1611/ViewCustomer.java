/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhataBookAsif1611;


import java.io.IOException;
import java.util.Objects;

/**
 *
 * @author bas200190
 */
public class ViewCustomer {
    public void viewAllCustomer(){
        Design.viewCustomerDesign();
        for (int i = 0; i < KhataBook.ob.kd.customer_details.length; i++) {
            System.out.print(KhataBook.ob.kd.customer_details[i]);
            System.out.print("\n+" + "-".repeat(15)+"+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(25) + "+"+ "-".repeat(20) + "+" );
            
        }
        System.out.println("");
        System.out.println("");
        System.out.println("");
        
    }
    
    public void viewParticularCustomer(){
                  
        String vpc_customer_phoneNo = null;
        int vpc_customer_id  = 0;
        try{
        System.out.println("Enter the Customer's Phone Number:");
        vpc_customer_phoneNo = KhataBook.ob.br.readLine();
        System.out.println("Enter the Customer ID:");
        vpc_customer_id = Integer.parseInt(KhataBook.ob.br.readLine());
        }catch(IOException ioe){
            
        }
        
        for (int i = 0; i < KhataBook.ob.kd.customer_details.length; i++) {
            if ((Objects.nonNull(vpc_customer_phoneNo)) && (KhataBook.ob.kd.customer_details[i].customer_phoneNo).equals(vpc_customer_phoneNo) && (KhataBook.ob.kd.customer_details[i].customer_id==vpc_customer_id)) {
                Design.viewCustomerDesign();    //wrong but
                System.out.print(KhataBook.ob.kd.customer_details[i]);
                System.out.print("\n+"+ "-".repeat(15) +"+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(25) + "+"+ "-".repeat(20) + "+" );
                break;
            }else{
                System.out.println("This Customer is Not Exist");
            }
            
        }
        System.out.println("");
        System.out.println("");
        System.out.println("");
    }
}
