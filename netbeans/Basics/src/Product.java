
public class Product {

    public double price;
    String name;
    private boolean sale;

    {
          name = "Book";
        price = 285;
        sale = true;
    }

    public boolean onSale() {
        return sale;
    }

    @Override
    public String toString() {
        //return "Product[price: " + price + "]";
        return String.format("Product[Name: %s, Price: %f]", name, price);
    }
}

