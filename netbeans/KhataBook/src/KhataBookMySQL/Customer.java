/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhataBookMySQL;

/**
 *
 * @author bas200190
 */
public class Customer {

    private int customer_id;
    private String customer_name;
    private String customer_phoneNo;
    private String customer_address;
    double customer_debit;

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_phoneNo() {
        return customer_phoneNo;
    }

    public void setCustomer_phoneNo(String customer_phoneNo) {
        this.customer_phoneNo = customer_phoneNo;
    }

    public String getCustomer_address() {
        return customer_address;
    }

    public void setCustomer_address(String customer_address) {
        this.customer_address = customer_address;
    }

    public double getCustomer_debit() {
        return customer_debit;
    }

    public void setCustomer_debit(double customer_debit) {
        this.customer_debit = customer_debit;
    }

   
    public Customer(int customer_id, String customer_name, String customer_phoneNo, String customer_address, double customer_debit) {
        this.customer_id = customer_id;
        this.customer_name = customer_name;
        this.customer_phoneNo = customer_phoneNo;
        this.customer_address = customer_address;
        this.customer_debit = customer_debit;
    }

}
