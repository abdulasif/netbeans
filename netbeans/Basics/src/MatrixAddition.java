import java.util.Scanner;
public class MatrixAddition {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the 1st size of 2dimensional array:");
        int sizea  = sc.nextInt();
        System.out.println("Enter the 2nd size of 2dimensional array:");
        int sizeb = sc.nextInt();
        int[][] arr1 = new int[sizea][sizeb];
        int[][] arr2 = new int[sizea][sizeb];
        System.out.println("Enter the elements of 2d 'arr1' array:");
          for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr1[i].length; j++) {
              arr1[i][j]=sc.nextInt();
            }
          }
          System.out.println("arr1 array:");
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr1[i].length; j++) {
                System.out.print(arr1[i][j]+" ");
            }
            System.out.println("");
        }  
        System.out.println("Enter the elements of 2d 'arr2' array:");
          for (int i = 0; i < arr2.length; i++) {
            for (int j = 0; j < arr2[i].length; j++) {
              arr2[i][j]=sc.nextInt();
            }
          }
          System.out.println("arr2 array:");
         for (int i = 0; i < arr2.length; i++) {
            for (int j = 0; j < arr2[i].length; j++) {
                System.out.print(arr2[i][j]+" ");
            }
            System.out.println("");
        } 
        int[][] arr = new int[sizea][sizeb];  
        System.out.println("Addition of 'arr1' & 'arr2' :"); 
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr1[i].length; j++) {
              arr[i][j] = arr1[i][j]+arr2[i][j];
            }
        }
        //System.out.println("");
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j]+" ");
            }
            System.out.println("");
        }
    }
}
