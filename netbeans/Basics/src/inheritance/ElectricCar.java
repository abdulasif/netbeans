/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package inheritance;

/**
 *
 * @author bas200190
 */
public class ElectricCar extends Car{
    public ElectricCar(){
        super("tesla");
        System.out.println("ElectricCar()");
    }
}
