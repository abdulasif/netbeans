/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhatabookAsif1311;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;


/**
 *
 * @author bas200190
 */
public class AddCustomer {
    LocalDate customer_joining_date;
    int customer_id;
    String customer_name;
    String customer_phoneNo;
    String customer_aadharNo;
    String customer_address;
    double customer_debit;
     AddCustomer[] customer_details = new AddCustomer[3];
    
    public AddCustomer(){
        
    }
    
    public AddCustomer(int customer_id, String customer_name, String customer_phoneNo, String customer_aadharNo, String customer_address,double customer_debit){
 
        this.customer_joining_date = LocalDate.now();
        this.customer_id = customer_id;
        this.customer_name = customer_name;
        this.customer_phoneNo = customer_phoneNo;
        this.customer_aadharNo = customer_aadharNo;
        this.customer_address = customer_address;
        this.customer_debit = customer_debit;
    }
    
        public AddCustomer(LocalDate joining_date,int customer_id, String customer_name, String customer_phoneNo, String customer_aadharNo, String customer_address,double customer_debit){
 
        this.customer_joining_date = joining_date;
        this.customer_id = customer_id;
        this.customer_name = customer_name;
        this.customer_phoneNo = customer_phoneNo;
        this.customer_aadharNo = customer_aadharNo;
        this.customer_address = customer_address;
        this.customer_debit = customer_debit;
    }

    public void addCustomer(){
        try{
        customer_details = Arrays.copyOf(customer_details, customer_details.length + 1);
        
        System.out.println("*****Enter Customer Details*****");
        System.out.println("Enter the Customer Name:");
        String set_customer_name = KhataBook.ob.br.readLine();
        System.out.println("Enter the Phone Number:");
        String set_customer_phoneNo = KhataBook.ob.br.readLine();
        System.out.println("Enter the Aadhar Number:");
        String set_customer_aadharNo = KhataBook.ob.br.readLine();
        System.out.println("Enter the Customer Address:");
        String set_customer_address = KhataBook.ob.br.readLine();
        customer_details[customer_details.length-1] = new AddCustomer(customer_details.length+200, set_customer_name, set_customer_phoneNo, set_customer_aadharNo, set_customer_address,0);
        }
        catch(IOException ioe){
            
        }
        
    }
    
    public void updateCustomer(){
        try{
        System.out.println("Enter the Customer's Phone Number:");
        String uc_customer_phoneNo = KhataBook.ob.br.readLine();
        System.out.println("Enter the Customer's ID:");
        int uc_customer_id = Integer.parseInt(KhataBook.ob.br.readLine());
        for (int i = 0; i < customer_details.length; i++) {
            if ((customer_details[i].customer_phoneNo).equals(uc_customer_phoneNo) && (customer_details[i].customer_id) == uc_customer_id) {
                
                System.out.println("*****Enter Customer's New Details*****");
                System.out.println("Enter the Customer Name:");
                customer_details[i].customer_name= KhataBook.ob.br.readLine();
                System.out.println("Enter the Phone Number:");
                customer_details[i].customer_phoneNo= KhataBook.ob.br.readLine();
                System.out.println("Enter the Aadhar Number:");
                customer_details[i].customer_aadharNo = KhataBook.ob.br.readLine();
                System.out.println("Enter the Customer Address:");
                    customer_details[i].customer_address = KhataBook.ob.br.readLine();

            }
        }
        System.out.println("SUCCESSFULLY UPDATED CUSTOMER DETAILS....");
        } catch (IOException ioe) {

        }
    }
    
    
    public String toString() {
        return String.format("\n|%-15s|%-20d|%-20s|%-20s|%-20s|%-25s|%-20s|", this.customer_joining_date,this.customer_id, this.customer_name, this.customer_phoneNo, this.customer_aadharNo, this.customer_address,this.customer_debit);
    }
    
   
}
