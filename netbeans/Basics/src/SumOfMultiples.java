
import java.util.Arrays;

class SumOfMultiples {

    int number;
    int[] set;
    SumOfMultiples(int number, int[] set) {
        this.number = number;
        this.set = set;
    }

    int[] getSum() {
        //int ans = 0; 
        int count = 0;
        for (int i = 0; i < set.length; i++) {
            for (int j = 2; j < number; j++) {
                if (j%set[i]==0) {
                    //ans = ans + j;
                    count++;
                }
            }
        }
        int index = 0;
        int[] ans = new int[count];
        for (int i = 0; i < set.length; i++) {
            for (int j = 2; j < number; j++) {
                if (j%set[i]==0) {
                    ans[index]=j;
                    index++;
                }
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        int[] set = {3,5};
        SumOfMultiples ex1 = new SumOfMultiples(20,set);
        
        System.out.println(Arrays.toString(ex1.getSum()));
    }
}
