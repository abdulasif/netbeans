import java.util.Scanner;
public class ATranspose {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the 1st size of 2dimensional array:");
        int sizea  = sc.nextInt();
        System.out.println("Enter the 2nd size of 2dimensional array:");
        int sizeb = sc.nextInt();
        int[][]arr = new int[sizea][sizeb];
        System.out.println("Enter the elements of 2d 'arr' array:");
          for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
              arr[i][j]=sc.nextInt();
            }
          }
          System.out.println(sizea+" * "+sizeb+" matrix:");
          for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j]+" ");//876
            }
            System.out.println("");
        }
        int[][] arr1 = new int[sizeb][sizea];
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr1[i].length; j++) {
                arr1[i][j] = arr[j][i];
            }
        }
        System.out.println(sizeb+" * "+sizea+" matrix:");
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr1[i].length; j++) {
                System.out.print(arr1[i][j]+" ");
            }
            System.out.println("");
        }
        
    }
}
