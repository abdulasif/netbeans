/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhataBookAsif1611;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.time.LocalDate;
import java.util.Arrays;

/**
 *
 * @author bas200190
 */
public class FileReading {

    public void customerDetailsRead() {
        try (DataInputStream dis_read_customer_details = new DataInputStream(new FileInputStream("/home/bas200190/Documents/netbeans/KhataBook/src/KhataBookAsif1611/customer_details.txt"))) {

            while (true) {

                LocalDate customer_joining_date = LocalDate.parse(dis_read_customer_details.readUTF());
                int customer_id = dis_read_customer_details.readInt();
                String customer_name = dis_read_customer_details.readUTF();
                String customer_phoneNo = dis_read_customer_details.readUTF();
                String customer_aadharNo = dis_read_customer_details.readUTF();
                String customer_address = dis_read_customer_details.readUTF();
                double customer_debit = dis_read_customer_details.readDouble();
                KhataBook.ob.kd.customer_details = Arrays.copyOf(KhataBook.ob.kd.customer_details, KhataBook.ob.kd.customer_details.length + 1);
                KhataBook.ob.kd.customer_details[KhataBook.ob.kd.customer_details.length - 1] = new Customer(customer_joining_date, customer_id, customer_name, customer_phoneNo, customer_aadharNo, customer_address, customer_debit);
            }
        } catch (Exception e) {
            System.out.println("cus read complete");
        }
    }

    public void productDetailsRead() {

        try (DataInputStream dis_read_product_details = new DataInputStream(new FileInputStream("/home/bas200190/Documents/netbeans/KhataBook/src/KhataBookAsif1611/product_det.txt"))) {

            while (dis_read_product_details.available() > 0) {

                int product_id = dis_read_product_details.readInt();
                String product_name = dis_read_product_details.readUTF();
                String product_weight = dis_read_product_details.readUTF();
                double product_buying_price = dis_read_product_details.readDouble();
                double product_selling_price = dis_read_product_details.readDouble();
                String product_brand = dis_read_product_details.readUTF();
                int product_quantity = dis_read_product_details.readInt();
                KhataBook.ob.kd.product_details = Arrays.copyOf(KhataBook.ob.kd.product_details, KhataBook.ob.kd.product_details.length + 1);
                KhataBook.ob.kd.product_details[KhataBook.ob.kd.product_details.length - 1] = new Product(product_id, product_name, product_weight, product_buying_price, product_selling_price, product_brand, product_quantity);
            }
        } catch (Exception e) {
            System.out.println("prod read complete");
        }
    }

    public ItemList[] itemListDetailsRead(int match_order_id) {
        ItemList[] temp_item_list = new ItemList[0];
        try (DataInputStream dis_item_list_details = new DataInputStream(new FileInputStream("/home/bas200190/Documents/netbeans/KhataBook/src/KhataBookAsif1611/item_list_details.txt"))) {
            while (true) {        //dis_item_list_details.available() > 0
                int order_id = dis_item_list_details.readInt();
                int product_id = dis_item_list_details.readInt();
                int product_quantity = dis_item_list_details.readInt();
                double each_price = dis_item_list_details.readDouble();
                double price = dis_item_list_details.readDouble();

                if (match_order_id == order_id) {
                    temp_item_list = Arrays.copyOf(temp_item_list, temp_item_list.length + 1);
                    temp_item_list[temp_item_list.length - 1] = new ItemList(order_id, product_id, product_quantity, each_price, price);
                }
            }
        } catch (Exception e) {
            System.out.println("item list read");
        }

        return temp_item_list;
    }

    public void orderDetailsRead() {
        try (DataInputStream dis_order_details = new DataInputStream(new FileInputStream("/home/bas200190/Documents/netbeans/KhataBook/src/KhataBookAsif1611/order_details.txt"))) {
            while (dis_order_details.available() > 0) {
                LocalDate ordered_date = LocalDate.parse(dis_order_details.readUTF());
                int order_id = dis_order_details.readInt();
                int customer_id = dis_order_details.readInt();
                double total_price = dis_order_details.readDouble();

                ItemList[] item_list = itemListDetailsRead(order_id);

                KhataBook.ob.ord_det.order_details = Arrays.copyOf(KhataBook.ob.ord_det.order_details, KhataBook.ob.ord_det.order_details.length + 1);
                KhataBook.ob.ord_det.order_details[KhataBook.ob.ord_det.order_details.length - 1] = new Orders(ordered_date, order_id, customer_id, total_price, item_list);
            }

        } catch (Exception e) {
            System.out.println("order read complete");
        }
    }

    public void paymentDetailsRead() {
        try (DataInputStream dis_payment_details = new DataInputStream(new FileInputStream("/home/bas200190/Documents/netbeans/KhataBook/src/KhataBookAsif1611/payment_details.txt"));) {

            while (dis_payment_details.available() > 0) {

                LocalDate paid_date = LocalDate.parse(dis_payment_details.readUTF());
                int payment_id = dis_payment_details.readInt();
                int customer_id = dis_payment_details.readInt();
                double paid_amount = dis_payment_details.readDouble();

                KhataBook.ob.paydet.payment_details = Arrays.copyOf(KhataBook.ob.paydet.payment_details, KhataBook.ob.paydet.payment_details.length + 1);
                KhataBook.ob.paydet.payment_details[KhataBook.ob.paydet.payment_details.length - 1] = new PaymentDetails(paid_date, payment_id, customer_id, paid_amount);
            }
        } catch (Exception e) {
            System.out.println("payment read complete");
        }
    }
}
