
import java.util.Scanner;

public class MatrixRotation {

    public static int[][] rightRotation(int arr[][], int r_size, int c_size) {
        int b[][] = new int[r_size][c_size];
        int k = 0;
        for (int i = 0; i < b.length; i++) {                   //R
            for (int j = (b[i].length - 1); j >= 0; j--) {
                b[i][k] = arr[j][i];
                k++;
            }
            k = 0;
        }
        
        return b;
    }

    public static int[][] leftRotation(int arr[][], int r_size, int c_size) {
        int b[][] = new int[r_size][c_size];                    //L
        int l = b.length - 1;
        for (int i = 0; i < b.length; i++) {
            for (int j = 0; j < b[i].length; j++) {
                b[i][j] = arr[j][l];
            }
            l--;
        }

        return b;
    }
    @Override
    public int hashCode()
    {
      return 123;
    }

    public static void main(String[] args) {
        MatrixRotation m = new MatrixRotation();
        System.out.println(m.hashCode());
        
        Scanner sc = new Scanner(System.in);
       //System.out.println( sc.getClass());
        System.out.println("Enter the row size:");
        int row_size = sc.nextInt();
        System.out.println("Enter the coloumn size:");
        int colomn_size = sc.nextInt();
        int input[][] = new int[row_size][colomn_size];
        System.out.println("Enter the elements");
        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < input[i].length; j++) {
                input[i][j] = sc.nextInt();
            }
        }
        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < input[i].length; j++) {
                System.out.print(input[i][j] + " ");
            }
            System.out.println("");
        }

        System.out.println("The instruction was only 3 characters in length");
        System.out.println("Enter the 1st Instruction:");
        String instruction = sc.next();
        int rotaion_1[][] = new int[row_size][colomn_size];
        if (instruction.charAt(0) == 'R' || instruction.charAt(0) == 'r') {
            rotaion_1 = rightRotation(input, row_size, colomn_size);
        } else if (instruction.charAt(0) == 'L' || instruction.charAt(0) == 'l') {
            rotaion_1 = leftRotation(input, row_size, colomn_size);
        }
        int rotation_2[][] = new int[row_size][colomn_size];
        if (instruction.charAt(1) == 'R' || instruction.charAt(1) == 'r') {
            rotation_2 = rightRotation(rotaion_1, row_size, colomn_size);
        } else if (instruction.charAt(1) == 'L' || instruction.charAt(1) == 'l') {
            rotation_2 = leftRotation(rotaion_1, row_size, colomn_size);
        }
        int rotation3[][] = new int[row_size][colomn_size];
        if (instruction.charAt(2) == 'R' || instruction.charAt(2) == 'r') {
            rotation3 = rightRotation(rotation_2, row_size, colomn_size);
        } else if (instruction.charAt(2) == 'L' || instruction.charAt(2) == 'l') {
            rotation3 = leftRotation(rotation_2, row_size, colomn_size);
        }

        for (int i = 0; i < rotation3.length; i++) {
            for (int j = 0; j < rotation3[i].length; j++) {
                System.out.print(rotation3[i][j] + " ");
            }
            System.out.println("");
        }

    }

}
