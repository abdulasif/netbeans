/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhatabookAsif1311;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author bas200190
 */
public class KhataBook {
    public static ObjectsDatabase ob = new ObjectsDatabase();
    
    public static void main(String[] args) {
        
        
        
        Scanner sc = new Scanner(System.in);
        ob.ac.customer_details[0] = new AddCustomer(LocalDate.of(2022, 10, 27),201, "asif", "8190877860", "12345", "chepauk",500);
        ob.ac.customer_details[1] = new AddCustomer(LocalDate.of(2022, 11, 5),202, "ismath", "6369466973", "54321", "triplicane",1500);
        ob.ac.customer_details[2] = new AddCustomer(LocalDate.of(2022, 11, 10),203, "lathif", "9944198786", "09876", "mount_road",3000);

        ob.p.product_details[0] = new Product(501, "maggie", "200g", 20, 25, "nestle", 200);
        ob.p.product_details[1] = new Product(502, "marrie_biscuit", "100g", 8, 10, "britania", 300);
        ob.p.product_details[2] = new Product(503, "chat_masala", "50g", 4, 10, "everest", 200);
        ob.p.product_details[3] = new Product(504, "detergent_powder", "1000g", 50, 65, "tide", 50);
        ob.p.product_details[4] = new Product(505, "bread", "250g", 18, 30, "modern", 30);

        boolean main_menu_loop = true;
        while (main_menu_loop) {
            ob.de.mainMenuDesign();
            int main_menu_input = sc.nextInt();
            switch(main_menu_input){
                case 1:
                    boolean customer_menu_loop = true;
                    while(customer_menu_loop){
                        ob.de.customerMenuDesign();
                        int customer_menu_input = sc.nextInt();
                                                                            
                        switch(customer_menu_input){
                            case 1:
                                ob.ac.addCustomer();
                                break;
                            case 2:
                                boolean view_customer_loop = true;
                                while (view_customer_loop) {
                                    ob.de.viewCustomerMenuDesign();
                                    int view_customer_input = sc.nextInt();

                                    switch (view_customer_input) {
                                        case 1:
                                            ob.vc.viewParticularCustomer();
                                            break;
                                        case 2:
                                            ob.de.viewCustomerDesign();
                                            ob.vc.viewAllCustomer();
                                            break;
                                        case 3:
                                            view_customer_loop = false;
                                            break;
                                    }
                                }
                                break;
                                        
                            case 3:
                                
                                ob.ac.updateCustomer();
                                
                                break;
                                
                            case 4:
                                
                                ob.vo.ors = Arrays.copyOf(ob.vo.ors, ob.vo.ors.length + 1);
                                ob.vo.ors[ob.vo.ors.length - 1] = ob.o1.addOrder();
                                ob.de.afterOrder1Design();
                                int after_order_1_input = sc.nextInt();
                                switch(after_order_1_input){
                                    case 1:
                                        ob.de.afterOrder2Design();
                                        int pay_now_input = sc.nextInt();
                                        switch(pay_now_input){
                                            case 1:
                                                ob.pay.payThisOrder(ob.vo.ors[ob.vo.ors.length - 1].customer_id,ob.vo.ors.length - 1);
                                                System.out.println("PAID");
                                                ob.pay.currentDebit(ob.vo.ors.length - 1);
                                                break;
                                            case 2:
                                                ob.pay.payPartially(ob.vo.ors[ob.vo.ors.length - 1].customer_id);
                                                System.out.println("PAID");
                                                ob.pay.currentDebit(ob.vo.ors.length - 1);
                                                break;
                                        }
                                        break;
                                    case 2:
                                        break;
                                }
                                break;
                            case 5:
                                boolean view_order_loop = true;
                                while (view_order_loop) {
                                    ob.de.viewOrderMenuDesign();
                                    int view_order_input = sc.nextInt();

                                    switch (view_order_input) {
                                        case 1:
                                            ob.vo.viewParticularOrder();
                                            System.out.println("");
                                            
                                            break;
                                        case 2:
                                            ob.vo.viewAllOrder();
                                            
                                            break;
                                        case 3:
                                            view_order_loop = false;
                                            break;
                                    }
                                }
                                break;
                            case 6:
                                boolean make_payment = true;
                                while (make_payment) {
                                    System.out.println("Enter Customer ID:");
                                    int pp_customer_id = sc.nextInt();
                                    for (int i = 0; i < ob.ac.customer_details.length; i++) {
                                        if (ob.ac.customer_details[i].customer_id == pp_customer_id) {
                                            ob.pay.payPartially(pp_customer_id);
                                            make_payment = false;
                                        }
                                    }
                                }
                                System.out.println("PAID");
                                ob.pay.currentDebit(ob.vo.ors.length - 1);
                                break;
                            case 7:
                                customer_menu_loop= false; 
                                break;
                        }
                    }
//                    customer_menu_loop = true;
                    break;
                case 2:
                    boolean inventory_menu_loop = true;
                    while (inventory_menu_loop) {
                        ob.de.inventoryMenuDesign();
                        int inventory_menu_input = sc.nextInt();

                        switch (inventory_menu_input) {
                            case 1:
                                boolean add_product_loop = true;
                                while (add_product_loop) {
                                    ob.p.addProduct();
                                    System.out.println("SUCCESSFULLY ADDED PRODUCT DETAILS....");
                                    System.out.println("To add More Product Press '1' (or) Exit Press '2'");
                                    int add_more_product_input = sc.nextInt();
                                    switch (add_more_product_input) {
                                        case 1:
                                            add_product_loop = true;
                                            break;
                                        case 2:
                                            add_product_loop = false;
                                            break;
                                    }
                                }
//                                add_product_loop = true;
                                break;
                            case 2:
                                
                                boolean view_product_loop = true;
                                while(view_product_loop){
                                    ob.de.viewProductMenuDesign();
                                    int view_product_input = sc.nextInt();
                                    switch(view_product_input){
                                        case 1:
                                            ob.p.viewParticularProduct();
                                            break;
                                        case 2:
                                            ob.p.viewAllProducts();
                                            break;
                                        case 3:
                                            view_product_loop = false;
                                            break;
                                    }
                                }
                                view_product_loop = true;
                                break;
                            case 3:
                                ob.p.updateProduct();
                                System.out.println("PRODUCT UPDATED SUCCESSFULLY");
                                break;
                            case 4:
                                inventory_menu_loop = false;
                                break;
                        }
                    }
//                    inventory_menu_loop = true;
                break;    
                case 4:
                    main_menu_loop = false;
                    break;
            }
        }
//        main_menu_loop = true;
    }
}
