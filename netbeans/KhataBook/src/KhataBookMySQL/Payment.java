/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhataBookMySQL;

/**
 *
 * @author bas200190
 */
public class Payment {
    private final int payment_id;
    private int customer_id;
    private double paid_amount;
    
    public Payment(int payment_id, int customer_id, double paid_amount) {
        this.payment_id = payment_id;
        this.customer_id = customer_id;
        this.paid_amount = paid_amount;
    }

    public int getPayment_id() {
        return payment_id;
    }

    
    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public double getPaid_amount() {
        return paid_amount;
    }

    public void setPaid_amount(double paid_amount) {
        this.paid_amount = paid_amount;
    }
}
