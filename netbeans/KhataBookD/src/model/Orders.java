/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.List;

/**
 *
 * @author bas200190
 */
public class Orders {

    private int order_id;
    private int customer_id;
    private double total_price;
    private List<ItemList> item_lists;

    public List<ItemList> getItem_lists() {
        return item_lists;
    }

    public void setItem_lists(List<ItemList> item_lists) {
        this.item_lists = item_lists;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public double getTotal_price() {
        return total_price;
    }

    public void setTotal_price(double total_price) {
        this.total_price = total_price;
    }

    public Orders(int order_id, int customer_id, double total_price, List<ItemList> item_lists) {

        this.order_id = order_id;
        this.customer_id = customer_id;
        this.total_price = total_price;
        this.item_lists = item_lists;
    }

}
