public class Twofer {
    public String twofer(String name) {
        if(name.isBlank()){
          return "One for you, one for me.";
        }
        else{
          return String.format("One for %s, one for me.",name==null?"you":name);
        }
    }
    public String twofer() {
        return "One for you, one for me.";
    }
    
    public static void main(String[] args) {
        Twofer t = new Twofer();
        System.out.println(t.twofer(""));
    }
}