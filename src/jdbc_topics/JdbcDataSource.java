package jdbc_topics;

import com.mysql.cj.jdbc.MysqlDataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JdbcDataSource {

    public static void main(String[] args) throws ClassNotFoundException {

        String DB_URL = "jdbc:mysql://localhost/hari";
        String USER = "root";
        String PASSWORD = "root";
        String QUERY = "SELECT * FROM Employees";

        Class.forName("com.mysql.cj.jdbc.Driver");

        try {

            //creating data source
            MysqlDataSource dataSource = new MysqlDataSource();

            dataSource.setUser(USER);
            dataSource.setPassword(PASSWORD);
            dataSource.setURL(DB_URL);

            // creating DB connection
            Connection connection = dataSource.getConnection();

            //creating callable statement
            Statement statement = connection.createStatement();

            //executing the query
            ResultSet resultSet = statement.executeQuery(QUERY);

            //processing the retrived data
            while (resultSet.next()) {
                System.out.println("\n---------------------------------------------------");
                System.out.println("EMPLOYEE ID : " + resultSet.getInt("EmployeeID"));
                System.out.println("FIRST NAME : " + resultSet.getString("FirstName"));
                System.out.println("LAST NAME : " + resultSet.getString("LastName"));
                System.out.println("TECHNOLOGY : " + resultSet.getString("Technology"));
                System.out.println("DESIGNATION : " + resultSet.getString("Designation"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(JdbcDataSource.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
