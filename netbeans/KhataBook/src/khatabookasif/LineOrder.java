/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package khatabookasif;

import java.util.Arrays;
import java.util.Scanner;
import static khatabookasif.KhataBook.oa;

/**
 *
 * @author bas200190
 */
public class LineOrder {
    static Scanner sc = new Scanner(System.in);
     LineItem[] line_order_details = new LineItem[0];
    
    
    public void addOrder() {

        System.out.println("Enter the Customers Phone Number");
        String match_phoneno = sc.next();

        for (int i = 0; i < oa.cus.customer_details.length; i++) {
            if (match_phoneno.equals(oa.cus.customer_details[i].customer_phoneNo)) {

                line_order_details = Arrays.copyOf(line_order_details,line_order_details.length+1);
                
                System.out.println("ALL PRODUCTS ARE....");
                oa.inv.viewProducts();

                boolean purchase_loop = true;
                while (purchase_loop) {
                    System.out.println("ENTER PRODUCT ID:");
                    int match_productid = sc.nextInt();
                    System.out.println("ENTER QUANTITY YOU NEED:");
                    int match_quantity = sc.nextInt();

                    for (int k = 0; k < oa.inv.product_details_arr.length; k++) {
                        if (match_productid == oa.inv.product_details_arr[k].product_id) {
                            oa.inv.product_details_arr[k].product_quantity = oa.inv.product_details_arr[k].product_quantity - match_quantity;

                            line_order_details[line_order_details.length-1] = oa.li.placeLineItem(oa.cus.customer_details[i].customer_id, oa.cus.customer_details[i].customer_phoneNo, oa.inv.product_details_arr[k].product_id, oa.inv.product_details_arr[k].product_name, oa.inv.product_details_arr[k].product_selling_price, match_quantity, (oa.inv.product_details_arr[k].product_selling_price * match_quantity));
                            
                        }

                    }

                    System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(20) + "+");
                    System.out.format("\n|%-5s|%-20s|", "1", "QUIT");
                    System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(20) + "+");
                    System.out.format("\n|%-5s|%-20s|", "2", "ADD ON PRODUCTS");
                    System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(20) + "+");
                    System.out.println("");
                    System.out.println("select option:");
                    int placeOrderRepeat = sc.nextInt();
                    switch (placeOrderRepeat) {
                        case 1:
                            purchase_loop = false;
                            break;
                        case 2:
                            purchase_loop = true;
                            break;
                    }

                }
                purchase_loop = true;

            }
        }
    
    }

}
