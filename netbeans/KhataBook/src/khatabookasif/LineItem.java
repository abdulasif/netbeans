/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package khatabookasif;

import java.util.Arrays;
import static khatabookasif.KhataBook.oa;

/**
 *
 * @author bas200190
 */
public class LineItem {

    static int set_line_item_id = 1;
    int line_item_id;
    int customer_id_match;
    String customer_phoneNo_match;
    int customer_ordered_product_id;
    String customer_ordered_product_name;
    int product_price_per_unit;
    int customer_ordered_product_quantity;
    int total_price_of_each_product;
    LineItem[] customer_line_item_details = new LineItem[0];
    int customer_line_item_details_index=0;
    
    
    
    public LineItem(){
    
    }
    
    public LineItem(int order_id,int customer_id_match, String customer_phoneNo_match, int customer_ordered_product_id, String customer_ordered_product_name ,int product_price_per_unit, int customer_ordered_product_quantity,int total_price_of_each_product){
    
      this.line_item_id = order_id;  
      this.customer_id_match = customer_id_match;
      this.customer_phoneNo_match = customer_phoneNo_match;
      this.customer_ordered_product_id = customer_ordered_product_id;
      this.customer_ordered_product_name = customer_ordered_product_name;
      this.product_price_per_unit =  product_price_per_unit;
      this.customer_ordered_product_quantity = customer_ordered_product_quantity;
      this.total_price_of_each_product =total_price_of_each_product;
      
    }
    
    public LineItem placeLineItem(int customer_id_match, String customer_phoneNo_match, int customer_ordered_product_id, String customer_ordered_product_name ,int product_price_per_unit, int customer_ordered_product_quantity,int total_price_per_product){
      customer_line_item_details = Arrays.copyOf(customer_line_item_details, customer_line_item_details.length+1);
      
      customer_line_item_details[customer_line_item_details_index] = new LineItem(set_line_item_id,customer_id_match,customer_phoneNo_match,customer_ordered_product_id,customer_ordered_product_name,product_price_per_unit,customer_ordered_product_quantity,total_price_per_product);
      customer_line_item_details_index++;
      set_line_item_id++;
      return customer_line_item_details[customer_line_item_details_index-1];
      
    }
    
    
    public void viewAllLineItem(){
        for (int n = 0; n < customer_line_item_details.length; n++) {
            System.out.println(customer_line_item_details[n]);
        }
    }
    
   
    
    
    
    public String toString(){
        System.out.print("\n+"+"-".repeat(20)+"+"+"-".repeat(20)+"+"+"-".repeat(20)+"+"+"-".repeat(20)+"+"+"-".repeat(20)+"+"+"-".repeat(20)+"+"+"-".repeat(20)+"+"+"-".repeat(20)+"+");
        System.out.format("\n|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|","LINE ITEM ID","CUSTOMER ID","CUSTOMER PH NO","PRODUCT ID","PRODUCT NAME","PRICE PER UNIT","PRODUCT QUANTITY","TOTAL");
        System.out.print("\n+"+"-".repeat(20)+"+"+"-".repeat(20)+"+"+"-".repeat(20)+"-".repeat(20)+"+"+"+"+"-".repeat(20)+"+"+"-".repeat(20)+"+"+"-".repeat(20)+"+"+"-".repeat(20)+"+");
        System.out.format("\n|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|",this.line_item_id,this.customer_id_match,this.customer_phoneNo_match,this.customer_ordered_product_id,this.customer_ordered_product_name,this.product_price_per_unit,this.customer_ordered_product_quantity,this.total_price_of_each_product);
        System.out.print("\n+"+"-".repeat(20)+"+"+"-".repeat(20)+"+"+"-".repeat(20)+"-".repeat(20)+"+"+"+"+"-".repeat(20)+"+"+"-".repeat(20)+"+"+"-".repeat(20)+"+"+"-".repeat(20)+"+");
        
        return "";
    }
    

    
}
