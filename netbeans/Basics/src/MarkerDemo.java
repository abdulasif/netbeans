
public class MarkerDemo {
    public static void main(String[] args) {
        Marker m1 = new Marker();
        System.out.println(m1.brand);
        System.out.println(m1.colour);
        System.out.println(m1.price);
        Marker m2 = new Marker();
        System.out.println(m2.brand);
        System.out.println(m2.colour);
        System.out.println(m2.price);
        m1.price=24.5;
        Marker.count = 10;
        System.out.println(m1.brand);
        System.out.println(m1.colour);
        System.out.println(m1.price);
        
        System.out.println(m2.brand);
        System.out.println(m2.colour);
        System.out.println(m2.price);
        System.out.println(Marker.count);
        Marker.count++;
        System.out.println(Marker.count);
    }
}
