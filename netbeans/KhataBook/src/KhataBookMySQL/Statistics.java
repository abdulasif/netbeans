/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhataBookMySQL;

import java.time.LocalDate;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Date;
import java.sql.ResultSet;

/**
 *
 * @author bas200190
 */
public class Statistics {

    public void todaysCollection(Connection connection) {
        try {
            String query = "select * from KB_payment where payment_date = ?";

            try (PreparedStatement ps = connection.prepareStatement(query);) {
                ps.setDate(1, Date.valueOf(LocalDate.now()));

                try (ResultSet rs = ps.executeQuery();) {

                    double today_collection = 0;
                    Design.paymentDetailsDesign();
                    while (rs.next()) {
                        System.out.format("\n|%-20s|%-20s|%-20s|%-20s|", rs.getDate(1), rs.getInt(2), rs.getInt(3), rs.getDouble(4));
                        System.out.print("\n+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");
                        today_collection = today_collection + rs.getDouble(4);
                    }
                    System.out.println("");
                    System.out.println("");
                    System.out.println("TODAY'S COLLECTION : " + today_collection);
                }
            }
        } catch (SQLException sqle) {
            System.out.println("Payments are not done in today");
        }
    }

    public void specificDateCollection(String date, Connection connection) {
        String query = "select * from KB_payment where payment_date = ?";
        try {
            try (PreparedStatement ps = connection.prepareStatement(query);) {
                ps.setDate(1, Date.valueOf(date));

                try (ResultSet rs = ps.executeQuery();) {

                    double collection = 0;
                    Design.paymentDetailsDesign();
                    while (rs.next()) {
                        System.out.format("\n|%-20s|%-20s|%-20s|%-20s|", rs.getDate(1), rs.getInt(2), rs.getInt(3), rs.getDouble(4));
                        System.out.print("\n+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");
                        collection = collection + rs.getDouble(4);
                    }
                    System.out.println("");
                    System.out.println("");
                    System.out.println(date + " COLLECTION : " + collection);
                }
            }
        } catch (SQLException sqle) {
            System.out.println("Payment is not done on that Date");
        }
    }

    public void totalCustomerDebits(Connection connection) {
        try {
            double total_debits = 0;

            String query = "select * from KB_customer";

            try (PreparedStatement ps = connection.prepareStatement(query); ResultSet rs = ps.executeQuery();) {

                while (rs.next()) {
                    total_debits = total_debits + rs.getDouble(6);
                }

                System.out.println("TOTAL DEBITS : " + total_debits);
            }
        } catch (SQLException sqle) {
            System.out.println("Total Customer is not Done..");
        }
    }

    public void currentStockPrice(Connection connection) {

        String query = "select product_buying_price , product_quantity from KB_product ";

        try (PreparedStatement ps = connection.prepareStatement(query); ResultSet rs = ps.executeQuery();) {
            double current_stock_price = 0;
            while (rs.next()) {
                double selling_price = rs.getDouble(1);
                int quantity = rs.getInt(2);

                current_stock_price = current_stock_price + (selling_price * quantity);
            }
            System.out.println("CURRENT STOCK PRICE : " + current_stock_price);
        } catch (SQLException sqle) {
            System.out.println("sql exception is raised..");
        }

    }

    public void highSellingProductsList(Connection connection) throws Exception {
        try{
        String query = "select max(product_id) , count(product_id) from KB_item_list group by product_id";

        try(PreparedStatement ps = connection.prepareStatement(query);
        ResultSet rs = ps.executeQuery();){

        Design.highSellingProductListDesign();
        while (rs.next()) {
            int product_id = rs.getInt(1);
            int count = rs.getInt(2);

            String query_1 = "select product_name from KB_product where product_id = ?";
            PreparedStatement ps_1 = connection.prepareStatement(query_1);
            ps_1.setInt(1, product_id);
            ResultSet rs_1 = ps_1.executeQuery();

            if (rs_1.next()) {
                System.out.format("\n|%-25d|%-25s|%-25d|", product_id, rs_1.getString(1), count);
                System.out.print("\n+" + "-".repeat(25) + "+" + "-".repeat(25) + "+" + "-".repeat(25) + "+");
            }

        }
        }
        } catch (SQLException sqle) {
            System.out.println("sql exception is raised..");
        }
    }
}
