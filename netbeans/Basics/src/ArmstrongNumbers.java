class ArmstrongNumbers {

    int isArmstrongNumber(int numberToCheck) {
        int number = numberToCheck;
        int noOfDigits = 0;
        while(number!=0) {
            number = number / 10;
            noOfDigits++;
        }
        return noOfDigits;
    }
    
    public static void main(String[] args) {
        ArmstrongNumbers ex1 = new ArmstrongNumbers();
        System.out.println(ex1.isArmstrongNumber(15389));
    }

}
