/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhataBookMySQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;

/**
 *
 * @author bas200190
 */
public class OrderDetails {

    public void viewAllOrder(Connection connection) {
        try {
            String query_1 = "select * from KB_order";

            try (PreparedStatement ps_1 = connection.prepareStatement(query_1); ResultSet rs_1 = ps_1.executeQuery()) {

                while (rs_1.next()) {
                    Design.viewOrderDesign();
                    int orders_order_id = rs_1.getInt(2);

                    String query_2 = "select * from KB_item_list where order_id = ?";

                    try (PreparedStatement ps_2 = connection.prepareStatement(query_2)) {
                        ps_2.setInt(1, orders_order_id);
                        try (ResultSet rs_2 = ps_2.executeQuery()) {
                            while (rs_2.next()) {
                                System.out.format("\n|%-15s|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|", rs_1.getDate(1), rs_1.getInt(3), rs_1.getInt(2), rs_2.getInt(3), rs_2.getInt(4), rs_2.getDouble(5), rs_2.getDouble(6));
                                System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");
                            }
                            System.out.format("\n|%120s|%-20s|", "TOTAL PRICE", rs_1.getDouble(4));
                            System.out.print("\n+" + "-".repeat(120) + "+" + "-".repeat(20) + "+");
                            System.out.println("");
                            System.out.println("");
                            System.out.println("");
                        }
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println("View all Order is not Done..");
        }

    }

    public void viewParticularOrder(int vpo_order_id, Connection connection) {
        try {
            String query_1 = "select * from KB_order where order_id = ?";

            try (PreparedStatement ps_1 = connection.prepareStatement(query_1)) {
                ps_1.setInt(1, vpo_order_id);
                try (ResultSet rs_1 = ps_1.executeQuery()) {
                    if (rs_1.next()) {
                        Design.viewOrderDesign();
                        String query_2 = "select * from KB_item_list where order_id = ?";

                        try (PreparedStatement ps_2 = connection.prepareStatement(query_2)) {
                            ps_2.setInt(1, vpo_order_id);
                            try (ResultSet rs_2 = ps_2.executeQuery()) {
                                while (rs_2.next()) {
                                    System.out.format("\n|%-15s|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|", rs_1.getDate(1), rs_1.getInt(3), rs_1.getInt(2), rs_2.getInt(3), rs_2.getInt(4), rs_2.getDouble(5), rs_2.getDouble(6));
                                    System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");
                                }
                                System.out.format("\n|%120s|%-20s|", "TOTAL PRICE", rs_1.getDouble(4));
                                System.out.print("\n+" + "-".repeat(120) + "+" + "-".repeat(20) + "+");
                                System.out.println("");
                                System.out.println("");
                                System.out.println("");
                            }
                        }

                    }
                }
            }
        } catch (SQLException e) {
            System.out.println("View particular Order is not Done..");
        }
    }
}
