/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhataBookAsif1611;

/**
 *
 * @author bas200190
 */
public class Design {

    public static void mainMenuDesign() {
        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(15) + "+");
        System.out.format("\n|%-5s|%-15s|", "S.NO", "MAIN MENU");
        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(15) + "+");
        String[] MainMenu = {"Customer", "Inventory", "Statistics", "Exit"};
        for (int i = 0; i < MainMenu.length; i++) {

            System.out.format("\n|%-5d|%-15s|", (i + 1), MainMenu[i]);
        }
        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(15) + "+");
        System.out.println("");
        System.out.print("select option:");

    }

    public static void customerMenuDesign() {

        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(18) + "+");
        System.out.format("\n|%-5s|%-18s|", "S.NO", "CUSTOMER MENU");
        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(18) + "+");
        String[] customerMenu = {"Add Customer", "View Customer", "Update Customer", "Place Order", "View Orders", "Make Payment", "Payment Details", "Exit"};
        for (int i = 0; i < customerMenu.length; i++) {

            System.out.format("\n|%-5d|%-18s|", (i + 1), customerMenu[i]);
        }
        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(18) + "+");
        System.out.println("");
        System.out.print("select option:");
    }

    public static void viewCustomerMenuDesign() {
        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(25) + "+");
        String[] customerMenu = {"View Particular Customer", "View All Customer", "Exit"};
        for (int i = 0; i < customerMenu.length; i++) {

            System.out.format("\n|%-5d|%-25s|", (i + 1), customerMenu[i]);
        }
        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(25) + "+");
        System.out.println("");
        System.out.print("select option:");

    }

    public static void viewCustomerDesign() {
        System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(25) + "+" + "-".repeat(20) + "+");
        System.out.format("\n|%-15s|%-20s|%-20s|%-20s|%-20s|%-25s|%-20s|", "JOINING DATE", "CUSTOMER ID", "CUSTOMER NAME", "CUSTOMER PHONE NO", "CUSTOMER AADHAR NO", "CUSTOMER ADDRESS", "CUSTOMER DEBIT");
        System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(25) + "+" + "-".repeat(20) + "+");

    }

    public static void inventoryMenuDesign() {
        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(15) + "+");
        System.out.format("\n|%-5s|%-15s|", "S.NO", "INVENTORY MENU");
        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(15) + "+");
        String[] inventoryMenu = {"Add Products", "View Products", "Top Up Product", "Update Products", "Exit"};
        for (int j = 0; j < inventoryMenu.length; j++) {

            System.out.format("\n|%-5d|%-15s|", (j + 1), inventoryMenu[j]);
        }
        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(15) + "+");
        System.out.println("");
        System.out.print("select option:");

    }

    public static void viewProductDesign() {
        System.out.print("\n+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");
        System.out.format("\n|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|", "PRODUCT ID", "PRODUCT NAME", "PRODUCT WEIGHT", "PRODUCT BUY PRICE", "CUSTOMER PRICE", "PRODUCT BRAND", "PRODUCT QUANTITY");
        System.out.print("\n+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");
    }

    public static void viewProductMenuDesign() {
        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(25) + "+");
        String[] view_product_menu = {"View Particular Product", "View All Product", "Exit"};
        for (int i = 0; i < view_product_menu.length; i++) {

            System.out.format("\n|%-5d|%-25s|", (i + 1), view_product_menu[i]);
        }
        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(25) + "+");
        System.out.println("");
        System.out.print("select option:");

    }

    public static void repeatPurchaseDesign() {
        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(20) + "+");
        System.out.format("\n|%-5s|%-20s|", "1", "QUIT");
        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(20) + "+");
        System.out.format("\n|%-5s|%-20s|", "2", "ADD ON PRODUCTS");
        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(20) + "+");
        System.out.println("");
        System.out.println("select option:");

    }

    public static void viewOrderMenuDesign() {
        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(25) + "+");
        String[] view_order_menu = {"View Particular Order", "View All Orders", "Exit"};
        for (int i = 0; i < view_order_menu.length; i++) {

            System.out.format("\n|%-5d|%-25s|", (i + 1), view_order_menu[i]);
        }
        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(25) + "+");
        System.out.println("");
        System.out.print("select option:");

    }

    public static void viewOrderDesign() {
        System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");
        System.out.format("\n|%-15s|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|", "ORDERED DATE", "CUSTOMER ID", "ORDER ID", "PRODUCT ID", "PRODUCT QUANTITY", "EACH PROD PRICE", "PRICE");
        System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");

    }

    public static void menuAfterOrder1Design() {   //new
        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(25) + "+");
        String[] after_order_menu = {"Pay Now", "Pay later"};
        for (int i = 0; i < after_order_menu.length; i++) {

            System.out.format("\n|%-5d|%-25s|", (i + 1), after_order_menu[i]);
        }
        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(25) + "+");
        System.out.println("");
        System.out.print("select option:");

    }

    public static void menuAfterOrder2Design() {      //new
        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(25) + "+");
        String[] after_order_menu = {"Pay this Order", "Pay Partially"};
        for (int i = 0; i < after_order_menu.length; i++) {

            System.out.format("\n|%-5d|%-25s|", (i + 1), after_order_menu[i]);
        }
        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(25) + "+");
        System.out.println("");
        System.out.print("select option:");

    }

    public static void showDebitDesign() {

    }

    public static void paymentDetailsDesign() {
        System.out.print("\n+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");
        System.out.format("\n|%-20s|%-20s|%-20s|%-20s|", "PAID DATE", "PAYMENT ID", "CUSTOMER ID", "PAID AMOUNT");
        System.out.print("\n+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");
    }

    public static void statisticsMenu() {

        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(25) + "+");
        System.out.format("\n|%-5s|%-25s|", "S.NO", "STATISTICS MENU");
        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(25) + "+");
        String[] after_order_menu = {"Todays Collection", "High Selling Product", "Total Customer Debits", "Current Stock Price", "Exit"};
        for (int i = 0; i < after_order_menu.length; i++) {

            System.out.format("\n|%-5d|%-25s|", (i + 1), after_order_menu[i]);
        }
        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(25) + "+");
        System.out.println("");
        System.out.print("select option:");
    }

    

}
