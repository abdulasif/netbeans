
import java.util.Scanner;
public class MatrixMultiplication {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the row size and coloumn size of 'arr1'");
        int sizea1  = sc.nextInt();
        int sizea2 = sc.nextInt();
        System.out.println("Enter the row size and coloumn size of 'arr2'");
        boolean temp = true;
        int sizeb1=0;
        while(temp)
        {
            System.out.println("1st matrix coloumn size and 2nd matrix row  should be equal");
          sizeb1 = sc.nextInt();
          if(sizea2==sizeb1)
          {
            temp=false;
          }
        }
        
        int sizeb2 = sc.nextInt();
        int[][] arr1 = new int[sizea1][sizea2];
        int[][] arr2 = new int[sizeb1][sizeb2];
        System.out.println("Enter the elements of 2d 'arr1' array:");
          for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr1[i].length; j++) {
              arr1[i][j]=sc.nextInt();
            }
          }
        System.out.println("arr1 array:");
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr1[i].length; j++) {
                System.out.print(arr1[i][j]+" ");
            }
            System.out.println("");
        }  
        System.out.println("Enter the elements of 2d 'arr2' array:");
        for (int i = 0; i < arr2.length; i++) {
            for (int j = 0; j < arr2[i].length; j++) {
              arr2[i][j]=sc.nextInt();
            }
        }
        System.out.println("arr2 array:");
        for (int i = 0; i < arr2.length; i++) {
            for (int j = 0; j < arr2[i].length; j++) {
                System.out.print(arr2[i][j]+" ");
            }
            System.out.println("");
        } 
        int[][] arr = new int[sizea1][sizeb2];
        for (int k = 0; k < sizea1; k++) {
            for (int i = 0; i < sizeb2; i++) {
                for (int j = 0; j < sizeb1 ;j++) {
                    arr[k][i] = arr[k][i] + (arr1[k][j] * arr2[j][i]);
                }
            }
        }
        System.out.println("Printing the answer 'arr' array");
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j]+" ");
            }
                System.out.println(" ");
        }
    }
}