import java.util.Scanner;
public class SafeElements {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the 1st size of 2dimensional array:");
        int sizea  = sc.nextInt();
        System.out.println("Enter the 2nd size of 2dimensional array:");
        int sizeb = sc.nextInt();
        int[][]arr = new int[sizea][sizeb];
        System.out.println("Enter the elements of 2d 'arr' array:");
          for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
              arr[i][j]=sc.nextInt();
            }
          }
        int max=arr[0][0];
        int max_i_index = 0;
        int max_j_index = 0;
        int min_i_index = 0;
        int min_j_index = 0;
        int min=arr[0][0];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.println(arr[i][j]);
                if(arr[i][j]>max)
                {
                    max=arr[i][j];
                  
                  max_i_index = i;
                  max_j_index = j;
                }
                if (arr[i][j]<min)
                {
                  min=arr[i][j];
                  min_i_index = i;
                  min_j_index = j;
                }
            }
        }
        System.out.println("max element : "+max);
        System.out.println("min element: "+min);
        System.out.println("max index i&j :"+max_i_index +","+max_j_index);
        System.out.println("min index i&j :"+min_i_index +","+min_j_index);
        int safe_count = 0;
        System.out.println("The safe Elements are: ");
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if(i!=max_i_index && j!=max_j_index && i!=min_i_index && j!=min_j_index)
                {
                    System.out.println(arr[i][j]);
                  safe_count++;
                }
            }
        }
        System.out.println("Safe Count is: "+safe_count);
    }
}
