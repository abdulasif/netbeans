/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;



/**
 *
 * @author bas200190
 */
public class ItemList {

    private int item_list_id;
    private int order_id;
    private int product_id;
    private int product_quantity;
    private double each_price;
    private double price;
    
    

    public int getItem_list_id() {
        return item_list_id;
    }

    public void setItem_list_id(int item_list_id) {
        this.item_list_id = item_list_id;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getProduct_quantity() {
        return product_quantity;
    }

    public void setProduct_quantity(int product_quantity) {
        this.product_quantity = product_quantity;
    }

    public double getEach_price() {
        return each_price;
    }

    public void setEach_price(double each_price) {
        this.each_price = each_price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    

    public ItemList(int item_list_id, int order_id, int product_id, int product_quantity, double eachprice, double price) {
        this.item_list_id = item_list_id;
        this.order_id = order_id;
        this.product_id = product_id;
        this.product_quantity = product_quantity;
        this.each_price = eachprice;
        this.price = eachprice * product_quantity;
    }
    
    

}
