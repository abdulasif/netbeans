class Leap {

    boolean isLeapYear(int year) {
       if(year%4==0){
           if(year%100==0){
               if(year%400==0){
                   return true;
               }
               else{
                   return false;
               }
           }
           else{
               return true;
           }
       }
       else{
         return false;
       }
    }

    public static void main(String[] args) {
        Leap ex1 = new Leap();
        System.out.println(ex1.isLeapYear(3000));
    }
}
