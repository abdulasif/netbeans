    /*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhataBookAsif1611;

import java.io.DataOutputStream;
import java.io.FileOutputStream;

/**
 *
 * @author bas200190
 */
public class FileWriting {
    
    public  void customerDetatilsWrite(){
        try(DataOutputStream dos_customer_details = new DataOutputStream(new FileOutputStream("/home/bas200190/Documents/netbeans/KhataBook/src/KhataBookAsif1611/customer_details.txt"));){
            for (int i = 0; i < KhataBook.ob.kd.customer_details.length; i++) {
                dos_customer_details.writeUTF(KhataBook.ob.kd.customer_details[i].customer_joining_date+"");
                dos_customer_details.writeInt(KhataBook.ob.kd.customer_details[i].customer_id);
                dos_customer_details.writeUTF(KhataBook.ob.kd.customer_details[i].customer_name);
                dos_customer_details.writeUTF(KhataBook.ob.kd.customer_details[i].customer_phoneNo);
                dos_customer_details.writeUTF(KhataBook.ob.kd.customer_details[i].customer_aadharNo);
                dos_customer_details.writeUTF(KhataBook.ob.kd.customer_details[i].customer_address);
                dos_customer_details.writeDouble(KhataBook.ob.kd.customer_details[i].customer_debit);
            }
            
        }catch(Exception e){
            System.out.println("ALL CUSTOMERS DETAIL WRITE SUCCESS");
        }
        
    }
    public  void productDetailsWrite(){
        try(DataOutputStream dos_product_details = new DataOutputStream(new FileOutputStream("/home/bas200190/Documents/netbeans/KhataBook/src/KhataBookAsif1611/product_det.txt"))){
            for (int i = 0; i < KhataBook.ob.kd.product_details.length; i++) {
                dos_product_details.writeInt(KhataBook.ob.kd.product_details[i].product_id);
                dos_product_details.writeUTF(KhataBook.ob.kd.product_details[i].product_name);
                dos_product_details.writeUTF(KhataBook.ob.kd.product_details[i].product_weight);
                dos_product_details.writeDouble(KhataBook.ob.kd.product_details[i].product_buying_price);
                dos_product_details.writeDouble(KhataBook.ob.kd.product_details[i].product_selling_price);
                dos_product_details.writeUTF(KhataBook.ob.kd.product_details[i].product_brand);
                dos_product_details.writeInt(KhataBook.ob.kd.product_details[i].product_quantity);
            }
            
        }catch(Exception e){
            System.out.println("ALL PRODUCT DETAILS WRITE SUCCESS");
        }
        
    }
    
    public void orderWithItemListWrite() {
         
        try ( DataOutputStream dos_order_details = new DataOutputStream(new FileOutputStream("/home/bas200190/Documents/netbeans/KhataBook/src/KhataBookAsif1611/order_details.txt"));
                DataOutputStream dos_item_list_details = new DataOutputStream(new FileOutputStream("/home/bas200190/Documents/netbeans/KhataBook/src/KhataBookAsif1611/item_list_details.txt"));) {
            
            for (int i = 0; i < KhataBook.ob.ord_det.order_details.length; i++) {
                dos_order_details.writeUTF(KhataBook.ob.ord_det.order_details[i].ordered_date + "");
                dos_order_details.writeInt(KhataBook.ob.ord_det.order_details[i].order_id);
                dos_order_details.writeInt(KhataBook.ob.ord_det.order_details[i].customer_id);
                dos_order_details.writeDouble(KhataBook.ob.ord_det.order_details[i].total_price);

                    for (int k = 0; k < KhataBook.ob.ord_det.order_details[i].item_list.length; k++) {
                        dos_item_list_details.writeInt(KhataBook.ob.ord_det.order_details[i].item_list[k].order_id);
                        dos_item_list_details.writeInt(KhataBook.ob.ord_det.order_details[i].item_list[k].product_id);
                        dos_item_list_details.writeInt(KhataBook.ob.ord_det.order_details[i].item_list[k].product_quantity);
                        dos_item_list_details.writeDouble(KhataBook.ob.ord_det.order_details[i].item_list[k].each_price);
                        dos_item_list_details.writeDouble(KhataBook.ob.ord_det.order_details[i].item_list[k].price);
                    }
                
            }
            
        } catch (Exception e) {
            System.out.println("ALL ORDERS AND LINE ITEMS WRITE SUCCESS");
        }
    }
    
    public void paymentDetailsWrite(){
        try(DataOutputStream dos_payment_details = new DataOutputStream(new FileOutputStream("/home/bas200190/Documents/netbeans/KhataBook/src/KhataBookAsif1611/payment_details.txt"))){
            for (int i = 0; i < KhataBook.ob.paydet.payment_details.length; i++) {
                dos_payment_details.writeUTF(KhataBook.ob.paydet.payment_details[i].payment_date+"");
                dos_payment_details.writeInt(KhataBook.ob.paydet.payment_details[i].payment_id);
                dos_payment_details.writeInt(KhataBook.ob.paydet.payment_details[i].customer_id);
                dos_payment_details.writeDouble(KhataBook.ob.paydet.payment_details[i].paid_amount);
            }
        }catch(Exception e){
            System.out.println("ALL PAYMENT DETAILS WRITE SUCCESS");
        }
    }
}
