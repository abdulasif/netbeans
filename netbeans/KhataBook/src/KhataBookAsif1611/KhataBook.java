/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhataBookAsif1611;

import java.util.Arrays;
import java.util.InputMismatchException;


/**
 *
 * @author bas200190
 */
public class KhataBook {

    public static ObjectsDatabase ob = new ObjectsDatabase();

    public static void main(String[] args) {

        ob.fr.customerDetailsRead();
        ob.fr.productDetailsRead();
        ob.fr.orderDetailsRead();
        ob.fr.paymentDetailsRead();
        boolean main_menu_loop = true;
        while (main_menu_loop) {
            Design.mainMenuDesign();
            try {
                int main_menu_input = Integer.parseInt(ob.br.readLine());
                switch (main_menu_input) {
                    
                    
                    
                    case 1:
                        boolean customer_menu_loop = true;
                        
                        while (customer_menu_loop) {
                            Design.customerMenuDesign();
                            int customer_menu_input = Integer.parseInt(ob.br.readLine());

                            switch (customer_menu_input) {
                                case 1:
                                    ob.addcus.addCustomer();
                                    break;
                                case 2:
                                    boolean view_customer_loop = true;
                                    while (view_customer_loop) {
                                        Design.viewCustomerMenuDesign();
                                        int view_customer_input = Integer.parseInt(ob.br.readLine());

                                        switch (view_customer_input) {
                                            case 1:
                                                ob.viewcus.viewParticularCustomer();
                                                break;
                                            case 2:
                                                ob.viewcus.viewAllCustomer();
                                                break;
                                            case 3:
                                                view_customer_loop = false;
                                                break;
                                        }
                                    }
                                    break;

                                case 3:
                                    ob.addcus.updateCustomer();
                                    break;

                                case 4:
                                    System.out.println("Enter phone number to place order: ");
                                    String entered_customer_phoneNo = ob.br.readLine();
                                    for (int i = 0; i < ob.kd.customer_details.length; i++) {
                                        if (ob.kd.customer_details[i].customer_phoneNo.equals(entered_customer_phoneNo)) {
                                            ob.ord_det.order_details = Arrays.copyOf(ob.ord_det.order_details, ob.ord_det.order_details.length + 1);
                                            ob.ord_det.order_details[ob.ord_det.order_details.length - 1] = ob.ord.addOrder(entered_customer_phoneNo);
                                            Design.menuAfterOrder1Design();
                                            
                                            int payment_input = Integer.parseInt(ob.br.readLine());
                                            switch (payment_input) {
                                                case 1:
                                                    Design.menuAfterOrder2Design();
                                                    int pay_now_input = Integer.parseInt(ob.br.readLine());
                                                    switch (pay_now_input) {
                                                        case 1:
                                                            ob.pay.payThisOrder( ob.ord_det.order_details[ob.ord_det.order_details.length - 1].customer_id,ob.ord_det.order_details.length - 1);//may be wrong bec of index passed
                                                            // //ob.vieword.order_details[ob.vieword.order_details.length - 1].customer_id
                                                            ob.pay.currentDebit(ob.ord_det.order_details.length - 1);  //new comment it
                                                            break;
                                                        case 2:
                                                            ob.pay.payPartially( ob.ord_det.order_details[ob.ord_det.order_details.length - 1].customer_id);

                                                            ob.pay.currentDebit(ob.ord_det.order_details.length - 1);   //new comment it
                                                            break;
                                                    }
                                                    break;
                                                case 2:
                                                    break;
                                            }
                                            break;  //new
                                        } else {
                                            if (i == ob.kd.customer_details.length - 1) {
                                                System.err.println("Enter Valid Mobile Number...");

                                            }
                                        }
                                    }
                                    break;

                                case 5:
                                    boolean view_order_loop = true;
                                    while (view_order_loop) {
                                        Design.viewOrderMenuDesign();
                                        int view_order_input = Integer.parseInt(ob.br.readLine());
                                        switch (view_order_input) {
                                            case 1:
                                                ob.ord_det.viewParticularOrder();
                                                break;
                                            case 2:
                                                ob.ord_det.viewAllOrder();
                                                break;
                                            case 3:
                                                view_order_loop = false;
                                                break;
                                            
                                        }
                                    }
                                    break;
                                case 6:
                                    int pp_customer_id = 0;
                                    try {
                                        boolean make_payment = true;
                                        while (make_payment) {
                                            System.out.println("Enter Customer ID:");
                                            pp_customer_id = Integer.parseInt(ob.br.readLine());
                                            for (int customer_index = 0; customer_index < ob.kd.customer_details.length; customer_index++) {
                                                if (ob.kd.customer_details[customer_index].customer_id == pp_customer_id) {
                                                    ob.pay.payPartially( pp_customer_id);
                                                    make_payment = false;
                                                }// new use else
                                            }
                                        }
                                    } catch (InputMismatchException ime) {
                                        System.out.println("PLEASE ENTER CORRECT CUSTOMER ID : ");
                                    }
                                    ob.pay.currentDebit(pp_customer_id);
                                    break;
                                case 7:
                                    ob.paydet.showPaymentDetails();
                                    break;
                                case 8:
                                    customer_menu_loop = false;
                                    break;
                            }
                        }

                        break;
                    case 2:
                        boolean inventory_menu_loop = true;
                        while (inventory_menu_loop) {
                            Design.inventoryMenuDesign();
                            int inventory_menu_input = Integer.parseInt(ob.br.readLine());

                            switch (inventory_menu_input) {
                                case 1:
                                    boolean add_product_loop = true;
                                    while (add_product_loop) {
                                        ob.prod.addProduct();
                                        
                                        System.out.println("SUCCESSFULLY ADDED PRODUCT DETAILS....");
                                        System.out.println("To add More Product Press '1' (or) Exit Press '2'");
//                                        try{   //new
                                        
                                            int add_more_product_input = Integer.parseInt(ob.br.readLine());
                                        
                                        switch (add_more_product_input) {
                                            case 1:
                                                add_product_loop = true;
                                                break;
                                            case 2:
                                                add_product_loop = false;
                                                break;
                                        }
//                                        }catch(InputMismatchException imme){
//                                            System.out.println("Please Enter Correct Input::");
//                                        }
                                    }

                                    break;
                                case 2:

                                    boolean view_product_loop = true;
                                    while (view_product_loop) {
                                        Design.viewProductMenuDesign();
                                        int view_product_input = Integer.parseInt(ob.br.readLine());
                                        switch (view_product_input) {
                                            case 1:
                                                ob.prod.viewParticularProduct();
                                                break;
                                            case 2:
                                                ob.prod.viewAllProducts();
                                                break;
                                            case 3:
                                                view_product_loop = false;
                                                break;
                                        }
                                    }
                                    view_product_loop = true;
                                    break;
                                case 3:
                                    ob.prod.topUpProduct();
                                    break;
                                case 4:
                                    ob.prod.updateProduct();
                                    System.out.println("PRODUCT UPDATED SUCCESSFULLY");
                                    break;
                                case 5:
                                    inventory_menu_loop = false;
                                    break;
                            }
                        }

                        break;

                    case 3:
                        boolean statistics_menu_loop = true;
                        while (statistics_menu_loop) {
                            Design.statisticsMenu();
                            int statistics_menu_input = Integer.parseInt(ob.br.readLine());
                            switch (statistics_menu_input) {
                                case 1:
                                    ob.stat.todaysCollection();
                                    break;
                                case 3:
                                    ob.stat.totalCustomerDebits();
                                    break;
                                case 5:
                                    statistics_menu_loop = false;
                                    break;
                            }
                        }
                        statistics_menu_loop = true;
                        break;
                    case 4:
                        System.out.println("order_list length : "+ob.ord_det.order_details.length);
//                        System.out.println("1st order_list its list_item length length : "+ob.ord_det.order_details[1].item_list.length);
//                        System.out.println("2st order_list its list_item length length : "+ob.ord_det.order_details[2].item_list.length);
//                        System.out.println("0st order_list its list_item length length : "+ob.ord_det.order_details[0].item_list.length);
//                        System.out.println("3st order_list its list_item length length : "+ob.ord_det.order_details[3].item_list.length);
                        
//                        System.out.println(ob.vieword.order_details[2].item_list[0].order_id);
//                        System.out.println(ob.vieword.order_details[3].item_list[0].order_id);
//                        System.out.println(ob.vieword.order_details[4].item_list[0].order_id);
//                        System.out.println(ob.vieword.order_details[5].item_list[0].order_id);
//                        System.out.println(ob.vieword.order_details[0].item_list[0].order_id);
//                        System.out.println(ob.vieword.order_details[1].item_list[0].order_id);
                        
                        ob.fw.customerDetatilsWrite();
                        ob.fw.productDetailsWrite();
                        ob.fw.orderWithItemListWrite();
                        ob.fw.paymentDetailsWrite();
                        main_menu_loop = false;
                        break;
                }
                
            } catch (Exception e) {
                System.out.println("Please Enter Correct Input: ");
            }
        }
//        main_menu_loop = true;
    }
}
