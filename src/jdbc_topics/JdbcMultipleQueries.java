package jdbc_topics;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JdbcMultipleQueries {

    public static void main(String[] args) {

        String DB_URL = "jdbc:mysql://localhost:3306/hari?allowMultiQueries=true";
        String USER = "root";
        String PASSWORD = "root";
        String QUERY = "SELECT * FROM Employees";
        String Q1 = "INSERT INTO Employees VALUES (4, 'Jackson', 'L', 'AWS', 'ASSOCIATE')";
        String Q2 = "UPDATE Employees SET technology = 'DBMS'";

        //creating DB connection | | executing statement
        try (Connection connection = DriverManager.getConnection(DB_URL, USER, PASSWORD)) {

            // Load / register driver
            Class.forName("com.mysql.cj.jdbc.Driver");

            DatabaseMetaData metaData = connection.getMetaData();

            ResultSet tables = metaData.getColumns("hari", null, "employees", null);

            while (tables.next()) {
                System.out.println("TABLE METADATA : " + tables.getString("COLUMN_NAME"));
            }

            // creating statement 
            Statement statement = connection.createStatement();

            // 1.  executing multiple queries with single statement object
            boolean execute = statement.execute(Q1 + ";" + Q2);

            System.out.println("Result : " + execute);

            statement.addBatch(Q1);
            statement.addBatch(Q2);

            // 2. executing multiple queries with single statement object
//            int[] executeBatch = statement.executeBatch();
//
//            System.out.println("BATCH result : " + executeBatch);

            // executing statement
            ResultSet resultSet = statement.executeQuery(QUERY);

            //processing the retrived data
            while (resultSet.next()) {
                System.out.println("\n---------------------------------------------------");

                System.out.println("EMPLOYEE ID : " + resultSet.getString("EmployeeID"));
                System.out.println("FIRST NAME : " + resultSet.getString("FirstName"));
                System.out.println("LAST NAME : " + resultSet.getString("LastName"));
                System.out.println("TECHNOLOGY : " + resultSet.getString("Technology"));
                System.out.println("DESIGNATION : " + resultSet.getString("Designation"));
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(JdbcMultipleQueries.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
