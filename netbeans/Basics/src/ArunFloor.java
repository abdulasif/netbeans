import java.util.Scanner;
public class ArunFloor {
    int indexi, indexj, indexk;
    String[][][] house = {
        {},
        //block 1
        {
            //ground 
            {},
            //floor1
            {
                "", "person111", "person112", "person113", "person114"
            },
            //floor2
            {
                "", "person121", "person122", "person123", "person124"
            },
            //floor3
            {
                "", "person131", "person132", "person133", "person134"
            },
            //floor4
            {
                "", "person141", "person142", "person143", "person144"
            }
        },
        //block 2
        {
            //ground 
            {},
            //floor1
            {
                "", "person211", "person212", "person213", "person214"
            },
            //floor2
            {
                "", "person221", "person222", "person223", "person224"
            },
            //floor3
            {
                "", "person231", "person232", "person233", "person234"
            },
            //floor4
            {
                "", "person241", "person242", "person243", "person244"
            }
        },
        //block 3
        {
            //ground 
            {},
            //floor1
            {
                "", "person311", "person312", "person313", "person314"
            },
            //floor2
            {
                "", "person321", "person322", "person323", "person324"
            },
            //floor3
            {
                "", "person331", "person332", "person333", "person334"
            },
            //floor4
            {
                "", "person141", "person342", "person343", "person344"
            }
        },
        //block 4
        {
            //ground 
            {},
            //floor1
            {
                "", "person411", "person412", "person413", "person414"
            },
            //floor2
            {
                "", "person421", "person422", "person423", "person424"
            },
            //floor3
            {
                "", "person431", "person432", "person433", "person434"
            },
            //floor4
            {
                "", "person441", "person442", "person443", "person444"
            }
        },
        //block 5
        {
            //ground 
            {},
            //floor1
            {
                "", "person511", "person512", "person513", "person514"
            },
            //floor2
            {
                "", "person521", "person522", "person523", "person524"
            },
            //floor3
            {
                "", "person531", "person532", "person533", "person534"
            },
            //floor4
            {
                "", "person541", "person542", "person543", "person544"
            }
        },
        //block 6
        {
            //ground 
            {},
            //floor1
            {
                "", "person611", "person612", "person613", "person614"
            },
            //floor2
            {
                "", "person621", "person622", "person623", "person624"
            },
            //floor3
            {
                "", "person631", "person632", "person633", "person634"
            },
            //floor4
            {
                "", "person641", "person642", "person643", "person644"
            }
        }
    };
    ArunFloor getindex(String person) {
        int starti = 0, startj = 0, startk = 0;
        boolean flage = false;
        for (int i = 0; i < house.length; i++) {
            starti++;
            startj = 0;
            for (int j = 0; j < house[i].length; j++) {
                startj++;
                startk = 0;
                for (int k = 0; k < house[i][j].length; k++) {
                    startk++;
                    if (person.equals(house[i][j][k])) {
                        flage = true;
                        break;
                    }
                }
                if (flage) {
                    break;
                }
            }
            if (flage) {
                break;
            }
        }
        ArunFloor obj = new ArunFloor();
        if(flage){
        obj.indexi = starti - 1;
        obj.indexj = startj - 1;
        obj.indexk = startk - 1;
        }else{
            obj.indexi = -1;
            System.out.println("Person not found...");
        }
        return obj;
    }
    public static void main(String arr[]) {
        Scanner scan = new Scanner(System.in);
        ArunFloor appobj = new ArunFloor();
        //printing houses
        for (String i[][] : appobj.house) {
            for (String j[] : i) {
                for (String k : j) {
                    System.out.print(k + " ");
                }
                System.out.println("");
            }
            System.out.print("\n\n");
        }
        System.out.print("Select starting person flate :");
        String start = scan.next();
        ArunFloor startindex = appobj.getindex(start);
       if(startindex.indexi>0)
       {
        System.out.print("Select ending person flate :");
        String end = scan.next();
        ArunFloor endindex = appobj.getindex(end);
      if(endindex.indexi>0)
       {
        int crossingblock = endindex.indexi > startindex.indexi ? endindex.indexi - startindex.indexi : startindex.indexi - endindex.indexi;
        int crossingfloor = endindex.indexi == startindex.indexi ? (startindex.indexj>endindex.indexj?startindex.indexj-endindex.indexj:endindex.indexj-startindex.indexj) : startindex.indexj + endindex.indexj;
        // int crossingdoor=(endindex.indexi==startindex.indexi && startindex.indexj==endindex.indexj)?(endindex.indexk>startindex.indexk?((endindex.indexk-startindex.indexk)-1):((startindex.indexk-endindex.indexk)-1)):(startindex.indexk-1)+(endindex.indexk-1);
        int crossingdoor = (endindex.indexi == startindex.indexi && startindex.indexj == endindex.indexj) ? (endindex.indexk > startindex.indexk ? (endindex.indexk - startindex.indexk) : (startindex.indexk - endindex.indexk)) : ((startindex.indexk + endindex.indexk));
        System.out.println("A person need to cross block :" + (crossingblock) + " floors :" + (crossingfloor) + " doors :" + (crossingdoor));
       }
       }
    }
}
