/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package daoMysql;

import bl.Design;
import dao.CustomerDAO;

import java.io.IOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import model.Customer;

/**
 *
 * @author bas200190
 */
public class CustomerDAOimpl implements CustomerDAO {

    @Override
    public  int setCustomerId(Connection connection)  {
        int customer_id = 0;
        try{
        String query = "select * from KB_customer";
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(query);
        
        while (rs.next()) {
            customer_id = rs.getInt(2);

        }

        if (customer_id != 0) {
            customer_id = customer_id + 1;
        } else {
            customer_id = 501;

        }
        System.out.println(customer_id);
        
        }catch(Exception e){
            e.printStackTrace();
        }
        return customer_id;
    }

    @Override
    public void addCustomer(Connection connection , Customer customer)  {

       
//        boolean add_customer_fullilly_loop = true;
//        while (add_customer_fullilly_loop) {
            
            
            String query = "insert into KB_customer values(default,?,?,?,?,default)";
            try (PreparedStatement prep_statement = connection.prepareStatement(query)) {

                prep_statement.setInt(1, customer.getCustomer_id());
                prep_statement.setString(2, customer.getCustomer_name());
                prep_statement.setString(3, customer.getCustomer_phoneNo());
                prep_statement.setString(4, customer.getCustomer_address());

                int rows_count = prep_statement.executeUpdate();
//                if (rows_count == 1) {
//                    System.out.println("successfully customer added");
//                    add_customer_fullilly_loop = false;
//                }
            } catch (SQLException sqle) {
                System.out.println("This phone number is already there");
                System.out.println("Try with another phone number");
            }

//        }
    }

    @Override
    public void updateCustomer(Connection connection) {
        try {
            System.out.println("Enter the Customer's Phone Number:");
            String uc_customer_phoneNo = ObjectsDatabase.br.readLine();
            System.out.println("Enter the Customer's ID:");
            int uc_customer_id = Integer.parseInt(ObjectsDatabase.br.readLine());

            String query = "select * from KB_customer where customer_id = ?  && customer_phone_no = ?";
            try (PreparedStatement ps = connection.prepareStatement(query)) {
                ps.setInt(1, uc_customer_id);
                ps.setString(2, uc_customer_phoneNo);

                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {

                        int update_customer_id = rs.getInt(2);
                        System.out.println("cus_id : " + update_customer_id);

                        System.out.println("*****Enter Customer's New Details*****");
                        System.out.println("Enter the Customer Name:");
                        String update_customer_name = ObjectsDatabase.br.readLine();
                        System.out.println("Enter the Phone Number:");
                        String update_customer_phoneNo = ObjectsDatabase.br.readLine();
                        System.out.println("Enter the Customer Address:");
                        String update_customer_address = ObjectsDatabase.br.readLine();

                        String query_2 = "update KB_customer set customer_name = ? ,  customer_phone_no = ? ,customer_address = ? where customer_id = ?";
                        try (PreparedStatement ps_2 = connection.prepareStatement(query_2)) {
                            ps_2.setString(1, update_customer_name);
                            ps_2.setString(2, update_customer_phoneNo);
                            ps_2.setString(3, update_customer_address);
                            ps_2.setInt(4, update_customer_id);

                            int update_returns = ps_2.executeUpdate();
                            System.out.println(update_returns + " query is updated ");
                        }

                    }
                }
            }

        } catch (IOException | NumberFormatException | SQLException e) {
            System.out.println("Update customer is not performed..");
        }
    }

    @Override
    public void viewAllCustomer(Connection connection) {
        try {
            Design.viewCustomerDesign();
            String query = "select * from KB_customer";
            try (Statement statement = connection.createStatement(); ResultSet rs = statement.executeQuery(query)) {
                while (rs.next()) {
                    Date customer_joining_date = rs.getDate(1);
                    int customer_id = rs.getInt(2);
                    String customer_name = rs.getString(3);
                    String customer_phoneNo = rs.getString(4);
                    String customer_address = rs.getString(5);
                    int customer_debit = rs.getInt(6);
                    System.out.format("\n|%-15s|%-20d|%-20s|%-20s|%-25s|%-20s|", customer_joining_date, customer_id, customer_name, customer_phoneNo, customer_address, customer_debit);
                    System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(25) + "+" + "-".repeat(20) + "+");
                }

            }

            System.out.println("");
            System.out.println("");
            System.out.println("");
        } catch (SQLException e) {
            System.out.println("view all order is not done..");
        }

    }

    /**
     *
     * @param connection
     */
    @Override
    public void viewParticularCustomer(Connection connection) {
        try {
            System.out.println("Enter the Customer's Phone Number:");
            String vpc_customer_phoneNo = ObjectsDatabase.br.readLine();
            System.out.println("Enter the Customer ID:");
            int vpc_customer_id = Integer.parseInt(ObjectsDatabase.br.readLine());

            String query = "select * from KB_customer where customer_id = ?  && customer_phone_no = ?";
            try (PreparedStatement ps = connection.prepareStatement(query)) {
                ps.setInt(1, vpc_customer_id);
                ps.setString(2, vpc_customer_phoneNo);

                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        Design.viewCustomerDesign();
                        Date customer_joining_date = rs.getDate(1);
                        int customer_id = rs.getInt(2);
                        String customer_name = rs.getString(3);
                        String customer_phoneNo = rs.getString(4);
                        String customer_address = rs.getString(5);
                        int customer_debit = rs.getInt(6);
                        System.out.format("\n|%-15s|%-20d|%-20s|%-20s|%-25s|%-20s|", customer_joining_date, customer_id, customer_name, customer_phoneNo, customer_address, customer_debit);
                        System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(25) + "+" + "-".repeat(20) + "+");
                    } else {
                        System.out.println("please provide valid data of customer");
                    }
                }
            }

            System.out.println("");
            System.out.println("");
            System.out.println("");
        } catch (IOException | NumberFormatException | SQLException e) {
            System.out.println("View Particular Order is not Done..");
        }
    }

}
