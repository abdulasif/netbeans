/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package dao;

import daoMysql.CustomerDAOimpl;
import daoMysql.ItemListDAOimpl;
import daoMysql.OrdersDAOimpl;
import daoMysql.PaymentDAOimpl;
import daoMysql.ProductDAOimpl;
import daoMysql.StatisticsDAOimpl;

/**
 *
 * @author bas200190
 */
public interface KhataBookDAO {

    public CustomerDAOimpl getCustomerDAOimpl();

    public ProductDAOimpl getProductDAOimpl();

    public ItemListDAOimpl getItemListDAOimpl();

    public OrdersDAOimpl getOrdersDAOimpl();

    public PaymentDAOimpl getPaymentDAOimpl();
    
    public StatisticsDAOimpl getStatisticsDAOimpl();
}
