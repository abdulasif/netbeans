/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package inheritance;

/**
 *
 * @author bas200190
 */
public class ParentClass {
   private void PClass()
    {
         System.out.println("This is Parent class.");
    } 
     
    public static void main(String[] args) {
        ParentClass a = new ParentClass();
        a.PClass();    //1
    }
}
