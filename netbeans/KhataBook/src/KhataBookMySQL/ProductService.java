/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhataBookMySQL;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author bas200190
 */
public class ProductService {

    public int setProductIdBefObj(Connection connection) throws Exception {

        String query = "select * from KB_product";
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(query);
        int product_id = 0;
        while (rs.next()) {
            product_id = rs.getInt(1);

        }

        if (product_id != 0) {
            product_id = product_id + 1;
        } else {
            product_id = 901;

        }
        System.out.println(product_id);
        return product_id;
    }

    public void addProduct(Connection connection) throws Exception {
        try {
            boolean add_product_fullfill_loop = true;
            while (add_product_fullfill_loop) {
                String query = "insert into KB_product values(?,?,?,?,?,?,?)";

                System.out.println("*****Enter Product Details*****");

                System.out.println("Enter Product ID:");
                int product_id = setProductIdBefObj(connection);
                System.out.println("Enter Product Name:");
                String product_name = KhataBook.ob.br.readLine();
                System.out.println("Enter Product Weight:");
                String product_weight = KhataBook.ob.br.readLine();
                System.out.println("Enter Product Buying Price:");
                double product_buying_price = Double.parseDouble(KhataBook.ob.br.readLine());
                System.out.println("Enter Product Selling Price:");
                double product_selling_price = Double.parseDouble(KhataBook.ob.br.readLine());
                System.out.println("Enter Product Brand:");
                String product_brand = KhataBook.ob.br.readLine();
                System.out.println("Enter Product Quantity:");
                int product_quantity = Integer.parseInt(KhataBook.ob.br.readLine());

                Product product = new Product(product_id, product_name, product_weight, product_buying_price, product_selling_price, product_brand, product_quantity);

                try (PreparedStatement ps = connection.prepareStatement(query);) {
                    ps.setInt(1, product.getProduct_id());
                    ps.setString(2, product.getProduct_name());
                    ps.setString(3, product.getProduct_weight());
                    ps.setDouble(4, product.getProduct_buying_price());
                    ps.setDouble(5, product.getProduct_selling_price());
                    ps.setString(6, product.getProduct_brand());
                    ps.setInt(7, product.getProduct_quantity());

                    int rows_count = ps.executeUpdate();
                    if (rows_count == 1) {
                        System.out.println(rows_count + " product is added successfully");
                        add_product_fullfill_loop = false;
                    }
                }
            }
        } catch (IOException | NumberFormatException | SQLException ioe) {
            System.out.println("product not added");
        }
    }

    public void viewAllProducts(Connection connection) {
        try {
            Design.viewProductDesign();

            String query = "select * from KB_product";
            try (Statement statement = connection.createStatement(); ResultSet rs = statement.executeQuery(query);) {
                while (rs.next()) {
                    int product_id = rs.getInt(1);
                    String product_name = rs.getString(2);
                    String product_weight = rs.getString(3);
                    double product_buying_price = rs.getDouble(4);
                    double product_selling_price = rs.getDouble(5);
                    String product_brand = rs.getString(6);
                    int product_quantity = rs.getInt(7);

                    System.out.format("\n|%-20d|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|", product_id, product_name, product_weight, product_buying_price, product_selling_price, product_brand, product_quantity);
                    System.out.print("\n+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");

                }
            }
            System.out.println("");
            System.out.println("");
        } catch (SQLException e) {
            System.out.println("View all products is not done..");
        }
    }

    public void viewParticularProduct(Connection connection) {
        try {
            System.out.println("Enter the Product ID:");
            int vpp_product_id = Integer.parseInt(KhataBook.ob.br.readLine());

            String query = "select * from KB_product where product_id = ?";

            try (PreparedStatement ps = connection.prepareStatement(query)) {
                ps.setInt(1, vpp_product_id);

                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        int product_id = rs.getInt(1);
                        String product_name = rs.getString(2);
                        String product_weight = rs.getString(3);
                        double product_buying_price = rs.getDouble(4);
                        double product_selling_price = rs.getDouble(5);
                        String product_brand = rs.getString(6);
                        int product_quantity = rs.getInt(7);

                        Design.viewProductDesign();
                        System.out.format("\n|%-20d|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|", product_id, product_name, product_weight, product_buying_price, product_selling_price, product_brand, product_quantity);
                        System.out.print("\n+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");

                        System.out.println("");
                        System.out.println("");
                    } else {
                        System.out.println("please enter valid product data");
                    }
                }
            }
        } catch (IOException | NumberFormatException | SQLException e) {
            System.out.println("View particular products is not done..");
        }
    }

    public void updateProduct(Connection connection) {

        try {

            System.out.println("Enter the product ID:");
            int up_product_id = Integer.parseInt(KhataBook.ob.br.readLine());

            String query = "select * from KB_product where product_id = ?";
            try (PreparedStatement ps = connection.prepareStatement(query)) {
                ps.setInt(1, up_product_id);

                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {

                        int update_product_id = rs.getInt(1);

                        System.out.println("*****Enter Product's New Details*****");
                        System.out.println("Enter the Product Name:");
                        String product_name = KhataBook.ob.br.readLine();
                        System.out.println("Enter Product Weight:");
                        String product_weight = KhataBook.ob.br.readLine();
                        System.out.println("Enter Product Buying Price:");
                        double product_buying_price = Double.parseDouble(KhataBook.ob.br.readLine());
                        System.out.println("Enter Product Selling Price:");
                        double product_selling_price = Double.parseDouble(KhataBook.ob.br.readLine());
                        System.out.println("Enter Product Brand:");
                        String product_brand = KhataBook.ob.br.readLine();
                        System.out.println("Enter Product Quantity:");
                        int product_quantity = Integer.parseInt(KhataBook.ob.br.readLine());

                        String query_2 = "update KB_product set product_name = ? ,  product_weight = ? , product_buying_price = ? , product_selling_price = ? , product_brand = ? , product_quantity = ? where product_id = ?";
                        try (PreparedStatement ps_2 = connection.prepareStatement(query_2)) {
                            ps_2.setString(1, product_name);
                            ps_2.setString(2, product_weight);
                            ps_2.setDouble(3, product_buying_price);
                            ps_2.setDouble(4, product_selling_price);
                            ps_2.setString(5, product_brand);
                            ps_2.setInt(6, product_quantity);
                            ps_2.setInt(7, update_product_id);

                            int update_returns = ps_2.executeUpdate();
                            System.out.println(update_returns + " query is updated ");
                        }

                    }
                }
            }

        } catch (IOException | NumberFormatException | SQLException e) {
            System.out.println("Product is not Updated..");
        }
    }

    public void topUpProduct(Connection connection) {
        try {
            System.out.println("Enter Product ID to Top-Up the Product Quantity : ");
            int product_id = Integer.parseInt(KhataBook.ob.br.readLine());

            String query = "select * from KB_product where product_id = ?";

            try (PreparedStatement ps = connection.prepareStatement(query);) {
                ps.setInt(1, product_id);

                try (ResultSet rs = ps.executeQuery();) {
                    if (rs.next()) {
                        int available_quantity = rs.getInt(7);
                        System.out.println("Available Quantity: " + available_quantity);

                        System.out.println("Enter The Quantity To Add :");
                        int quantity = Integer.parseInt(KhataBook.ob.br.readLine());

                        String query_1 = "update KB_product set product_quantity = ? where product_id = ?";

                        PreparedStatement ps_1 = connection.prepareStatement(query_1);
                        available_quantity = available_quantity + quantity;
                        ps_1.setInt(1, available_quantity);
                        ps_1.setInt(2, product_id);
                        int result = ps_1.executeUpdate();
                        if (result == 1) {
                            System.out.println("Updated Successfully..");
                        }
                    } else {
                        System.out.println("Please Enter Correct Product ID..");
                    }
                }
            }
        } catch (IOException | NumberFormatException | SQLException e) {
            System.out.println("Top-Up products is not done..");
        }
    }
}
