package khatabookasif;


import java.util.Arrays;
import java.util.Scanner;


public class KhataBook {
    static boolean customerMenuLoop  =true;
    static boolean mainMenuLoop = true;
    
    
    static ObjectsArray oa = new ObjectsArray();
    public static void main(String[] args) {
        while (mainMenuLoop) {
            customerMenuLoop = true;
            System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(15) + "+");
            System.out.format("\n|%-5s|%-15s|", "S.NO", "MAIN MENU");
            System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(15) + "+");
            String[] MainMenu = {"Customer", "Inventory", "Statistics", "Exit"};
            for (int i = 0; i < MainMenu.length; i++) {

                System.out.format("\n|%-5d|%-15s|", (i + 1), MainMenu[i]);
            }
            System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(15) + "+");
            System.out.println("");
            System.out.print("select option:");
            Scanner sc = new Scanner(System.in);
            int menuInput = sc.nextInt();
                    
            switch (menuInput) {
                case 1:
                    
                    while (customerMenuLoop) {
                        String[] customerMenu = {"Add Customer", "View Customer", "View All Customer", "Update Customer", "Place Order","View All Orders","Make Payment", "Exit"};
                        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(18) + "+");
                        System.out.format("\n|%-5s|%-18s|", "S.NO", "CUSTOMER MENU");
                        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(18) + "+");
                        for (int i = 0; i < customerMenu.length; i++) {

                            System.out.format("\n|%-5d|%-18s|", (i + 1), customerMenu[i]);
                        }
                        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(18) + "+");
                        System.out.println("");
                        System.out.print("select option:");
                        int customerInput = sc.nextInt();
                        oa.cus.customer_details[0] = new Customer(501, "asif", "8190877860", "12345", "chepauk");
                        oa.cus.customer_details[1] = new Customer(502, "ismath", "6369466973", "54321", "triplicane");
                        oa.cus.customer_details[2] = new Customer(503, "lathif", "9944198786", "09876", "mount_road");
                        
                        oa.inv.product_details_arr[0] = new Inventory(701,"maggie","200g",20,25,"nestle","ai_enterprises",200);
                        oa.inv.product_details_arr[1] = new Inventory(702,"marrie_biscuit","100g",8,10,"britania","bismi_enterprises",300);
                        oa.inv.product_details_arr[2] = new Inventory(703,"chat_masala","50g",4,10,"everest","ai_enterprises",200);
                        oa.inv.product_details_arr[3] = new Inventory(704,"detergent_powder","1000g",50,65,"tide","star_company",50);
                        oa.inv.product_details_arr[4] = new Inventory(705,"bread","250g",18,30,"modern","bismi_enterprises",30);
                        switch (customerInput) {
                            case 1:
 
                                oa.cus.addCustomer();
                                break;
                            case 2:
                                
                            case 3:
                                System.out.println("CUSTOMER DETAILS");
                                oa.cus.viewCustomer();
                                break;
                            case 4:
                                
                                boolean updateCustomerLoop = true;
                                while (updateCustomerLoop) {
                                    System.out.println("Enter the Customers Old Phone Number:");
                                    String customer_oldPhNo = sc.next();
                                    for (int k = 0; k < oa.cus.customer_details.length; k++) {
                                        if (customer_oldPhNo.equals(oa.cus.customer_details[k].customer_phoneNo)) {
                                            System.out.println("successfully matched one customer & Enter new Details:");
                                            System.out.println("Enter new name:");
                                            oa.cus.customer_details[k].customer_name = sc.next();
                                            System.out.println("Enter new phone number:");
                                            oa.cus.customer_details[k].customer_phoneNo = sc.next();
                                            System.out.println("Enter new aadhar number:");
                                            oa.cus.customer_details[k].customer_aadharNo = sc.next();
                                            System.out.println("Enter new address:");
                                            oa.cus.customer_details[k].customer_address = sc.next();
                                            System.out.println("SUCCESFULLY UPDATED CUSTOMER DETAILS...");
                                            updateCustomerLoop = false;
                                            break;
                                        }
                                    }
                                }
                                break;
                            case 5:
                                oa.lo.addOrder();
                                //orders[orders.length+1] =  oa.lo.line_order_details[oa.lo.line_order_details.length-1];
                                break;
                            case 6:
                                oa.li.viewAllLineItem();
                                //oa.li.viewAllOrders();
                            case 7:
                                
                            case 8:
                                customerMenuLoop = false; 
                            
                        }
                    }
                    break;
                case 2:
                    boolean inventoryMenuLoop = true;
                    
                    while(inventoryMenuLoop){
                      System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(15) + "+");
                      System.out.format("\n|%-5s|%-15s|", "S.NO", "INVENTORY MENU");
                      System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(15) + "+");
                      String[] inventoryMenu = {"Add Products","View Products","Delete Products","Update Products","Exit"};
                      for (int j = 0; j < inventoryMenu.length; j++) {

                            System.out.format("\n|%-5d|%-15s|", (j + 1), inventoryMenu[j]);
                      }
                      System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(15) + "+");
                      System.out.println("");
                      System.out.print("select option:");
                      
                      int inventoryInput = sc.nextInt();
                      switch(inventoryInput){
                          case 1:
                            oa.inv.addProduct();
                            System.out.println("PRODUCT ADDED SUCCESSFULLY....");
                              break;
                          case 2:
                            oa.inv.viewProducts();
                            break;
                          case 3:
                            oa.inv.deleteProduct();
                          case 4:
                            oa.inv.updateProduct();
                          case 5:
                            inventoryMenuLoop = false;
                          
                        }
                    }  
                    break;
                case 4:
                    mainMenuLoop = false;
                    break;
                
            }
            
        }
    }
}




//                                    System.out.println("Enter the Customers Old Phone Number to Place Order:");
//                                    String customer_oldPhNo = sc.next();
//                                    for (int k = 0; k < oa.cus.customer_details.length; k++) {
//                                        if (customer_oldPhNo.equals(oa.cus.customer_details[k].customer_phoneNo)) {
//                                            System.out.println("ALL PRODUCTS ARE....");
//                                            oa.inv.viewProducts();
//                                            boolean productAndProductQuantityLoop = true;
//                                            while (productAndProductQuantityLoop) {
//                                                System.out.println("Enter the product ID:");
//                                                int placeOrderProductId = sc.nextInt();
//                                                System.out.println("Enter How Many quantity you Need:");
//                                                int placeOrderProductQuantity = sc.nextInt();
//                                                for (int j = 0; j < oa.inv.product_details_arr.length; j++) {
//                                                    if (placeOrderProductId == oa.inv.product_details_arr[j].product_id) {
//                                                        if (oa.inv.product_details_arr[j].product_quantity - placeOrderProductQuantity > 0) {
//                                                            oa.inv.product_details_arr[j].product_quantity = oa.inv.product_details_arr[j].product_quantity - placeOrderProductQuantity;
//                                                            LineItem l1 = oa.li.placeLineItem(oa.cus.customer_details[k].customer_id,oa.cus.customer_details[k].customer_phoneNo,oa.inv.product_details_arr[j].product_id,oa.inv.product_details_arr[j].product_name,oa.inv.product_details_arr[j].product_selling_price,placeOrderProductQuantity,(oa.inv.product_details_arr[j].product_selling_price*placeOrderProductQuantity));        
//                                                            System.out.print("\n+"+"-".repeat(5)+"+"+"-".repeat(20)+"+");
//                                                            System.out.format("\n|%-5s|%-20s|","1","QUIT");
//                                                            System.out.print("\n+"+"-".repeat(5)+"+"+"-".repeat(20)+"+");
//                                                            System.out.format("\n|%-5s|%-20s|","2","ADD ON PRODUCTS");
//                                                            System.out.print("\n+"+"-".repeat(5)+"+"+"-".repeat(20)+"+");
//                                                            System.out.println("");
//                                                            System.out.println("select option:");
//                                                            int placeOrderRepeat = sc.nextInt();
//                                                            switch(placeOrderRepeat){
//                                                                case 1:
//                                                                    productAndProductQuantityLoop = false;
//                                                                    break;
//                                                                case 2:
//                                                                    productAndProductQuantityLoop = true;
//                                                                    break;
//                                                            }
//                                                            
//                                                        } else {
//                                                            System.out.println("DONT ENTER THE QUANTITY MORE THAN "+oa.inv.product_details_arr[j].product_quantity+" AND LESS THAN "+0);
//                                                        }
//                                                    }
//                                                }
//                                            }
//
//                                        }
//                                    }
//                                    break;
