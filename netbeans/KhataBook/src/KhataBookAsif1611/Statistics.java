/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhataBookAsif1611;


import java.time.LocalDate;



/**
 *
 * @author bas200190
 */
public class Statistics {
    
    public void todaysCollection(){
        String today_date = LocalDate.now()+"";
        double today_collection = 0;
        for (int i = 0; i < KhataBook.ob.paydet.payment_details.length; i++) {
            String temp = KhataBook.ob.paydet.payment_details[i].payment_date+"";
            if (today_date.equals(temp)) {
                today_collection = today_collection + KhataBook.ob.paydet.payment_details[i].paid_amount;
            }
        }
        System.out.println("TODAY'S COLLECTION : "+today_collection);
    }
    
   
    public void totalCustomerDebits(){
        double total_debits = 0;
        for (int i = 0; i < KhataBook.ob.kd.customer_details.length; i++) {
            total_debits = total_debits + (KhataBook.ob.kd.customer_details[i].customer_debit);
        }
        System.out.println("TOTAL DEBITS : "+total_debits);
    }
    //public void currentStockPrice(){}
    //public void highSellingProductsList(){}
}
