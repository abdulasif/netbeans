
class ResistorColorDuo {
    static int Concat(int[] a) {
        String s1 = Integer.toString(a[0]);
        String s2 = Integer.toString(a[1]);
        String s = s1 + s2;
        int c = Integer.parseInt(s);
        return c;
    }
    
    int value(String[] colors) {
        int[] output = new int[2];
        String[] colorsList = {"Black",
            "Brown",
            "Red",
            "Orange",
            "Yellow",
            "Green",
            "Blue",
            "Violet",
            "Grey",
            "White"};
        for (int j = 0; j < 2; j++) {
            for (int i = 0; i < colorsList.length; i++) {
                if (colors[j].equalsIgnoreCase(colorsList[i])) {
                    output[j] = i;
                }
            }
        }
         int final_output = Concat(output);
         return final_output;
    }
    
    public static void main(String[] args) {
        ResistorColorDuo ex1 = new ResistorColorDuo();
        String[] a = {"Blue","Grey"};
        System.out.println(ex1.value(a));
    }
}
