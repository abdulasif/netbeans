import java.util.Scanner;
public class sort1 {
    static void selectionSort(int[] anew)   //int[] a = {10,5,12,2,1,9};
    {
        for (int i = 0; i < anew.length; i++) {
            int min = i;
            for (int j = i+1; j < anew.length; j++) {
                if (anew[min]>anew[j]) {
                   min = j;
                   int temp = anew[i];
                   anew[i] = anew[min];
                   anew[min] = temp;
                }
            }
        }
        for(int h : anew)
        {
            System.out.println(h);
        }
    }
    static void insertionSort(int[] anew)
    {
      for(int i=0;i<anew.length-1;i++)
      {
          if (anew[i]>anew[i+1]) {
              int now = anew[i];
              anew[i] = anew[i+1];
              anew[i+1] = now;
              for (int j = i; j >= 1; j--) {
                  if (anew[j-1]>anew[j]) {
                      int now1 = anew[j];
                      anew[j] = anew[j-1];
                      anew[j-1] = now1;
                  }
              }
          }
      }
      for(int h : anew)
        {
            System.out.println(h);
        }
    }
    static void bubbleSort(int[] anew)
    {
        for (int i = 0; i < anew.length; i++) {
            for (int j = 0; j < anew.length-1; j++) {
                if(anew[j]>anew[j+1])
                {
                  int now = anew[j];
                  anew[j] = anew[j+1];
                  anew[j+1] = now;
                }
            }
        }
        for(int g : anew)
        {
            System.out.println(g);
        }
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the size of the array:");
        int size = sc.nextInt();
        int[] a = new int[size];
        System.out.println("Enter the Elements:");
        for (int i = 0; i < a.length; i++) {
            a[i] = sc.nextInt();
        }
        boolean an = true;
        while(an)
        {
          System.out.println("Select the method:"
                + "click 1: Bubble Sort. click 2: Insertion Sort. click 3: Selection Sort");
          int select = sc.nextInt();  
          switch(select)
        {
            case 1:{
                bubbleSort(a);
                an =false;
                break;
            }
            case 2:{
                insertionSort(a);
                an=false; 
                break;
            }
            case 3:{
                selectionSort(a);
                an=false;
                break;
            }
            default:{
                System.out.println("please enter the valid number:");
            }
        }
        }
    }
}