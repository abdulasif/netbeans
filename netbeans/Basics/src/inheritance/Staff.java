
package inheritance;

/**
 *
 * @author bas200190
 */
public class Staff extends Person{
    private String school;
    private double pay;
    
    public Staff(String name,String address,String school,double pay){
     super(name,address);
     this.school = school;
     this.pay = pay;
    }
    
    public String getSchool(){
      return this.school;
    }
    
    public void setSchool(String school){
      this.school = school;
    }
    
    public double getPay(){
      return this.pay;
    }
    
    public void setPay(double pay){
      this.pay = pay;
    }
    @Override
    public String toString(){
      return super.getName()+" "+super.getAddress()+" "+this.school+" "+this.pay;
    }
    
    public static void main(String[] args) {
        Staff st1 = new Staff("muthu","thiruvannamalai","veltech",35000);
        System.out.println(st1.getName()+" "+st1.getAddress()+" "+st1.getSchool()+" "+st1.getPay());
        System.out.println(st1);
    }
}
