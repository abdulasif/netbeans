/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bl;

import java.sql.Connection;
import dao.KhataBookDAO;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import model.Customer;
import model.ItemList;
import model.Orders;
import model.Product;

/**
 *
 * @author bas200190
 */
public class OrdersBL {

    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public static Orders addOrder(KhataBookDAO kbd, Connection connection) {
        Orders order = null;
        try {
            System.out.println("Enter phone number to place order: ");
            String entered_customer_phoneNo = br.readLine();

            Customer customer_details = kbd.getOrdersDAOimpl().customerDetailsWRTPhoneNo(entered_customer_phoneNo, connection);

            if (Objects.nonNull(customer_details)) {
                List<ItemList> temp_item_list_arr = new ArrayList<>();

                kbd.getProductDAOimpl().viewAllProducts(connection);

                System.out.println(" Order ID : ");
                int order_id = kbd.getOrdersDAOimpl().setOrderId(connection);

                double temp_total_price_for_order = 0;
                double temp_customer_debit = customer_details.getCustomer_debit();

                boolean purchase_loop = true;
                while (purchase_loop) {
                    int product_id = 0;
                    int quantity_need = 0;
                    try{
                    System.out.println("Enter the product ID:");
                    product_id = Integer.parseInt(br.readLine());
                    System.out.println("Enter How Many quantity you Need:");
                    quantity_need = Integer.parseInt(br.readLine());
                    }catch(IOException | NumberFormatException e){
                    
                    }
                    Product product_details = kbd.getOrdersDAOimpl().productDetailsWRTProductId(product_id, connection);

                    if (Objects.nonNull(product_details)) {
                        int available_quantity = product_details.getProduct_quantity();

                        int product_quantity = available_quantity - quantity_need;
                        if (product_quantity > 0 && product_quantity!=0) {
                            kbd.getOrdersDAOimpl().updateProductAfterPurchase(product_id, product_quantity, connection);

                            int item_list_id = 1;
                            double each_price = product_details.getProduct_selling_price();
                            double price = ((double) quantity_need) * (each_price);

                            ItemList temp_item_list = new ItemList(item_list_id, order_id, product_id, quantity_need, each_price, price);
                            temp_item_list_arr.add(temp_item_list);
                            temp_customer_debit = temp_customer_debit + price;

                            temp_total_price_for_order = temp_total_price_for_order + price;

                            item_list_id++;

                            Design.repeatPurchaseDesign();
                            int repeat_purchase_input = Integer.parseInt(br.readLine());
                            switch (repeat_purchase_input) {
                                case 1:
                                    purchase_loop = false;
                                    break;
                                case 2:
                                    purchase_loop = true;
                                    break;
                            }

                        } else {
                            System.out.println("DONT ENTER THE QUANTITY MORE THAN " + available_quantity + " AND LESS THAN " + 0);
                        }
                    } else {
                        System.out.println("Please Enter Valid Product Id..");
                    }

                }
                kbd.getOrdersDAOimpl().updateCustomerAfterPurchase(customer_details.getCustomer_id(), temp_customer_debit, connection);

                order = new Orders(order_id, customer_details.getCustomer_id(), temp_total_price_for_order, temp_item_list_arr);

                Design.menuAfterOrder1Design();
                int payment_input = Integer.parseInt(br.readLine());
                switch (payment_input) {
                    case 1:
                        Design.menuAfterOrder2Design();
                        int pay_now_input = Integer.parseInt(br.readLine());
                        switch (pay_now_input) {
                            case 1:
                                kbd.getPaymentDAOimpl().payThisOrder(customer_details.getCustomer_id(), temp_customer_debit, connection);
                                break;

                            case 2:
                                kbd.getPaymentDAOimpl().payPartially(customer_details.getCustomer_id(), connection);
                                break;
                        }
                        break;
                    case 2:
                        break;
                }

            } else {
                System.out.println("Enter valid customer phone number...");
            }

        } catch (IOException e) {
            System.out.println("add order is not done..");
        }
        return order;
    }
}
