/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package filelearning;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


/**
 *
 * @author bas200190
 */
public class FileFolderDemo {
    public static void main(String[] args) {
        File file = new File("//home//bas200190//asif_file//asif.txt");  
//        boolean file_present = file.exists();
//        System.out.println("file present = "+file_present);
        try {
            //file.createNewFile();

            FileWriter fw = new FileWriter("//home//bas200190//asif_file//asif.txt");
            fw.write("Hi Iam Asif  developer");
            fw.flush();
            fw.close();
            FileReader fr = new FileReader("//home//bas200190//asif_file//asif.txt");
//            int output = fr.read();
//            while(output!=-1){
//                System.out.print((char)output);
//                output = fr.read();
//            }

            char[] chArr = new char[(int)file.length()];
            fr.read(chArr);
            for (char c1 : chArr) {
                System.out.print(c1);
            }
            fr.close();
            } catch (IOException e) {
            e.printStackTrace();
        }
        
        
    }
}
