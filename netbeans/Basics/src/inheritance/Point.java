/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package inheritance;

import java.util.Arrays;

/**
 *
 * @author bas200190
 */
public class Point {
    private float x;
    private float y;
    
    
    public Point(){
      this(0.0f,0.0f);
    }
    
    public Point(float x,float y){
      this.x = x;
      this.y = y;
    }
    
    
    public float getX(){
      return this.x;
    }
    
    public void setX(float x){
      this.x = x;
    }
    
    public float getY(){
      return this.y;
    }
    
    public void setY(float y){
      this.y = y;
    }
    
    public float[] getXY(){
      float[] a = new float[2];
      a[0]=this.x;
      a[1]=this.y;
      return a;
    }
    
    public void setXY(float x,float y){
      this.x = x;
      this.y = y;
    }
    @Override
    public String toString(){
     return "("+getX()+","+getY()+")";
    }
    
    public static void main(String[] args) {
        Point p1 = new Point();
        System.out.println(p1);
        System.out.println(p1.getX());
        System.out.println(p1.getY());
        float[] p1xy = p1.getXY();
        System.out.println(Arrays.toString(p1xy));
        p1.setXY(3f, 8f);
         System.out.println(p1);
        System.out.println(p1.getX());
        System.out.println(p1.getY());
        float[] pxy = p1.getXY();
        System.out.println(Arrays.toString(pxy));
        
        
        Point p2 = new Point(4.5f,5.6f);
        System.out.println(p2);
        System.out.println(p2.getX());
        System.out.println(p2.getY());
        float[] p2xy = p2.getXY();
        System.out.println(Arrays.toString(p2xy));
    }
}
