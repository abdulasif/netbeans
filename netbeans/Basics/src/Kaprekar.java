
import java.util.Scanner;

public class Kaprekar {

    public static int ascendingInt(int inp) {

        int b = inp;
        int[] arr = new int[4];
        for (int i = 0; i < 4; i++) {
            arr[i] = b % 10;
            b = b / 10;

        }
        int[] asc_arr = arr;
        for (int i = 0; i < asc_arr.length; i++) {
            for (int j = 0; j < asc_arr.length - 1; j++) {
                if (asc_arr[j] > asc_arr[j + 1]) {
                    int temp = asc_arr[j];
                    asc_arr[j] = asc_arr[j + 1];
                    asc_arr[j + 1] = temp;
                }
            }
        }
        int asc_temp = 0;
        for (int i = 0; i < asc_arr.length; i++) {
            asc_temp = asc_temp * 10 + asc_arr[i];
        }
        return asc_temp;

    }

    public static int descendingInt(int inp) {
        int b = inp;
        int[] arr = new int[4];
        for (int i = 0; i < 4; i++) {
            arr[i] = b % 10;
            b = b / 10;

        }
        int[] des_arr = arr;
        for (int i = 0; i < des_arr.length; i++) {
            for (int j = 0; j < des_arr.length - 1; j++) {
                if (des_arr[j] < des_arr[j + 1]) {
                    int temp = des_arr[j];
                    des_arr[j] = des_arr[j + 1];
                    des_arr[j + 1] = temp;
                }
            }
        }
        int des_temp = 0;
        for (int i = 0; i < des_arr.length; i++) {
            des_temp = des_temp * 10 + des_arr[i];
        }
        return des_temp;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean flag = true;
        int inp = 0;
        while (flag) {
            System.out.println("Enter the number between 1000 and 10000. And it should be min two different numbers.");
            inp = sc.nextInt();
            if (inp>=10 && inp<10000 && inp != 1111 && inp != 2222 && inp != 3333 && inp != 4444 && inp != 5555 && inp != 6666 && inp != 7777 && inp != 8888 && inp != 9999 && inp != 0000) {
                flag = false;
            }
        }
        int count = 0;
        while (inp != 6174) {
            inp = descendingInt(inp) - ascendingInt(inp);
            count++;
        }
        System.out.println(count);
    }
}
