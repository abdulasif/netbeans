/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package dao;

import java.sql.Connection;
import model.Product;

/**
 *
 * @author bas200190
 */
public interface ProductDAO {
    public int setProductId(Connection connection);
    public void addProduct(Product product , Connection connection);
    public void updateProduct(Connection connection);
    public void viewAllProducts(Connection connection);
    public void viewParticularProduct(Connection connection);
    public void topUpProduct(Connection connection);
}
