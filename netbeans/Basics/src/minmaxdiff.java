import java.util.Scanner;
public class minmaxdiff {
    Scanner sc = new Scanner(System.in);
    System.out.println("Enter the size of array:");
    int size = sc.nextInt();
    int[] arr = new int[size];
    System.out.println("Enter the elements:");
    for(int p=0;p<arr.length;p++)
    {
      arr[p] = sc.nextInt();
    }
    System.out.println("Original array 'arr':");
    
    for(int i=0;i<arr.length;i++)
    {
      System.out.println(arr[i]);
    }
    int temp=0;
    for( int k=0;k<arr.length;k++)
    {
      for(int j=k+1;j<arr.length;j++)
      {
        if(arr[k]>arr[j])
        {
          temp=arr[k];
          arr[k]=arr[j];
          arr[j]=temp;
        }
      }
    }
    System.out.println("Sorted array:");
    for(int n=0;n<arr.length;n++)
    {
      System.out.println(arr[n]);
    }
    
}
