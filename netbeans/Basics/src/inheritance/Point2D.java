
package inheritance;
import java.util.*;
public class Point2D {
    private float x;
    private float y;
    
    public Point2D(){
        this(0.0f,0.0f);
    }
    
    public Point2D(float x,float y){
      this.x = x;
      this.y = y;
    }
    
    public float getX( ){
      return x;
    }
    
    public void setX(float x){
      this.x = x;
    }
    
    public float getY(){
      return y;
    }
    
    public void setY(float y){
      this.y  = y;
    }
    
    public void setXY(float x,float y){
      this.x = x;
      this.y = y;
    }
    public float[] getXY() {
      float[] a = new float[2];
      a[0]=x;
      a[1]=y;
      return a;
    }
    @Override
    public String toString(){
      return "("+this.x+","+this.y+")";
    }
    public static void main(String[] args) {
        Point2D p1 = new Point2D();
        System.out.println(p1);
        System.out.println(p1.getX());
        System.out.println(p1.getY());
        float[] p1getxy = p1.getXY();
        System.out.println(Arrays.toString(p1getxy));
        p1.setXY(3.2f, 5.9f);
        System.out.println(p1);
        System.out.println(p1.getX());
        System.out.println(p1.getY());
        float[] pgetxy = p1.getXY();
        System.out.println(Arrays.toString(pgetxy));
        
        
        Point2D p2 = new Point2D(1.2f,9.8f);
        System.out.println(p2);
        System.out.println(p2.getX());
        System.out.println(p2.getY());
        float[] p2getxy = p2.getXY();
        System.out.println(Arrays.toString(p2getxy));
        p2.setXY(4f,9.1f);
        System.out.println(p2);
        System.out.println(p2.getX());
        System.out.println(p2.getY());
        float[] p21getxy = p2.getXY();
        System.out.println(Arrays.toString(p21getxy));
    }
}
