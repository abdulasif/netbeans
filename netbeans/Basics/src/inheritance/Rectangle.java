/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package inheritance;

/**
 *
 * @author bas200190
 */
public class Rectangle {
   double length;
   double breadth;
   
   public double area()
   {
     return length*breadth;
   }
   
   public double perimeter()
   {
     return 2*(length+breadth);
   }
   
   public Rectangle(double length, double breadth){
     this.length = length;
     this.breadth = breadth;
   }
}
