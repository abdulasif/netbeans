/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhataBookAsif1611;

import java.time.LocalDate;

/**
 *
 * @author bas200190
 */
public class PaymentDetails {
    LocalDate payment_date;
    int payment_id;
    int customer_id;
    double paid_amount;
    
    
    PaymentDetails[] payment_details = new PaymentDetails[0];
    
    public PaymentDetails(){
    
    }
    
    public PaymentDetails(int payment_id,int customer_id,double paid_amount){
        this.payment_date = LocalDate.now();
        this.payment_id = payment_id;
        this.customer_id = customer_id;
        this.paid_amount = paid_amount;
    }
    
    public PaymentDetails(LocalDate payment_date,int payment_id,int customer_id,double paid_amount){
        this.payment_date = payment_date;
        this.payment_id = payment_id;
        this.customer_id = customer_id;
        this.paid_amount = paid_amount;
    }
    
    public void showPaymentDetails() {
        Design.paymentDetailsDesign();
        for (int j = 0; j < payment_details.length; j++) {

            System.out.format("\n|%-20s|%-20s|%-20s|%-20s|", payment_details[j].payment_date, payment_details[j].payment_id, payment_details[j].customer_id, payment_details[j].paid_amount);
            System.out.print("\n+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");

        }
        System.out.println("");System.out.println("");System.out.println("");
    }
    
    
}
