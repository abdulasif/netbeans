/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package inheritance;
import java.util.*;

/**
 *
 * @author bas200190
 */
public class Point3D extends Point2D
{
    private float z;
    public Point3D(){
      this(0.0f,0.0f,0.0f);
    }
    public Point3D(float x,float y,float z){
      super(x,y);
      this.z = z;
    }
    
    public float getZ(){
      return z;
    }
    
    public void setZ(float z){
      this.z = z;
    }
    
    public float[] getXYZ(){;
      float[] a = new float[3];
      a[0] = getX();
      a[1] = getY();
      a[2] = getZ();
      return a;
    }
    
    public void setXYZ(float x,float y,float z){
      setX(x);
      setY(y);
      setZ(z);
    }
    @Override
    public String toString(){
      return "("+getX()+","+getY()+","+getZ()+")";
    }
    
    public static void main(String[] args) {
        Point3D p3d1 = new Point3D();
        System.out.println(p3d1);
        Point3D p3d2 = new Point3D(2.3f,4.5f,6.7f); 
        System.out.println(p3d2);
        System.out.println(p3d2.getX());
        float[] a = p3d2.getXY();
        System.out.println(Arrays.toString(a));
        float[] b = p3d2.getXYZ();
        System.out.println(Arrays.toString(b));
        
        
    }
    
}
