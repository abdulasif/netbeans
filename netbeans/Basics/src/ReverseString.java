class ReverseString {

    String reverse(String inputString) {
        String output = "";
        for (int i = inputString.length()-1; i >=0; i--) {
            output = output + inputString.charAt(i);
        }
        return output;
    }
  
    public static void main(String[] args) {
        ReverseString ex1 = new ReverseString();
        System.out.println(ex1.reverse("abdul asif"));
    }
}
