package khatabookasif.temp_27_12;

import java.util.Arrays;

public class Demo {
    
    public static void main(String[] args) {
        int[] a = {10,20,30};
        int[] b = {20,10,30};
        boolean ans = Arrays.equals(a, b);
        System.out.println(ans);
    }
    
    public static void main28(String[] args) {   //without using sort 1st maximum 2nd max and 3rd max can be founded.
        int[] arr = {90,75,111,120,122,121};
        int max_1 = 0;
        int max_2 = 0;
        int max_3 = 0;
        
        for (int i = 0; i < arr.length; i++) {
            if (arr[i]>max_1) {
                max_3 = max_2;
                max_2 = max_1;
                max_1 = arr[i];
            }
            
            else if(arr[i]>max_2){
                max_3  = max_2;
                max_2 = arr[i];
            }
            
            else if (arr[i]>max_3) {
                max_3 = arr[i];
            }
        }
        System.out.println(max_3);
    }
    
    public static void main27(String[] args) {        //maximum element of array
        int[] arr = {89,56,90,122,43};
        int max = 0;
        for (int i = 0; i < arr.length; i++) {
            max = max > arr[i] ? max : arr[i];
        }
        
        System.out.println("maximum : "+max);
    }
    
    public static void main26(String[] args) {      //subset of given string...
        int k = 0;
        String name = "asif";
        int l = name.length();
        String[] subset_of_name = new String[(l*(l+1))/2];
        for (int i = 0; i < name.length(); i++) {
            String temp = "";
            for (int j = i; j < name.length(); j++) {
                temp = temp + name.charAt(j);
                subset_of_name[k] = temp;
                k++;
            }
        }
        System.out.println(subset_of_name.length);
        for (int i = 0; i < subset_of_name.length; i++) {
            System.out.print(subset_of_name[i]+", ");
        }
    }

    public static void main25(String[] args) {
        String str = "god is great";
//        String temp = "";
//        temp = str.replace("great", " ");
//        System.out.println(temp);
        String[] str_arr = str.split(" ");

        for (int i = 0; i < str_arr.length; i++) {
            if (str_arr[i].charAt(0) == 'g' && str_arr[i].charAt(str_arr[i].length() - 1) == 't') {
                str_arr[i] = "";
            }
        }
        String str_new = "";
        for (int i = 0; i < str_arr.length; i++) {
            str_new = str_new + str_arr[i];
        }
        System.out.println(str_new);
    }

    public static void main24(String[] args) {      //adding of each even (or) odd digits
        int[] arr = {2312, 4565, 34, 65, 90, 1111111};
        int answer = 0;
        for (int i = 0; i < arr.length; i++) {
            int temp = arr[i];
            while (temp > 0) {
                if ((temp % 10) % 2 != 0) {
                    answer = answer + temp % 10;
                }
                temp = temp / 10;
            }
        }
        System.out.println(answer);
    }

    public static void main23(String[] args) {
        String str = "JAVA CREATED BY ASIFa";
        char[] char_arr_str = str.toCharArray();
        for (int i = 0; i < char_arr_str.length; i++) {
            if (Character.isUpperCase(str.charAt(i))) {
                int digit = char_arr_str[i] + 32;
                char_arr_str[i] = (char) digit;
            }
        }
        String new_str = "";
        for (int i = 0; i < char_arr_str.length; i++) {
            new_str = new_str + char_arr_str[i];
        }
        System.out.println(new_str);
    }

    public static void main22(String[] args) {    //kannan method for convert lower to upper and upper tp lower
        String str = "java created By asif";
        char ch[] = str.toCharArray();
        for (int i = 0; i < ch.length; i++) {
            if (Character.isLowerCase(str.charAt(i))) {
                int d = ch[i] - 32;
                ch[i] = (char) d;
            }

        }
        String st = "";
        for (int i = 0; i < ch.length; i++) {
            st = st + ch[i];
        }
        System.out.println(st);
        System.out.println((char) ('j' - 32));
    }

    public static void main21(String[] args) {    //only printing starting with caps and ending with caps word .
        String str = "JavA created by JameS and Asif@";
        String s = "[A-Za-z]";
        String[] str1 = str.split(" ");
        for (int i = 0; i < str.length(); i++) {
//            if (str1[i].charAt(0)>='A' &&  str1[i].charAt(0)<='Z' && str1[i].charAt(str1[i].length()-1)>='A' &&  str1[i].charAt(str1[i].length()-1)<='Z') {
//                System.out.println(str1[i]);
//            }

//            if (str1[i].charAt(0)>='a' &&  str1[i].charAt(0)<='z' && str1[i].charAt(str1[i].length()-1)>='a' &&  str1[i].charAt(str1[i].length()-1)<='z') {
//                System.out.println(str1[i]);
//            }
//            if (str1[i].charAt(0)>='a' &&  str1[i].charAt(0)<='z' && str1[i].charAt(str1[i].length()-1)>='a' &&  str1[i].charAt(str1[i].length()-1)<='z') {
            if ((str.charAt(i) + "").matches(s)) {
                System.out.println(str.charAt(i));
            }
        }
    }

    public static void main20(String[] args) {        //removing duplicate elements
        int[] arr = {23, 786, 786, 45, 32, 23, 45, 59, 61};
        int count = 0;

        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length - count; j++) {
                if (arr[i] == arr[j]) {
                    for (int k = j; k < arr.length - 1; k++) {
                        arr[k] = arr[k + 1];
                    }
                    count++;
                }
            }
        }
        arr = Arrays.copyOf(arr, arr.length - count);
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }

    public static void main19(String[] args) {   //counting of each digit

        int[] arr = {342, 56, 9876, 311, 734};
        int[] individual_elem_arr = new int[0];
        for (int i = 0; i < arr.length; i++) {
            int temp = arr[i];
            while (temp > 0) {
                individual_elem_arr = Arrays.copyOf(individual_elem_arr, individual_elem_arr.length + 1);
                individual_elem_arr[individual_elem_arr.length - 1] = temp % 10;
                temp = temp / 10;
            }
        }
        int count = 1;
        for (int i = 0; i < individual_elem_arr.length; i++) {
            count = 1;
            for (int j = i + 1; j < individual_elem_arr.length; j++) {
                if (individual_elem_arr[i] != 0 && individual_elem_arr[i] == individual_elem_arr[j]) {
                    count++;
                    individual_elem_arr[j] = 0;
                }
            }
            if (individual_elem_arr[i] != 0) {
                System.out.println(individual_elem_arr[i] + " : " + count + "times");
            }
        }
    }

    public static void main17(String[] args) {
        int[] arr = {342, 56, 9876, 311, 734};
        int largest_big = 0;
        for (int i = 0; i < arr.length; i++) {
            int t = arr[i];
            int big = 0;
            while (t > 0) {
                big = big > t % 10 ? big : t % 10;
                t /= 10;
            }
            largest_big = largest_big > big ? largest_big : big;
        }
        System.out.println(largest_big);
    }

    public static void main18(String[] args) {     //arun bro method for bigest digit of each element
        int[] arr = {342, 56, 9876, 311, 734, 465, 999};
        for (int i = 0; i < arr.length; i++) {
            int t = arr[i];
            int big = 0;
            while (t > 0) {
                big = big > t % 10 ? big : t % 10;
                t /= 10;
            }
            System.out.println(big);
        }

    }

    public static void main16(String[] args) {     //Adding of largest digits of individul array elements
        int[] arr = {342, 56, 9876, 311, 734};
        int output = 0;
        for (int k = 0; k < arr.length; k++) {
            int temp1 = arr[k];
            int[] new_arr = new int[0];
            while (temp1 != 0) {
                new_arr = Arrays.copyOf(new_arr, new_arr.length + 1);
                new_arr[new_arr.length - 1] = temp1 % 10;
                temp1 = temp1 / 10;

            }
            for (int i = 0; i < new_arr.length; i++) {
                for (int j = 0; j < new_arr.length - 1; j++) {
                    if (new_arr[j] > new_arr[j + 1]) {
                        int temp = new_arr[j];
                        new_arr[j] = new_arr[j + 1];
                        new_arr[j + 1] = temp;
                    }
                }
            }

            output = output + new_arr[new_arr.length - 1];
        }
        System.out.println(output);
    }

    public static void main15(String[] args) {   //printing largest digits of individul array elements
        int[] arr = {342, 56, 9876, 311, 734};

        for (int k = 0; k < arr.length; k++) {
            int temp1 = arr[k];
            int[] new_arr = new int[0];
            while (temp1 != 0) {
                new_arr = Arrays.copyOf(new_arr, new_arr.length + 1);
                new_arr[new_arr.length - 1] = temp1 % 10;
                temp1 = temp1 / 10;

            }
            for (int i = 0; i < new_arr.length; i++) {
                for (int j = 0; j < new_arr.length - 1; j++) {
                    if (new_arr[j] > new_arr[j + 1]) {
                        int temp = new_arr[j];
                        new_arr[j] = new_arr[j + 1];
                        new_arr[j + 1] = temp;
                    }
                }
            }

            System.out.println(new_arr[new_arr.length - 1]);
        }
    }

    public static void main14(String[] args) {     //largest digit of arr array
        int[] arr = {342, 56, 9876};
        int[] new_arr = new int[0];
        for (int i = 0; i < arr.length; i++) {
            int temp = arr[i];
            while (temp != 0) {
                new_arr = Arrays.copyOf(new_arr, new_arr.length + 1);
                new_arr[new_arr.length - 1] = temp % 10;
                temp = temp / 10;

            }
        }

        for (int i = 0; i < new_arr.length; i++) {
            for (int j = 0; j < new_arr.length - 1; j++) {
                if (new_arr[j] > new_arr[j + 1]) {
                    int temp = new_arr[j];
                    new_arr[j] = new_arr[j + 1];
                    new_arr[j + 1] = temp;
                }
            }
        }

        for (int i = 0; i < new_arr.length; i++) {
            System.out.println(new_arr[i]);
        }
    }

    public static void main13(String[] args) {            //3 solution are there....
        String str = "abdul asif abdul lathif a1er";
//        String[] a=str.split(" ");

        for (int i = 0; i < str.length(); i++) {
//            String s=a[i];
//            for (int j = 0; j < s.length(); j++) {
//                if(j%2==1){
//                    System.out.println(s.charAt(j));
//                    break;
//                }
//                
//            }
//            System.out.println(a[i].charAt(1));

            //if (Character.isAlphabetic(str.charAt(i)) && i == 1) {
            //    System.out.println(str.charAt(i));
            //}
            str = " " + str;
            if (str.charAt(i) == ' ') {
                if (Character.isAlphabetic(str.charAt(i + 2))) {
                    System.out.println(str.charAt(i + 2));
                }
            }

        }
    }

    public static void main12(String[] args) {
        String str = "abdul asif abdul lathif";
        char[] str_arr = str.toCharArray();
        int count = 1;
        for (int i = 0; i < str_arr.length; i++) {
            count = 1;
            for (int j = i + 1; j < str_arr.length; j++) {
                if (str_arr[i] != ' ' && str_arr[i] == str_arr[j]) {
                    str_arr[j] = ' ';
                    count++;
                }
            }
            if (str_arr[i] != ' ') {
                System.out.println(str_arr[i] + " : " + count);
            }

        }
    }

    public static void main11(String[] args) {
        //Add element of  2nd digit from last of each elements
        int[] arr = {1234, 675, 23, 795, 100};
        int answer = 0;
        for (int i = 0; i < arr.length; i++) {
            int temp = arr[i];
            temp = temp / 10;
            if (temp != 0 && (temp % 10) % 2 != 0) {  //odd only
                answer = answer + temp % 10;
            }

        }
        System.out.println("answer  : " + answer);
    }

    public static void main10(String[] args) {
        String str = "james created jav";
        for (int i = 2; i < str.length(); i = i + 3) {
            if (Character.isAlphabetic(str.charAt(i))) {
                System.out.println(str.charAt(i));
            }
        }

        String str1 = "james created java";
        for (int i = str1.length() - 3; i >= 0; i = i - 3) {
            if (Character.isAlphabetic(str1.charAt(i))) {
                System.out.println(str1.charAt(i));
            }
        }
    }

    public static void main9(String[] args) {
        int[] arr = {1112, 3945, 657, 728, 5411};
        int ans = 0;
        for (int i = 0; i < arr.length; i++) {
            int temp = arr[i];
            while (temp != 0) {
                ans = ans + temp % 10;
                temp = temp / 10;
            }
        }
        System.out.println(ans);
    }

    public static void main8(String[] args) {
        int[] arr1 = {10, 20, 30, 40, 50};
        int[] arr2 = {60, 40, 70, 80, 20, 10};
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr2.length; j++) {
                if (arr1[i] == arr2[j]) {
                    System.out.println(arr2[j]);
                }
            }
        }
    }

    public static void main7(String[] args) {
        System.out.println("factorial of 6 is " + factorial(6));
    }

    public static void main6(String[] args) {
        int[] arr = {423, 756, 415, 908, 696};
        int ans = 0;
        for (int i = 0; i < arr.length; i++) {
            int temp = arr[i];
            ans = ans + temp % 10;
            temp = temp / 10;
            ans = ans + temp % 10;
            temp = temp / 10;
            ans = ans + temp;
        }
        System.out.println("addition of all digits : " + ans);
    }

    public static void main5(String[] args) {
        int[] arr = {23, 76, 45, 98, 66};
        int ans = 0;
        for (int i = 0; i < arr.length; i++) {
            ans = ans + arr[i] % 10;
            ans = ans + arr[i] / 10;
        }
        System.out.println("addition of all digits : " + ans);
    }

    public static void main4(String[] args) {
        int[] arr = {88, 54, 65, 92, 34, 29};
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
        System.out.println("After Sorting : ");
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
        System.out.println("Largest number : " + arr[arr.length - 1]);
        System.out.println("Second Largest number : " + arr[arr.length - 2]);
    }

    public static void main3(String[] args) {
        boolean temp = false;
        for (int i = 2; i < 101; i++) {
            for (int j = 2; j <= i / 2; j++) {
                if (i % j == 0) {
                    temp = true;
                    break;
                }
            }
            if (temp == false) {
                System.out.println(i + " is a prime number.");
            }
            temp = false;
        }
    }

    public static void main2(String[] args) {
        String name = "abdul asif";
        char[] name_arr = name.toCharArray();
        int count = 1;
        for (int i = 0; i < name.length(); i++) {
            count = 1;
            for (int j = i + 1; j < name.length(); j++) {
                if (name_arr[i] == name_arr[j] && name_arr[i] != ' ') {
                    name_arr[j] = ' ';
                    count++;
                }

            }
            if (name_arr[i] != ' ') {
                System.out.println(name_arr[i] + " = " + count);
            }
        }
    }

    public static void main1(String[] args) {
//        int a=819087786;
//        int b=20;

//        a= a+b;
//        b=a-b;
//        a=a-b;
//        System.out.println("a:"+a);
//        System.out.println("b:"+b);
//          if ((a&1)==0) {
//              System.out.println(a+"is even");
//           }else{
//              System.out.println(a+"is odd");
//          }
        int a = 5687;
        int rev_a = 0;
        while (a != 0) {
            int temp = a % 10;
            rev_a = (rev_a * 10) + temp;
            a = a / 10;
        }
        System.out.println(rev_a);
//        String name = "abdulasif";
//        String copy_name = "";
//        for (int i = name.length()-1; i >=0 ; i--) {
//            copy_name= copy_name +name.charAt(i)+"";
//        }
//        System.out.println(copy_name);
//          String name = "bala murugan";
//          String name1 = name.substring(0,name.indexOf(" "));
//          String name2 = name.substring(name.indexOf(" "));
//          String copy_name = name2+" "+name1;
//          System.out.println(copy_name);
//        String state = "malayalam";
//        String copy_name = "";
//        for (int i = state.length() - 1; i >= 0; i--) {
//            copy_name = copy_name + state.charAt(i) + "";
//        }
//        System.out.println(copy_name);
//        if (state.equals(copy_name)) {
//            System.out.println(state + " is palindrome");
//        } else {
//            System.out.println(state + " is not palindrome");
//        }
    }

    public static int factorial(int gn) {
        int ans = 1;
        if (gn == 1) {
            return 1;
        } else {
            ans = gn * factorial(gn - 1);
        }
        return ans;
    }
}
