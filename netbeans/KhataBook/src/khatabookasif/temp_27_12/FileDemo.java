/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package khatabookasif.temp_27_12;

import java.io.File;

/**
 *
 * @author bas200190
 */
public class FileDemo {

    
    public static void main1(String[] args) {
        File customer_file = new File("/home/bas200190/KhataBookAsif/customer.txt");
    File order_file = new File("/home/bas200190/KhataBookAsif/order.txt");
    File item_list_file = new File("/home/bas200190/KhataBookAsif/itemlist.txt");
    File product_file = new File("/home/bas200190/KhataBookAsif/product.txt");

       

        boolean customer_file_exists = customer_file.exists();
        System.out.println("customer file present: " + customer_file_exists);
        boolean order_file_exists = order_file.exists();
        System.out.println("order file present: " + order_file_exists);
        boolean item_list_file_exists = item_list_file.exists();
        System.out.println("item list file present: " + item_list_file_exists);
        boolean product_file_exists = product_file.exists();
        System.out.println("product file present: " + product_file_exists);
        try {
            customer_file.createNewFile();
            order_file.createNewFile();
            item_list_file.createNewFile();
            product_file.createNewFile();
            boolean customer_file_exists_now = customer_file.exists();
            boolean order_file_exists_now = order_file.exists();
            boolean item_list_file_exists_now = item_list_file.exists();
            boolean product_file_exists_now = product_file.exists();

            System.out.println("customer file present: " + customer_file_exists_now);
            System.out.println("order file present: " + order_file_exists_now);
            System.out.println("item list file present: " + item_list_file_exists_now);
            System.out.println("product file present: " + product_file_exists_now);
        } catch (Exception e) {
            System.err.println("FILE ALREADY CREATED");
        }
    }
}
