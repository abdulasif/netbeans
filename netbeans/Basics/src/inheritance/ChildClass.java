/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package inheritance;

/**
 *
 * @author bas200190
 */
public class ChildClass extends ParentClass{
    public void CClass()
    {
        System.out.println("This is Child class.");
    }
    
    public static void main(String[] args) {
        ChildClass b = new ChildClass();
        b.CClass();   //2
        b.PClass();   //3
    }
}
