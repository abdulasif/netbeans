/*
* Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
* Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
*/
package inheritance;

/**
*
* @author bas200190
*/
public class Square1 extends Rectangle1{
public Square1(){
      this(5);
    }
    
    public Square1(double side){
      this(side,"red",true);
    }
    
    public Square1(double side,String color,boolean filled){
        super(side,side,color,filled);
    }
    
    public double getSide(){
      return getLength();
    }
    
    public void setSide(double side){
      setLength(side);
      setWidth(side);
    }
    @Override
    public String toString(){
      return super.toString();
    }
    
    public static void main(String[] args) {
        
        Square1 sq1 = new Square1();
        System.out.println(sq1);
        System.out.println(sq1.getSide());
        System.out.println(sq1.getLength());
        System.out.println(sq1.getWidth());
        System.out.println(sq1.getArea());
        System.out.println(sq1.getPerimeter());
        System.out.println(sq1.isFilled());
        
        Square1 sq2 = new Square1(8);
        System.out.println(sq2);
        System.out.println(sq2.getSide());
        System.out.println(sq2.getLength());
        System.out.println(sq2.getWidth());
        System.out.println(sq2.getArea());
        System.out.println(sq2.getPerimeter());
        System.out.println(sq2.isFilled());
        
        Square1 sq3 = new Square1(2,"black",false);
        System.out.println(sq3);
        System.out.println(sq3.getSide());
        System.out.println(sq3.getLength());
        System.out.println(sq3.getWidth());
        System.out.println(sq3.getArea());
        System.out.println(sq3.getPerimeter());
        System.out.println(sq3.isFilled());
        
        
    }
}
