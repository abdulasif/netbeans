/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package daoMysql;

import dao.ItemListDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import model.ItemList;

/**
 *
 * @author bas200190
 */
public class ItemListDAOimpl implements ItemListDAO {

    @Override
    public int setItemListId(Connection connection) {
        int item_list = 0;
        try {
            String query = "select * from KB_item_list";
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                item_list = rs.getInt(1);

            }

            if (item_list != 0) {
                item_list = item_list + 1;
            } else {
                item_list = 201;

            }

        } catch (SQLException sqle) {
            System.out.println("sql exception is raised..");
        }
        return item_list;
    }

    @Override
    public void addItemList(List<ItemList> item_lists, Connection connection) {

        for (ItemList il : item_lists) {
            String query_6 = "insert into KB_item_list values ( ? , ? , ? , ? , ? , ?) ";

            try (PreparedStatement ps = connection.prepareStatement(query_6);) {
                int new_item_list_id = setItemListId(connection);
                ps.setInt(1, new_item_list_id);
                ps.setInt(2, il.getOrder_id());
                ps.setInt(3, il.getProduct_id());
                ps.setInt(4, il.getProduct_quantity());
                ps.setDouble(5, il.getEach_price());
                ps.setDouble(6, il.getPrice());

                ps.executeUpdate();
            } catch (Exception e) {
                System.out.println("");
            }
        }
    }
}
