
public class Laptop{
    String brand;
    String model;
    private double price;
    double size;
    String cpu;
    String storageType;
    double batteryCapacity;
    
    
    public double getPrice()
    {
      return price;
    }
    
    public Laptop()
    {
      brand = "Lenovo";
      model = "ThinkPad";
      price = 60000.0;
      size = 12.5;
      cpu = "i9";
      storageType = "ssd";
      batteryCapacity = 5200.0;
      
    }
    
    public Laptop(String brand)
    {
      this.brand = brand;
      model = "ThinkPad";
      price = 60000.0;
      size = 12.5;
      cpu = "i9";
      storageType = "ssd";
      batteryCapacity = 5200.0;
      
    }
    
     public Laptop(String brand,String model)
    {
      this.brand = brand;
      this.model = model;
      price = 70000.0;
      size = 12.5;
      cpu = "i9";
      storageType = "ssd";
      batteryCapacity = 5200.0;
      
    }
     
      public Laptop(String brand,String model,double price)
    {
      this.brand = brand;
      this.model = model;
      this.price = price;
      size = 12.5;
      cpu = "i9";
      storageType = "ssd";
      batteryCapacity = 5200.0;
      
    }
      
     public Laptop(String brand,String model,double price,double size)
    {
      this.brand = brand;
      this.model = model;
      this.price = price;
      this.size = size;
      cpu = "i9";
      storageType = "ssd";
      batteryCapacity = 5200.0;
      
    }  
     
     public Laptop(String brand,String model,double price,double size,String cpu)
    {
      this.brand = brand;
      this.model = model;
      this.price = price;
      this.size = size;
      this.cpu = cpu;
      storageType = "ssd";
      batteryCapacity = 5200.0;
      
    }
     
      public Laptop(String brand,String model,double price,double size,String cpu,String storageType)
    {
      this.brand = brand;
      this.model = model;
      this.price = price;
      this.size = size;
      this.cpu = cpu;
      this.storageType = storageType;
      batteryCapacity = 5200.0;
      
    }
      
      public Laptop(String brand,String model,double price,double size,String cpu,String storageType,double batteryCapacity)
    {
      this.brand = brand;
      this.model = model;
      this.price = price;
      this.size = size;
      this.cpu = cpu;
      this.storageType = storageType;
      this.batteryCapacity = batteryCapacity;
      
    }    
}
