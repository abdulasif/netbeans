package jdbc_topics;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JdbcPreparedStatementDemo {

    public static void main(String[] args) {

        String DB_URL = "jdbc:mysql://localhost/hari";
        String USER = "root";
        String PASS = "root";
        String QUERY = "INSERT INTO Employees VALUES (?, ?, ?, ?, ?)";

        // creating DB connection
        try (Connection connection = DriverManager.getConnection(DB_URL, USER, PASS)) {

            //creating prepared statement
            PreparedStatement preparedStatement = connection.prepareStatement(QUERY);

            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter Employee ID : ");
            int employeeId = Integer.parseInt(scanner.nextLine());
            System.out.println("Enter First name : ");
            String firstName = scanner.nextLine();
            System.out.println("Enter Last name : ");
            String lastName = scanner.nextLine();
            System.out.println("Enter Technology: ");
            String technology = scanner.nextLine();
            System.out.println("Enter Designation : ");
            String designation = scanner.nextLine();

            preparedStatement.setInt(1, employeeId);
            preparedStatement.setString(2, firstName);
            preparedStatement.setString(3, lastName);
            preparedStatement.setString(4, technology);
            preparedStatement.setString(5, designation);

            //executing the query
            int result = preparedStatement.executeUpdate();

            //printing the result
            System.out.println("Number of rows updated : " + result);

        } catch (SQLException ex) {
            Logger.getLogger(JdbcPreparedStatementDemo.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
