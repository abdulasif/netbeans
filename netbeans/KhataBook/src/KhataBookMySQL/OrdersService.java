/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhataBookMySQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;

/**
 *
 * @author bas200190
 */
public class OrdersService {
    public int setOrderId(Connection connection) throws Exception {

        String query = "select * from KB_order";
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(query);
        int order_id = 0;
        while (rs.next()) {
            order_id = rs.getInt(2);

        }

        if (order_id != 0) {
            order_id = order_id + 1;
        } else {
            order_id = 401;

        }
        System.out.println(order_id);
        return order_id;
    }

    public int setItemListId(Connection connection) throws Exception {

        String query = "select * from KB_item_list";
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(query);
        int item_list = 0;
        while (rs.next()) {
            item_list = rs.getInt(1);

        }

        if (item_list != 0) {
            item_list = item_list + 1;
        } else {
            item_list = 201;

        }
        System.out.println(item_list);
        return item_list;
    }

    public void addOrder(Connection connection) {
        try {
            System.out.println("Enter phone number to place order: ");
            String entered_customer_phoneNo = KhataBook.ob.br.readLine();

            String query_1 = "select * from KB_customer where customer_phone_no = ?";

            try (PreparedStatement ps_1 = connection.prepareStatement(query_1);) {
                ps_1.setString(1, entered_customer_phoneNo);

                try (ResultSet rs_1 = ps_1.executeQuery();) {

                    if (rs_1.next()) {
                        ItemList[] temp_item_list_arr = new ItemList[0];

                        int validated_customer_id = rs_1.getInt(2);

                        KhataBook.ob.prod.viewAllProducts(connection);

                        System.out.println("Enter the Order ID : ");
                        int enter_order_id = setOrderId(connection);

                        double temp_total_price_for_order = rs_1.getInt(6);
                        double temp_total_price_for_customer_debit = rs_1.getInt(6);

                        boolean purchase_loop = true;
                        while (purchase_loop) {
                            System.out.println("Enter the product ID:");
                            int temp_ao_product_id = Integer.parseInt(KhataBook.ob.br.readLine());
                            System.out.println("Enter How Many quantity you Need:");
                            int temp_ao_quantity_need = Integer.parseInt(KhataBook.ob.br.readLine());

                            String query_2 = "select * from KB_product where product_id = ?";

                            try (PreparedStatement ps_2 = connection.prepareStatement(query_2);) {
                                ps_2.setInt(1, temp_ao_product_id);

                                try (ResultSet rs_2 = ps_2.executeQuery();) {

                                    if (rs_2.next()) {
                                        int available_quantity = rs_2.getInt(7);
                                        if (available_quantity - temp_ao_quantity_need > 0) {

                                            String query_3 = "update KB_product set product_quantity = ? where product_id = ?";

                                            try (PreparedStatement ps_3 = connection.prepareStatement(query_3);) {
                                                ps_3.setInt(1, (available_quantity - temp_ao_quantity_need));
                                                ps_3.setInt(2, temp_ao_product_id);

                                                ps_3.executeUpdate();

                                                int item_list_id = 1;
                                                int product_id = rs_2.getInt(1);
                                                int product_quantity = temp_ao_quantity_need;
                                                double each_price = rs_2.getDouble(5);
                                                double price = ((double) product_quantity) * (each_price);

                                                temp_item_list_arr = Arrays.copyOf(temp_item_list_arr, temp_item_list_arr.length + 1);
                                                temp_item_list_arr[temp_item_list_arr.length - 1] = new ItemList(item_list_id, enter_order_id, product_id, product_quantity, each_price, price);

                                                temp_total_price_for_customer_debit = temp_total_price_for_customer_debit + price;
                                                item_list_id++;
                                                String query_4 = "update KB_customer set customer_debit = ? where customer_id = ?";

                                                try (PreparedStatement ps_4 = connection.prepareStatement(query_4);) {
                                                    ps_4.setDouble(1, temp_total_price_for_customer_debit);
                                                    ps_4.setInt(2, validated_customer_id);

                                                    ps_4.executeUpdate();

                                                    Design.repeatPurchaseDesign();
                                                    int repeat_purchase_input = Integer.parseInt(KhataBook.ob.br.readLine());
                                                    switch (repeat_purchase_input) {
                                                        case 1:
                                                            purchase_loop = false;
                                                            break;
                                                        case 2:
                                                            purchase_loop = true;
                                                            break;
                                                    }
                                                }
                                            }
                                        } else {
                                            System.out.println("DONT ENTER THE QUANTITY MORE THAN " + available_quantity + " AND LESS THAN " + 0);
                                        }
                                    } else {
                                        System.out.println("Please Enter Valid Product Id..");
                                    }
                                }
                            }

                        }

                        String query_5 = "insert into KB_order values (default , ? , ? , ?)";

                        Orders order = new Orders(enter_order_id , validated_customer_id , (temp_total_price_for_customer_debit - temp_total_price_for_order));
                        
                        try (PreparedStatement ps_5 = connection.prepareStatement(query_5);) {
                            ps_5.setInt(1, order.getOrder_id());
                            ps_5.setInt(2, order.getCustomer_id());
                            ps_5.setDouble(3, order.getTotal_price());

                            ps_5.executeUpdate();

                            for (int i = 0; i < temp_item_list_arr.length; i++) {
                                String query_6 = "insert into KB_item_list values ( ? , ? , ? , ? , ? , ?) ";

                                try (PreparedStatement ps_6 = connection.prepareStatement(query_6);) {

                                    System.out.println("Enter the item list id");
                                    int new_item_list_id = setItemListId(connection);
                                    ps_6.setInt(1, new_item_list_id);
                                    ps_6.setInt(2, temp_item_list_arr[i].getOrder_id());
                                    ps_6.setInt(3, temp_item_list_arr[i].getProduct_id());
                                    ps_6.setInt(4, temp_item_list_arr[i].getProduct_quantity());
                                    ps_6.setDouble(5, temp_item_list_arr[i].getEach_price());
                                    ps_6.setDouble(6, temp_item_list_arr[i].getPrice());

                                    ps_6.executeUpdate();
                                }
                            }
                        }

                        Design.menuAfterOrder1Design();
                        int payment_input = Integer.parseInt(KhataBook.ob.br.readLine());
                        switch (payment_input) {
                            case 1:
                                Design.menuAfterOrder2Design();
                                int pay_now_input = Integer.parseInt(KhataBook.ob.br.readLine());
                                switch (pay_now_input) {
                                    case 1:
                                        KhataBook.ob.pay.payThisOrder(validated_customer_id, temp_total_price_for_customer_debit, connection);//may be wrong bec of index passed
                                        break;

                                    case 2:
                                        KhataBook.ob.pay.payPartially(validated_customer_id, connection);
                                        break;
                                }
                                break;
                            case 2:
                                break;
                        }
                        KhataBook.ob.ord_det.viewParticularOrder(enter_order_id, connection);

                    } else {
                        System.out.println("Enter valid customer phone number...");
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Add order is not Done..");
        }
    }
}
