/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package inheritance;

/**
 *
 * @author bas200190
 */
public class Rectangle1 extends Shape1{
    private double width;
    private double length;
    
    public Rectangle1(){
      this(1.0,1.0);
    }
    
    public Rectangle1(double width,double length){
     this(width,length,"red",true);
    }
    
    public Rectangle1(double width,double length,String color,boolean filled){
      super(color,filled);
      this.width = width;
      this.length = length;
    }
    
    public double getWidth(){
      return this.width;
    }
    
    public void setWidth(double width){
      this.width = width;
    }
    
    public double getLength(){
      return this.length;
    }
    
    public void setLength(double length){
      this.length = length;
    }
    
    public double getArea(){
      return length*width;
    }
    
    public double getPerimeter(){
      return 2*(length+width);
    }
    
    @Override
    public String toString(){
      return super.toString()+" "+getWidth()+" "+getLength();
      
    }
    
    public static void main(String[] args) {
        Rectangle1 r1 = new Rectangle1();
        System.out.println(r1);
        System.out.println(r1.getLength());
        System.out.println(r1.getWidth());
        System.out.println(r1.getArea());
        System.out.println(r1.getPerimeter());
        System.out.println(r1.getColor());
        System.out.println(r1.isFilled());
        
        Rectangle1 r2 = new Rectangle1(2,4);
        System.out.println(r2);
        System.out.println(r2.getLength());
        System.out.println(r2.getWidth());
        System.out.println(r2.getArea());
        System.out.println(r2.getPerimeter());
        System.out.println(r2.getColor());
        System.out.println(r2.isFilled());
        
        Rectangle1 r3 = new Rectangle1(6,8,"blue",true);
        System.out.println(r3);
        System.out.println(r3.getLength());
        System.out.println(r3.getWidth());
        System.out.println(r3.getArea());
        System.out.println(r3.getPerimeter());
        System.out.println(r3.getColor());
        System.out.println(r3.isFilled());
        
    }
}
