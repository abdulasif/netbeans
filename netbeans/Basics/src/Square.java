class Square {

    int computeSquareOfSumTo(int input) {
        int sumOfInput = 0;
        for(int i=input;i>0;i--){
            sumOfInput = sumOfInput + i;
        }
        int squareOfSum = sumOfInput*sumOfInput;
        return squareOfSum;
    }

    int computeSumOfSquaresTo(int input) {
        int sumOfSquare = 0;
        for(int i=input;i>0;i--){
            int squareOfNumber = i*i;
            sumOfSquare = sumOfSquare + squareOfNumber;
        }
        return sumOfSquare;
    }

    int computeDifferenceOfSquares(int input) {
        return computeSquareOfSumTo(input)-computeSumOfSquaresTo(input);
        
    }
    
    public static void main(String[] args) {
        Square s1 = new Square();
        System.out.println(s1.computeDifferenceOfSquares(10));;
    }

}

