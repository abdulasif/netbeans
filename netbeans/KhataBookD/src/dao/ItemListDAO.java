/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package dao;

import java.sql.Connection;
import java.util.List;
import model.ItemList;

/**
 *
 * @author bas200190
 */
public interface ItemListDAO {
    public int setItemListId(Connection connection);
    public void addItemList( List<ItemList> item_lists, Connection connection);
}
