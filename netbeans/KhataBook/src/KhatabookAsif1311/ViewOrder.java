/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhatabookAsif1311;

import java.io.IOException;

/**
 *
 * @author bas200190
 */
public class ViewOrder {

    Orders[] ors = new Orders[0];

    public void viewAllOrder() {
        for (int i = 0; i < ors.length; i++) {
            KhataBook.ob.de.viewAllOrderDesign();
            //System.out.println(ors.length);
            //System.out.println(ors[0].itemList.length);
            for (int j = 0; j < ors[i].itemList.length; j++) {

                System.out.format("\n|%-15s|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|", ors[i].ordered_date,ors[i].customer_id, ors[i].order_id, ors[i].itemList[j].product_id, ors[i].itemList[j].product_quantity, ors[i].itemList[j].each_price, ors[i].itemList[j].price);
                System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");
            }
            System.out.println("");
        }
    }

    public void viewParticularOrder() {
        //Scanner sc = new Scanner(System.in);
        try{
        System.out.println("Enter the Order ID:");
        int vpo_order_id = Integer.parseInt(KhataBook.ob.br.readLine());
        for (int i = 0; i < ors.length; i++) {
            KhataBook.ob.de.viewAllOrderDesign();
            if ((ors[i].order_id == vpo_order_id)) {
                for (int j = 0; j < ors[i].itemList.length; j++) {
                
                    System.out.format("\n|%-15s|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|", ors[i].ordered_date,ors[i].customer_id, ors[i].order_id, ors[i].itemList[j].product_id, ors[i].itemList[j].product_quantity, ors[i].itemList[j].each_price, ors[i].itemList[j].price);
                    System.out.print("\n+"+ "-".repeat(15) + "+"  + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");

                }
                System.out.format("\n%120s|%-20s|", "TOTAL PRICE",ors[i].total_price);
                System.out.print("\n" + " ".repeat(120) + "+" + "-".repeat(20) + "+");
            }

        }
        }
        catch(IOException ioe){
           
        }

    }

}
