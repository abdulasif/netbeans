/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package inheritance;

/**
 *
 * @author bas200190
 */
public class Manager extends Member {

    String specialization;
    String department;
    
    public static void main(String[] args) {
        Member a = new Member();
        a.name = "arun";
        a.age = 25;
        a.phoneNumber = 8428168785l;
        a.address = "10 ponneri";
        a.salary = 55000;
        
        System.out.println(a.name);
        System.out.println(a.age);
        System.out.println(a.phoneNumber);
        System.out.println(a.address);
        System.out.println(a.printSalary());
    }
}
