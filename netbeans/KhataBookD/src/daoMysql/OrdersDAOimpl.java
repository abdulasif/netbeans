/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package daoMysql;

import bl.Design;
import dao.KhataBookDAO;
import dao.OrdersDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import model.Customer;
import model.Orders;
import model.Product;

/**
 *
 * @author bas200190
 */
public class OrdersDAOimpl implements OrdersDAO {

    @Override
    public int setOrderId(Connection connection) {
        int order_id = 0;
        try {
            String query = "select * from KB_order";
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                order_id = rs.getInt(2);

            }

            if (order_id != 0) {
                order_id = order_id + 1;
            } else {
                order_id = 401;

            }
            System.out.println(order_id);

        } catch (SQLException sqle) {
            System.out.println("sql exception is raised..");
        }
        return order_id;
    }

    

    @Override
    public void viewAllOrder(Connection connection) {
        try {
            String query_1 = "select * from KB_order";

            try (PreparedStatement ps_1 = connection.prepareStatement(query_1); ResultSet rs_1 = ps_1.executeQuery()) {

                while (rs_1.next()) {
                    Design.viewOrderDesign();
                    int orders_order_id = rs_1.getInt(2);

                    String query_2 = "select * from KB_item_list where order_id = ?";

                    try (PreparedStatement ps_2 = connection.prepareStatement(query_2)) {
                        ps_2.setInt(1, orders_order_id);
                        try (ResultSet rs_2 = ps_2.executeQuery()) {
                            while (rs_2.next()) {
                                System.out.format("\n|%-15s|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|", rs_1.getDate(1), rs_1.getInt(3), rs_1.getInt(2), rs_2.getInt(3), rs_2.getInt(4), rs_2.getDouble(5), rs_2.getDouble(6));
                                System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");
                            }
                            System.out.format("\n|%120s|%-20s|", "TOTAL PRICE", rs_1.getDouble(4));
                            System.out.print("\n+" + "-".repeat(120) + "+" + "-".repeat(20) + "+");
                            System.out.println("");
                            System.out.println("");
                            System.out.println("");
                        }
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println("View all Order is not Done..");
        }

    }

    @Override
    public void viewParticularOrder(int vpo_order_id, Connection connection) {
        try {
            String query_1 = "select * from KB_order where order_id = ?";

            try (PreparedStatement ps_1 = connection.prepareStatement(query_1)) {
                ps_1.setInt(1, vpo_order_id);
                try (ResultSet rs_1 = ps_1.executeQuery()) {
                    if (rs_1.next()) {
                        Design.viewOrderDesign();
                        String query_2 = "select * from KB_item_list where order_id = ?";

                        try (PreparedStatement ps_2 = connection.prepareStatement(query_2)) {
                            ps_2.setInt(1, vpo_order_id);
                            try (ResultSet rs_2 = ps_2.executeQuery()) {
                                while (rs_2.next()) {
                                    System.out.format("\n|%-15s|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|", rs_1.getDate(1), rs_1.getInt(3), rs_1.getInt(2), rs_2.getInt(3), rs_2.getInt(4), rs_2.getDouble(5), rs_2.getDouble(6));
                                    System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");
                                }
                                System.out.format("\n|%120s|%-20s|", "TOTAL PRICE", rs_1.getDouble(4));
                                System.out.print("\n+" + "-".repeat(120) + "+" + "-".repeat(20) + "+");
                                System.out.println("");
                                System.out.println("");
                                System.out.println("");
                            }
                        }

                    }
                }
            }
        } catch (SQLException e) {
            System.out.println("View particular Order is not Done..");
        }
    }

    @Override
    public Customer customerDetailsWRTPhoneNo(String entered_customer_phoneNo, Connection connection) {
        String query_1 = "select * from KB_customer where customer_phone_no = ?";
        Customer customer = null;
        try (PreparedStatement ps_1 = connection.prepareStatement(query_1)) {
            ps_1.setString(1, entered_customer_phoneNo);

            try (ResultSet rs_1 = ps_1.executeQuery();) {

                if (rs_1.next()) {
                    customer = new Customer(rs_1.getInt(2), rs_1.getString(3), rs_1.getString(4), rs_1.getString(5), rs_1.getInt(6));
                }
            } catch (Exception e) {

            }
        } catch (Exception e) {

        }
        return customer;
    }
    
    @Override
    public Customer customerDetailsWRTCustomerId(int customer_id, Connection connection){
        String query_1 = "select * from KB_customer where customer_id = ?";
        Customer customer = null;
        try (PreparedStatement ps_1 = connection.prepareStatement(query_1)) {
            ps_1.setInt(1, customer_id);

            try (ResultSet rs_1 = ps_1.executeQuery();) {

                if (rs_1.next()) {
                    customer = new Customer(rs_1.getInt(2), rs_1.getString(3), rs_1.getString(4), rs_1.getString(5), rs_1.getInt(6));
                }
            } catch (Exception e) {

            }
        } catch (Exception e) {

        }
        return customer;
    }

    @Override
    public Product productDetailsWRTProductId(int product_id, Connection connection) {

        Product product = null;
        String query_2 = "select * from KB_product where product_id = ?";

        try (PreparedStatement ps_2 = connection.prepareStatement(query_2);) {
            ps_2.setInt(1, product_id);

            try (ResultSet rs_2 = ps_2.executeQuery();) {

                if (rs_2.next()) {
                    product = new Product(rs_2.getInt(1), rs_2.getString(2), rs_2.getString(3), rs_2.getDouble(4), rs_2.getDouble(5), rs_2.getString(6), rs_2.getInt(7));
                }
            } catch (Exception e) {

            }
        } catch (Exception e) {

        }
        return product;
    }

    @Override
    public void updateProductAfterPurchase(int product_id, int product_quantity, Connection connection) {
        String query = "update KB_product set product_quantity = ? where product_id = ?";

        try (PreparedStatement ps = connection.prepareStatement(query);) {
            ps.setInt(1, product_quantity);
            ps.setInt(2, product_id);

            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("");
        }
    }

    @Override
    public void updateCustomerAfterPurchase(int customer_id, double latest_customer_debit, Connection connection) {
        String query_4 = "update KB_customer set customer_debit = ? where customer_id = ?";

        try (PreparedStatement ps_4 = connection.prepareStatement(query_4);) {
            ps_4.setDouble(1, latest_customer_debit);
            ps_4.setInt(2, customer_id);

            ps_4.executeUpdate();
        } catch (Exception e) {

        }
    }
    
    

    @Override
    public void addOrder(KhataBookDAO kbd , Orders order, Connection connection) {
        String query = "insert into KB_order values (default , ? , ? , ?)";

        try (PreparedStatement ps = connection.prepareStatement(query);) {
            ps.setInt(1, order.getOrder_id());
            ps.setInt(2, order.getCustomer_id());
            ps.setDouble(3, order.getTotal_price());

            ps.executeUpdate();
            
            kbd.getItemListDAOimpl().addItemList( order.getItem_lists(), connection);
            kbd.getOrdersDAOimpl().viewParticularOrder(order.getOrder_id(), connection);
            
        } catch (Exception e) {
//            System.out.println("");
        }
    }

}
