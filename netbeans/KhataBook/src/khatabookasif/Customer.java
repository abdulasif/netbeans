package khatabookasif;

import java.util.Scanner;
import java.util.Arrays;

public class Customer {

    static int set_customer_id = 504;
    int customer_id;
    String customer_name;
    String customer_phoneNo;
    String customer_aadharNo;
    String customer_address;
    Customer[] customer_details = new Customer[3];
    static int cust_details_index = 3;

    public Customer() {

    }

    public Customer(int customer_id, String customer_name, String customer_phoneNo, String customer_aadharNo, String customer_address) {

        this.customer_id = customer_id;
        this.customer_name = customer_name;
        this.customer_phoneNo = customer_phoneNo;
        this.customer_aadharNo = customer_aadharNo;
        this.customer_address = customer_address;
    }

    public void addCustomer() {

        customer_details = Arrays.copyOf(customer_details, customer_details.length + 1);
        Scanner sc = new Scanner(System.in);
        System.out.println("*****Enter Customer Details*****");
        System.out.println("Enter the Customer Name:");
        String set_customer_name = sc.next();
        System.out.println("Enter the Phone Number:");
        String set_customer_phoneNo = sc.next();
        System.out.println("Enter the Aadhar Number:");
        String set_customer_aadharNo = sc.next();
        System.out.println("Enter the Customer Address:");
        String set_customer_address = sc.next();
        customer_details[cust_details_index] = new Customer(set_customer_id, set_customer_name, set_customer_phoneNo, set_customer_aadharNo, set_customer_address);
        set_customer_id++;
        cust_details_index++;

    }

    public void viewCustomer() {
        System.out.print("\n+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(25) + "+");
        System.out.format("\n|%-20s|%-20s|%-20s|%-20s|%-25s|", "CUSTOMER ID", "CUSTOMER NAME", "CUSTOMER PHONE NO", "CUSTOMER AADHAR NO", "CUSTOMER ADDRESS");
        System.out.print("\n+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(25) + "+");

        for (int j = 0; j < customer_details.length; j++) {
            System.out.print(customer_details[j]);
            System.out.print("\n+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(25) + "+");
        }

    }

    @Override
    public String toString() {
        return String.format("\n|%-20d|%-20s|%-20s|%-20s|%-25s|", this.customer_id, this.customer_name, this.customer_phoneNo, this.customer_aadharNo, this.customer_address);
    }

}
