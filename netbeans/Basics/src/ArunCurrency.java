
import java.util.Scanner;

public class ArunCurrency {

    static String[] ones = {"Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen",
        "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"};
    static String[] tens = {"", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};
    static String[] unit = {"", "hundred", "thousand", "lakh", "crore"};
    static long money;
    static String money_value = "";

    //return ones value
    static String getones(int money) {
        return ones[money];
    }

    //return ten value
    static String gettens(int money) {
        return tens[money];
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the money value in number :");
        money = scan.nextInt();
        int count = 0;
        int digit = 0;
        if (money == 0) {
            money_value = ones[(int) money];
        }
        while (money > 0) {
            if (count == 1) {
                digit = (int) money % 10;
                money = money / 10;
            } else {
                digit = (int) money % 100;
                money = money / 100;
            }
            if (digit > 0 && digit < 20 && digit != 0) {
                if (count == 1) {
                    money_value = getones(digit) + " " + unit[count] + " and " + money_value;
                } else {
                    money_value = getones(digit) + unit[count] + " " + money_value;
                }
            } else if (digit % 10 == 0 && digit < 91 && digit != 0) {
                if (count == 1) {
                    money_value = (gettens(digit / 10) + " " + unit[count] + " and " + money_value);
                } else {
                    money_value = (gettens(digit / 10) + " " + unit[count] + " " + money_value);
                }
            } else if (digit > 20 && 100 > digit && digit != 0) {
                int digit_ten = digit / 10;
                int digit_one = digit % 10;
                if (count == 1) {
                    money_value = (gettens(digit_ten) + getones(digit_one) + " " + unit[count] + " and " + money_value);
                } else {
                    money_value = (gettens(digit_ten) + getones(digit_one) + " " + unit[count] + " " + money_value);
                }
            }
            count++;
        }
        System.out.print("your money in text:" + money_value + "\n");
    }
}
