/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhatabookAsif1311;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;

/**
 *
 * @author bas200190
 */
public class Orders {
    LocalDate ordered_date;
    int order_id;
    int customer_id;
    double total_price;
    OrderList[] itemList;

    static public int order_id_inc = 900;
//    Scanner sc = new Scanner(System.in);

    public Orders() {

    }

    public Orders(int order_id, int customer_id, double total_price, OrderList[] itemList) {
        this.ordered_date = LocalDate.now();
        this.order_id = order_id;
        this.customer_id = customer_id;
        this.total_price = total_price;
        this.itemList = itemList;
    }

    public Orders addOrder() {
        OrderList[] temp_itemList = new OrderList[0];
        int ao_order_id = order_id_inc++;
        int ao_customer_id = 0;
        int ao_total_price = 0;
        try{
        System.out.println("Enter the Customers Old Phone Number to Place Order:");
        String ao_customer_phoneNo = KhataBook.ob.br.readLine();
        for (int k = 0; k < KhataBook.ob.ac.customer_details.length; k++) {
            if (ao_customer_phoneNo.equals(KhataBook.ob.ac.customer_details[k].customer_phoneNo)) {
                System.out.println("ALL PRODUCTS ARE....");
                KhataBook.ob.p.viewAllProducts();
                System.out.println("");
                boolean purchase_loop = true;
                while (purchase_loop) {
                    System.out.println("Enter the product ID:");
                    int temp_ao_product_id = Integer.parseInt(KhataBook.ob.br.readLine());
                    System.out.println("Enter How Many quantity you Need:");
                    int temp_ao_quantity_need = Integer.parseInt(KhataBook.ob.br.readLine());
                    for (int j = 0; j < KhataBook.ob.p.product_details.length; j++) {
                        if (temp_ao_product_id == KhataBook.ob.p.product_details[j].product_id) {
                            if (KhataBook.ob.p.product_details[j].product_quantity - temp_ao_quantity_need > 0) {
                                KhataBook.ob.p.product_details[j].product_quantity = KhataBook.ob.p.product_details[j].product_quantity - temp_ao_quantity_need;
                                ao_customer_id = KhataBook.ob.ac.customer_details[k].customer_id;
                                ao_total_price = ao_total_price + (temp_ao_quantity_need * KhataBook.ob.p.product_details[j].product_selling_price);
                                temp_itemList = Arrays.copyOf(temp_itemList, temp_itemList.length + 1);
                                temp_itemList[temp_itemList.length - 1] = new OrderList(KhataBook.ob.p.product_details[j].product_id, temp_ao_quantity_need, KhataBook.ob.p.product_details[j].product_selling_price, KhataBook.ob.p.product_details[j].product_selling_price * temp_ao_quantity_need);

                                KhataBook.ob.ac.customer_details[k].customer_debit = KhataBook.ob.ac.customer_details[k].customer_debit + (KhataBook.ob.p.product_details[j].product_selling_price * temp_ao_quantity_need);
                                // why wrong //System.out.println(AddCustomer.customer_details[k].customer_debit);
                                KhataBook.ob.de.repeatPurchaseDesign();
                                int repeat_purchase_input = Integer.parseInt(KhataBook.ob.br.readLine());
                                switch (repeat_purchase_input) {
                                    case 1:
                                        purchase_loop = false;
                                        break;
                                    case 2:
                                        purchase_loop = true;
                                        break;
                                }

                            } else {
                                System.out.println("DONT ENTER THE QUANTITY MORE THAN " + KhataBook.ob.p.product_details[j].product_quantity + " AND LESS THAN " + 0);
                            }
                        }

                    }
                }

            }
        }
        }
        catch(IOException ioe){
                
        }
        Orders x = new Orders(ao_order_id, ao_customer_id, ao_total_price, temp_itemList);
        ao_customer_id = 0;
        ao_total_price = 0;
        System.out.println("ORDER PLACED ");
        System.out.println("WAITING FOR PAYMENT...");
        // System.out.println("Item list Length:"+this.itemList.length);
        return x;
    }

}
