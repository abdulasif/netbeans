/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package inheritance;

public class Student extends Person{
    private String program;
    private int year;
    private double fee;
    
    public Student(String name,String address,String program,int year,double fee){
      super(name,address);
      this.program = program;
      this.year = year;
      this.fee = fee;
    }
    
    public String getProgram(){
      return this.program;
    }
    
    public void setProgram(String program){
      this.program = program;
    }
    
    public int getYear(){
      return this.year;
    }
    
    public void setyear(int year){
      this.year = year;
    }
    
    public double getFee(){
      return this.fee;
    }
    
    public void setFee(double fee){
      this.fee = fee;
    }
    @Override
    public String toString(){
      return getName()+""+getAddress()+" "+this.program+" "+this.year+" "+this.fee;
    }
    
    
    public static void main(String[] args) {
        Student s1 = new Student("Bala","kodambkkam","mechanical",2017,120000);
        System.out.println(s1.getName()+" "+s1.getAddress()+" "+s1.getProgram()+" "+s1.getYear()+" "+s1.getFee());
        System.out.println(s1);
    }
}
