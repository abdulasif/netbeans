/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhataBookAsif1611;



import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;

/**
 *
 * @author bas200190
 */
public class Orders {
    LocalDate ordered_date;
    int order_id;
    int customer_id;
    double total_price;
    ItemList[] item_list;

    //static public int order_id_inc = 900;
//    Scanner sc = new Scanner(System.in);

    public Orders() {

    }

    public Orders(int order_id, int customer_id, double total_price, ItemList[] item_list) {
        this.ordered_date = LocalDate.now();
        this.order_id = order_id;
        this.customer_id = customer_id;
        this.total_price = total_price;
        this.item_list = item_list;
    }
    
    public Orders(LocalDate ordered_date,int order_id, int customer_id, double total_price, ItemList[] item_list) {
        this.ordered_date = ordered_date;
        this.order_id = order_id;
        this.customer_id = customer_id;
        this.total_price = total_price;
        this.item_list = item_list;
    }

    public Orders addOrder(String matched_phoneNo) {
        ItemList[] temp_itemList = new ItemList[0];
        int ao_order_id = KhataBook.ob.ord_det.order_details.length+900;
        int ao_customer_id = 0;
        double ao_total_price = 0;
        try{
        
        for (int k = 0; k < KhataBook.ob.kd.customer_details.length; k++) {
            if (matched_phoneNo.equals(KhataBook.ob.kd.customer_details[k].customer_phoneNo)) {
                System.out.println("ALL PRODUCTS ARE....");
                KhataBook.ob.prod.viewAllProducts();
                System.out.println("");
                boolean purchase_loop = true;
                while (purchase_loop) {
                    System.out.println("Enter the product ID:");
                    int temp_ao_product_id = Integer.parseInt(KhataBook.ob.br.readLine());
                    System.out.println("Enter How Many quantity you Need:");
                    int temp_ao_quantity_need = Integer.parseInt(KhataBook.ob.br.readLine());
                    for (int j = 0; j < KhataBook.ob.kd.product_details.length; j++) {
                        if (temp_ao_product_id == KhataBook.ob.kd.product_details[j].product_id) {
                            if (KhataBook.ob.kd.product_details[j].product_quantity - temp_ao_quantity_need > 0) {
                                KhataBook.ob.kd.product_details[j].product_quantity = KhataBook.ob.kd.product_details[j].product_quantity - temp_ao_quantity_need;
                                ao_customer_id = KhataBook.ob.kd.customer_details[k].customer_id;
                                ao_total_price = ao_total_price + (temp_ao_quantity_need * KhataBook.ob.kd.product_details[j].product_selling_price);
                                temp_itemList = Arrays.copyOf(temp_itemList, temp_itemList.length + 1);
                                
                                double each_item_list_tot_price = KhataBook.ob.kd.product_details[j].product_selling_price * temp_ao_quantity_need;
                                temp_itemList[temp_itemList.length - 1] = new ItemList(ao_order_id,KhataBook.ob.kd.product_details[j].product_id, temp_ao_quantity_need, KhataBook.ob.kd.product_details[j].product_selling_price,each_item_list_tot_price);
                                
                                KhataBook.ob.kd.customer_details[k].customer_debit = KhataBook.ob.kd.customer_details[k].customer_debit + each_item_list_tot_price;   //future update not good to every time changes made in cus debit , this statement placed in before order object created
                                
                                Design.repeatPurchaseDesign();
                                int repeat_purchase_input = Integer.parseInt(KhataBook.ob.br.readLine());
                                switch (repeat_purchase_input) {
                                    case 1:
                                        purchase_loop = false;
                                        break;
                                    case 2:
                                        purchase_loop = true;
                                        break;
                                }

                            } else {
                                System.out.println("DONT ENTER THE QUANTITY MORE THAN " + KhataBook.ob.kd.product_details[j].product_quantity + " AND LESS THAN " + 0);
                            }
                        }else{
                            if (j==KhataBook.ob.kd.product_details.length-1 && KhataBook.ob.kd.product_details[j].product_quantity - temp_ao_quantity_need > 0) {
                                System.out.println("PLEASE ENTER VALID PRODUCT ID");
                            }
                        }

                    }
                }

            }
        }
        }
        catch(IOException ioe){
            System.out.println("PLEASE ENTER CORRECT INPUT");
        }
        Orders x = new Orders(ao_order_id, ao_customer_id, ao_total_price, temp_itemList);
//        temp_itemList = null;
//        ao_order_id = 0;
//        ao_customer_id = 0;
//        ao_total_price = 0;
        System.out.println("ORDER PLACED ");
        System.out.println("WAITING FOR PAYMENT...");
        
        return x;
    }

}
