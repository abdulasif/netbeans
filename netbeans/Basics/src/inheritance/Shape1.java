/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package inheritance;

/**
 *
 * @author bas200190
 */
public class Shape1 {
    private String color;
    private boolean filled;
    
    public Shape1(){
        this("red",true);
    }
    
    public Shape1(String color,boolean filled){
      this.color = color;
      this.filled = filled;
    }
    
    public String getColor(){
      return this.color;
    }
    
    public void setColor(String color){
      this.color = color;
    }
    
    public boolean isFilled(){
      return this.filled;
    }
    
    public void setFilled(boolean filled){
      this.filled = filled;
    }
    
    @Override
    public String toString(){
      return this.color+" "+this.filled;
    }
    
    public static void main(String[] args) {
        Shape1 s1 = new Shape1();
        System.out.println(s1);
        System.out.println(s1.getColor());
        System.out.println(s1.isFilled());
        s1.setColor("meroon");
        System.out.println(s1.getColor());
        
        Shape1 s2 = new Shape1("rose",false);
        System.out.println(s2);
        System.out.println(s2.getColor());
        System.out.println(s2.isFilled());
        s2.setColor("pink");
        s2.setFilled(true);
        System.out.println(s2.getColor());
        System.out.println(s2.isFilled());
    }
}
