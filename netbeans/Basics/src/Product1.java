
public class Product1 {
    public static void main(String[] args) throws CloneNotSupportedException {
        Marker1 m1 = new Marker1(50);
        Pen1 p1 = new Pen1("cello","blue",m1);
        System.out.println(p1.brand+"..."+p1.color+"..."+p1.p.price);
        
        Pen1 p2 = (Pen1)p1.clone();
       
        p2.p.price = 45;
        System.out.println(p2.brand+"..."+p2.color+"..."+p2.p.price);
        p2.brand = "reynolds";
        p2.color = "black";
        System.out.println((p1.brand).equals((p2.brand)));
        
        //System.out.println(p1.brand+"..."+p1.color+"..."+p1.p.price);    //before cloneing p1 is overrided because of p2
        
        System.out.println(p1.brand+"..."+p1.color+"..."+p1.p.price);      //after overriding
        
    }
 
}
