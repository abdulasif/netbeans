/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhataBookAsif1611;



import java.io.IOException;

/**
 *
 * @author bas200190
 */
public class OrderDetails {

    Orders[] order_details = new Orders[0];

    public void viewAllOrder() {
        for (int i = 0; i < order_details.length; i++) {
            Design.viewOrderDesign();
            for (int j = 0; j < order_details[i].item_list.length; j++) {

                System.out.format("\n|%-15s|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|", order_details[i].ordered_date,order_details[i].customer_id, order_details[i].order_id, order_details[i].item_list[j].product_id, order_details[i].item_list[j].product_quantity, order_details[i].item_list[j].each_price, order_details[i].item_list[j].price);
                System.out.print("\n+" + "-".repeat(15) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");
            }
            System.out.format("\n|%120s|%-20s|", "TOTAL PRICE",order_details[i].total_price);
            System.out.print("\n+" + "-".repeat(120) + "+" + "-".repeat(20) + "+");System.out.println("");System.out.println("");System.out.println("");
            
            
        }
    }

    public void viewParticularOrder() {
        //Scanner sc = new Scanner(System.in);
        try{
        System.out.println("Enter the Order ID:");
        int vpo_order_id = Integer.parseInt(KhataBook.ob.br.readLine());
        for (int i = 0; i < order_details.length; i++) {
            
            if ((order_details[i].order_id == vpo_order_id)) {
                Design.viewOrderDesign();
                for (int j = 0; j < order_details[i].item_list.length; j++) {
                
                    System.out.format("\n|%-15s|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|", order_details[i].ordered_date,order_details[i].customer_id, order_details[i].order_id, order_details[i].item_list[j].product_id, order_details[i].item_list[j].product_quantity, order_details[i].item_list[j].each_price, order_details[i].item_list[j].price);
                    System.out.print("\n+"+ "-".repeat(15) + "+"  + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");

                }
                System.out.format("\n|%120s|%-20s|", "TOTAL PRICE",order_details[i].total_price);
                System.out.print("\n+" + "-".repeat(120) + "+" + "-".repeat(20) + "+");System.out.println("");System.out.println("");System.out.println("");
                
            }
        }
        }
        catch(IOException ioe){
            System.out.println("Please Enter correct input...");
        }
    }
}
