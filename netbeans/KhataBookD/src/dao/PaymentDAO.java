/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package dao;

import java.sql.Connection;

/**
 *
 * @author bas200190
 */
public interface PaymentDAO {

    public int setPaymentId(Connection connection);

    public void payThisOrder(int customer_id, double amount, Connection connection);

    public void payPartially(int customer_id, Connection connection);

    public double currentDebit(int cd_customer_id, Connection connection);

    public void showPaymentDetails(Connection connection);
}
