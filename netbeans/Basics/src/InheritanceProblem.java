//Create a class named 'Member' having the following members: 
//Data members 
//1 - Name 
//2 - Age 
//3 - Phone number 
//4 - Address 
//5 - Salary 
//It also has a method named 'printSalary' which prints the salary of the members. 
//Two classes 'Employee' and 'Manager' inherits the 'Member' class. The 'Employee' and 'Manager' classes have data members 'specialization' and 'department' respectively. Now, assign name, age, phone number, address and salary to an employee and a manager by making an object of both of these classes and print the same. 
//
// 
//
//Create a class with a method that prints "This is parent class" and its subclass with another method that prints "This is child class". Now, create an object for each of the class and call 
//1 - method of parent class by object of parent class 
//2 - method of child class by object of child class 
//3 - method of parent class by object of child class 
//
//Note: In the above example, declare the method of the parent class as private and then repeat the first two operations (You will get error in the third). 
//
//Create a class named 'Rectangle' with two data members 'length' and 'breadth' and two methods to print the area and perimeter of the rectangle respectively. Its constructor having parameters for length and breadth is used to initialize length and breadth of the rectangle. Let class 'Square' inherit the 'Rectangle' class with its constructor having a parameter for its side (suppose s) calling the constructor of its parent class as 'super(s,s)'. Print the area and perimeter of a rectangle and a square. 
//
//Note: 
//
//Now repeat the above example to print the area of 10 squares. 
//
//Hint-Use array of objects 
//
// 
//
//Create a class named 'Shape' with a method to print "This is This is shape". Then create two other classes named 'Rectangle', 'Circle' inheriting the Shape class, both having a method to print "This is rectangular shape" and "This is circular shape" respectively. Create a subclass 'Square' of 'Rectangle' having a method to print "Square is a rectangle". Now call the method of 'Shape' and 'Rectangle' class by the object of 'Square' class. 
//
// 
//
//Here's a problem from a previous Final Exam. 
//
//Consider the following skeleton for a Robot class, which has private fields for storing the 
//
//location of aRobot object, its name, and the direction it’s facing (North for a direction parallel to 
//
//the positive y axis,South for the negative y axis, East for the positive x axis, or West for the 
//
//negative x axis). It also hasstub methods for constructing a Robot object, changing the direction, 
//
//and moving the location of therobot in the direction it’s facing. 

//public class Robot 
//
//{ 
//
//private String name; 
//
//private char direction; //’N’,’S’,’E’, or ’W’ 
//
//privateintxLoc, yLoc; // the (x, y) location of the robot 
//
//// Initialize name, direction, and (x, y) location 
//
//public Robot(String name, char dir, int x, int y) { ... } 
//
//public String toString() 
//
//{ 
//
//return name + " is standing at (" + x + "," + y + ") and facing" 
//
//+ direction); 
//
//} 
//
//// turn 90 degrees clockwise, e.g. ’N’ changes to ’E’, ’E’ to ’S’, ... 
//
//public void turnClockwise() { ... } 
//
//// turn 90 degrees counterclockwise, e.g. ’N’ to ’W’, ’W’ to ’S’, ... 
//
//public void turnCounterClockwise() { ... } 
//
//// move numSteps in direction you are facing, 
//
//// e.g. if ’N’ 3 steps, then y increases 3 
//
//public void takeSteps(intnumSteps) { ... } 
//
//} 

 
public class InheritanceProblem {
    
}
