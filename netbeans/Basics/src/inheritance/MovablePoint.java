/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package inheritance;

import java.util.Arrays;

/**
 *
 * @author bas200190
 */
public class MovablePoint extends Point {
    private float xSpeed;
    private float ySpeed;
    
    public MovablePoint(){
      xSpeed = 0.0f;
      ySpeed = 0.0f;
    }
    
    public MovablePoint(float x,float y,float xSpeed,float ySpeed){
      super(x,y);
      this.xSpeed = xSpeed;
      this.ySpeed = ySpeed;
    }
    
    
    public float getXSpeed(){
      return this.xSpeed;
    }
    
    public void setXSpeed(float xSpeed){
      this.xSpeed = xSpeed;
    }
    
    public float getYSpeed(){
      return this.ySpeed;
    }
    
    public void setYSpeed(float ySpeed){
      this.ySpeed = ySpeed;
    }
    
    public void setSpeed(float xSpeed,float ySpeed){
      this.xSpeed = xSpeed;
      this.ySpeed = ySpeed;
    }
    
    public float[] getSpeed(){
      float[] a = new float[2];
      a[0] = xSpeed;
      a[1] = ySpeed;
      return a;
    }
    
    @Override
    public String toString(){
      return super.toString()+",speed=("+this.xSpeed+","+this.ySpeed+")";
    }
    
    public MovablePoint move(){
        setX(getX()+this.xSpeed);
        setY(getY()+this.ySpeed);
        return this;
    }
      
  
    public static void main(String[] args) {
        MovablePoint mp1 = new MovablePoint();
        System.out.println(mp1);
        System.out.println(mp1.getXSpeed());
        System.out.println(mp1.getYSpeed());
        mp1.setSpeed(5.7f,8.6f);
        float[] mp1arr = mp1.getSpeed();
        System.out.println(Arrays.toString(mp1arr));
        System.out.println(mp1.move());
        
        MovablePoint mp2 = new MovablePoint(2.3f,4.5f,8.9f,5.4f);
        System.out.println(mp2);
        System.out.println(mp2.getXSpeed());
        System.out.println(mp2.getYSpeed());
        mp2.setSpeed(4.3f,6.9f);
        float[] mp2arr = mp2.getSpeed();
        System.out.println(Arrays.toString(mp2arr));
        System.out.println(mp2.move());
    }
}
