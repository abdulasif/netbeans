/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhataBookMySQL;

import java.sql.Statement;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
//import java.io.IOException;

/**
 *
 * @author bas200190
 */
public class PaymentService {
    
    public int setPaymentId(Connection connection) throws Exception {

        String query = "select * from KB_payment";
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(query);
        int customer_id = 0;
        while (rs.next()) {
            customer_id = rs.getInt(2);

        }

        if (customer_id != 0) {
            customer_id = customer_id + 1 ;
        } else {
            customer_id = 1;

        }
        System.out.println(customer_id);
        return customer_id;
    }

    public void payThisOrder(int customer_id, double amount, Connection connection) throws Exception{
        try {
            double current_debit = currentDebit(customer_id, connection);
            System.out.println("Your This Order's Total Price is: " + amount);

            boolean correct_price_input_loop = true;
            while (correct_price_input_loop) {
                System.out.println("Enter The Amount:");
                System.out.println("");
                double pto_price_input = 0;

                pto_price_input = Double.parseDouble(KhataBook.ob.br.readLine());

                if (pto_price_input == amount) {

                    String query = "update KB_customer set customer_debit = ? where customer_id = ?";

                    try (PreparedStatement ps = connection.prepareStatement(query);) {
                        double final_debit = (current_debit - amount);

                        ps.setDouble(1, final_debit);
                        ps.setInt(2, customer_id);

                        int result = ps.executeUpdate();
                        if (result == 1) {
                            System.out.println("Enter Payment ID : ");
                            int payment_id = setPaymentId(connection);

                            Payment payment = new Payment(payment_id , customer_id , amount);
                            
                            
                            String query_1 = "insert into KB_payment values (default , ? , ? , ?)";

                            try (PreparedStatement ps_1 = connection.prepareStatement(query_1);) {
                                ps_1.setInt(1, payment.getPayment_id());
                                ps_1.setInt(2, payment.getCustomer_id());
                                ps_1.setDouble(3, payment.getPaid_amount());

                                int result_1 = ps_1.executeUpdate();
                                if (result_1 == 1) {
                                    System.out.println("Payment table is updated..");
                                    System.out.println("PAID");
                                    correct_price_input_loop = false;
                                } else {
                                    System.out.println("Payment table is not updated..");
                                }
                            }
                        } else {
                            System.out.println("Not entering into payment procedure...");
                        }
                    }

                } else {
                    System.out.println("Please Enter Correct amount..");
                }

            }
        } catch (IOException | NumberFormatException | SQLException e) {
            System.out.println("Payment is not Done..");
        }

    }

    public void payPartially(int customer_id, Connection connection) throws Exception {
        try {
            double current_debit = currentDebit(customer_id, connection);
            System.out.println("CURRENT DEBIT : " + current_debit);
            boolean correct_price_input_loop = true;
            while (correct_price_input_loop) {
                System.out.println("Enter The Amount:");
                double partial_amount_input = 0;

                partial_amount_input = Double.parseDouble(KhataBook.ob.br.readLine());

                if (partial_amount_input > 0) {
                    String query = "update KB_customer set customer_debit = ? where customer_id = ?";

                    try (PreparedStatement ps = connection.prepareStatement(query);) {
                        double final_debit = (current_debit - partial_amount_input);

                        ps.setDouble(1, final_debit);
                        ps.setInt(2, customer_id);

                        int result = ps.executeUpdate();
                        if (result == 1) {
                            System.out.println("Enter Payment ID : ");
                            int payment_id = setPaymentId(connection);
                            
                            
                            Payment payment = new Payment(payment_id , customer_id , partial_amount_input);
                            
                            String query_1 = "insert into KB_payment values (default , ? , ? , ?)";

                            try (PreparedStatement ps_1 = connection.prepareStatement(query_1);) {
                                ps_1.setInt(1, payment.getPayment_id());
                                ps_1.setInt(2, payment.getCustomer_id());
                                ps_1.setDouble(3, payment.getPaid_amount());

                                int result_1 = ps_1.executeUpdate();
                                if (result_1 == 1) {
                                    System.out.println("Payment table is updated..");
                                    correct_price_input_loop = false;
                                } else {
                                    System.out.println("Payment table is not updated..");
                                }
                            }
                        } else {
                            System.out.println("Not entering into payment procedure...");
                        }
                    }

                } else {
                    System.out.println("Please Enter Correct amount more than 0..");
                }
            }
        } catch (IOException | NumberFormatException | SQLException e) {
            System.out.println("Pay Partially is not Done..");
        }

    }

    public double currentDebit(int cd_customer_id, Connection connection) throws SQLException {

        String query_1 = "select customer_debit from KB_customer where customer_id = ?";
        double current_debit = 0;

        try (PreparedStatement ps_1 = connection.prepareStatement(query_1)) {
            ps_1.setInt(1, cd_customer_id);
            try (ResultSet rs = ps_1.executeQuery()) {
                if (rs.next()) {
                    current_debit = rs.getDouble(1);

                }
            }
        }
        return current_debit;

    }
    
    public void showPaymentDetails(Connection connection) {
        try {
            String query = "select * from KB_payment";

            try (PreparedStatement ps = connection.prepareStatement(query); ResultSet rs = ps.executeQuery();) {

                Design.paymentDetailsDesign();
                while (rs.next()) {
                    System.out.format("\n|%-20s|%-20s|%-20s|%-20s|", rs.getDate(1), rs.getInt(2), rs.getInt(3), rs.getDouble(4));
                    System.out.print("\n+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");
                }
                System.out.println("");
                System.out.println("");
                System.out.println("");
            }
        } catch (SQLException e) {
            System.out.println("Show Payment Details is not Done");
        }
    }

}
