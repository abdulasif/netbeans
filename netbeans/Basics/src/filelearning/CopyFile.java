/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package filelearning;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;



/**
 *
 * @author bas200190
 */
public class CopyFile {
    public static void main(String[] args) {
        try{
          InputStream is = new FileInputStream("//home//bas200190//Downloads//starelectrical.jpeg");
          OutputStream os = new FileOutputStream("//home//bas200190//Downloads//electricals.jpeg");
          int content = is.read();
          while(content != -1){
            os.write(content);
            content = is.read();
          }
          is.close();
          os.close();
        }catch(IOException ioe){
          ioe.printStackTrace();
        }
        
    }
}
