/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package inheritance;

/**
 *
 * @author bas200190
 */
public class Circle1 extends Shape1{
    private double radius;
    
    Circle1(){
      this(1.0);
    }
    Circle1(double radius){
      this(radius,"red",true);
    }
    
    Circle1(double radius,String color,boolean filled){
      super(color,filled);
      this.radius = radius;
    }
    
    public double getRadius(){
      return this.radius;
    }
    
    public void setRadius(double radius){
      this.radius = radius;
    }
    
    public double getArea(){
      return 3.14*(radius*radius);
    }
    
    public double getPerimeter(){
      return 2*3.14*radius;
    }
    
    @Override
    public String toString(){
      return getColor()+" "+isFilled()+" "+getRadius();
      
    }
    
    public static void main(String[] args) {
        Circle1 c1 = new Circle1();
        System.out.println(c1);
        System.out.println(c1.getArea());
        System.out.println(c1.getPerimeter());
        System.out.println(c1.getRadius());
        System.out.println(c1.getColor());
        System.out.println(c1.isFilled());
        
        Circle1 c2 = new Circle1(2.2);
        System.out.println(c2);
        System.out.println(c2.getArea());
        System.out.println(c2.getPerimeter());
        System.out.println(c2.getRadius());
        System.out.println(c2.getColor());
        System.out.println(c2.isFilled());
        
        Circle1 c3 = new Circle1(7.8,"black",false);
        System.out.println(c3);
        System.out.println(c3.getArea());
        System.out.println(c3.getPerimeter());
        System.out.println(c3.getRadius());
        System.out.println(c3.getColor());
        System.out.println(c3.isFilled());
    }
}
