class RnaTranscription {

    String transcribe(String dnaStrand) {
        String temp = dnaStrand.replaceAll("G","1").replaceAll("C", "2").replaceAll("T","3").replaceAll("A","4");
        return temp.replaceAll("1","C").replaceAll( "2","G").replaceAll("3","A").replaceAll("4","U");
    }

    public static void main(String[] args) {
        RnaTranscription ex1 = new RnaTranscription();
        System.out.println(ex1.transcribe("GCTA"));
    }
}
