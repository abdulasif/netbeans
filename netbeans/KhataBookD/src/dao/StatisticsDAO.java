/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package dao;

import java.sql.Connection;

/**
 *
 * @author bas200190
 */
public interface StatisticsDAO {
     public void todaysCollection(Connection connection);
     public void totalCustomerDebits(Connection connection);
     public void currentStockPrice(Connection connection);
     public void highSellingProductsList(Connection connection);
}
