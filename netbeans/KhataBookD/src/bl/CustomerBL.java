/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bl;

import dao.KhataBookDAO;
import java.sql.Connection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import model.Customer;

/**
 *
 * @author bas200190
 */
public class CustomerBL {

    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    
    public static Customer addCustomer(KhataBookDAO kbd , Connection connection) {
        Customer customer = null;
        try {
            System.out.println("*****Enter Customer Details*****");

            System.out.println("Enter the Customer ID:");
            int set_customer_id = kbd.getCustomerDAOimpl().setCustomerId(connection);
            System.out.println("Enter the Customer Name:");
            String set_customer_name = br.readLine();
            System.out.println("Enter the Phone Number:");
            String set_customer_phoneNo = br.readLine();
            System.out.println("Enter the Customer Address:");
            String set_customer_address = br.readLine();

            customer = new Customer(set_customer_id, set_customer_name, set_customer_phoneNo, set_customer_address, 0);

        } catch (IOException e) {
            System.out.println("Please Enter correct Input ..");
        }
        return customer;
    }
}
