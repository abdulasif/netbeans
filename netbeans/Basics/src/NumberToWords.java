
import java.util.*;

public class NumberToWords {

    static int concat(int a, int b) {
        String s1 = Integer.toString(a);
        String s2 = Integer.toString(b);
        String s = s1 + s2;
        int c = Integer.parseInt(s);
        return c;
    }

    public static void main(String[] args) {
        String[] a = {"", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
        String[] b = {"", "", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety"};
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a number to get words of number:");
        int input = sc.nextInt();
        if (input < 20) {
            System.out.println(a[input] + " rupees ");
        } else if (input > 19 && input < 100) {
            System.out.println(b[input / 10] + " " + a[input % 10] + " rupees ");
        } else if (input >= 100 && input < 1000) {

            int input_3rd = input % 10;
            int input_2nd = (input / 10) % 10;
            int temp = concat(input_2nd, input_3rd);
            int input_1st = input / 100;
            System.out.println(a[input_1st] + ((input_2nd != 0 || input_3rd != 0) ? " hundred and " : " hundred ") + ((temp < 20) ? a[temp] : b[input_2nd] + " " + a[input_3rd]) + " rupees ");
        } else if (input >= 1000 && input < 20000) {
            int input_4th = input % 10;
            int input_3rd = (input / 10) % 10;
            int temp = concat(input_3rd, input_4th);
            int input_2nd = (input / 100) % 10;
            int input_1st = input / 1000;
            System.out.println(a[input_1st] + ((input_2nd != 0 || input_3rd != 0 || input_4th != 0) ? " thousand and " : " thousand ") + a[input_2nd] + ((input_2nd != 0) ? ((input_3rd != 0 || input_4th != 0) ? " hundred and " : " hundred ") : "") + ((temp < 20) ? a[temp] : b[input_3rd] + " " + a[input_4th]) + " rupees ");
        } else if (input >= 20000 && input < 100000) {
            int input_5th = input % 10;
            int input_4th = (input / 10) % 10;
            int temp = concat(input_4th, input_5th);
            int input_3rd = (input / 100) % 10;
            int input_2nd = (input / 1000) % 10;
            int input_1st = input / 10000;
            System.out.println(b[input_1st] + " " + a[input_2nd] + ((input_3rd != 0 || input_4th != 0 || input_5th != 0) ? " thousand and " : " thousand ") + a[input_3rd] + ((input_3rd != 0) ? ((input_4th != 0 || input_5th != 0) ? " hundred and " : " hundred ") : "") + ((temp < 20) ? a[temp] : b[input_4th] + " " + a[input_5th]) + " rupees ");

        } else if (input >= 100000 && input < 2000000) {
            int input_6th = input % 10;
            int input_5th = (input / 10) % 10;
            int temp2 = concat(input_5th, input_6th);
            int input_4th = (input / 100) % 10;
            int input_3rd = (input / 1000) % 10;
            int input_2nd = (input / 10000) % 10;
            int temp1 = concat(input_2nd, input_3rd);
            int input_1st = input / 100000;
            System.out.println(a[input_1st] + ((input_2nd == 0 && input_3rd == 0 && input_4th == 0 && input_5th == 0 && input_6th == 0) ? " lakhs " : " lakhs and ") + ((temp1 < 20) ? a[temp1] : b[input_2nd] + "" + a[input_3rd]) + ((input_2nd != 0 || input_3rd != 0) ? ((input_4th != 0 || input_5th != 0 || input_6th != 0) ? " thousand and " : " thousand ") : "") + a[input_4th] + ((input_4th != 0) ? ((input_5th != 0 || input_6th != 0) ? " hundred and " : " hundred ") : "") + ((temp2 < 20) ? a[temp2] : b[input_5th] + " " + a[input_6th]) + " rupees ");
        } else if (input >= 2000000 && input < 10000000) {
            int input_7th = input % 10;
            int input_6th = (input / 10) % 10;
            int temp2 = concat(input_6th, input_7th);
            int input_5th = (input / 100) % 10;
            int input_4th = (input / 1000) % 10;
            int input_3rd = (input / 10000) % 10;
            int temp1 = concat(input_3rd, input_4th);
            int input_2nd = (input / 100000) % 10;
            int input_1st = input / 1000000;
            System.out.println(b[input_1st] + " " + a[input_2nd] + ((input_3rd == 0 && input_4th == 0 && input_5th == 0 && input_6th == 0 && input_7th == 0) ? " lakhs " : " lakhs and ") + ((temp1 < 20) ? a[temp1] : (b[input_3rd] + " " + a[input_4th])) + ((input_3rd != 0 || input_4th != 0) ? ((input_5th != 0 || input_6th != 0 || input_7th != 0) ? " thousand and " : " thousand ") : "  ") + a[input_5th] + ((input_5th != 0) ? ((input_6th == 0 && input_7th == 0) ? " hundred " : " hundred and ") : " ") + ((temp2 < 20) ? a[temp2] : (b[input_6th] + " " + a[input_7th])) + " rupees ");
        } else if (input == 10000000) {
            System.out.println("one crore rupees");
        }
    }
}
