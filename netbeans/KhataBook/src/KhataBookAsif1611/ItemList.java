/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhataBookAsif1611;



/**
 *
 * @author bas200190
 */
public class ItemList {
    int order_id;
    int product_id;
    int product_quantity;
    double each_price;
    double price;

    ItemList() {
    }

    ItemList(int order_id, int product_id , int product_quantity, double eachprice, double price) {
        this.order_id = order_id;
        this.product_id = product_id ;
        this.product_quantity = product_quantity;
        this.each_price = eachprice;
        this.price = eachprice * product_quantity;
    }


}
