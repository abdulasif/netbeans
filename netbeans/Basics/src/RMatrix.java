
import java.util.Scanner;

public class RMatrix {

    public static int[][] R(int arr[][], int sizea, int sizeb) {
        int b[][] = new int[sizea][sizeb];
        int k = 0;
        for (int i = 0; i < b.length; i++) {                   //R
            for (int j = (b[i].length - 1); j >= 0; j--) {
                b[i][k] = arr[j][i];
                k++;
            }
            k = 0;
        }

        return b;
    }

    public static int[][] L(int arr[][], int sizea, int sizeb) {
        int b[][] = new int[sizea][sizeb];                    //L
        int l = b.length - 1;
     for (int i = 0; i <b.length; i++) {
            for (int j = 0; j < b[i].length; j++) {
                b[i][j] = arr[j][l];
            }
            l--;
        }

        return b;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the row size:");
        int sizea = sc.nextInt();
        System.out.println("Enter the coloumn size:");
        int sizeb = sc.nextInt();
        int a[][] = new int[sizea][sizeb];
        System.out.println("Enter the elements");
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                a[i][j] = sc.nextInt();
            }
        }
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println("");
        }

        System.out.println("The instruction was only 3 characters in length");
        System.out.println("Enter the 1st Instruction:");
        char instruction1 = sc.next().charAt(0);
        int ans[][] = new int[sizea][sizeb];
        if (instruction1 == 'R') {
            ans = R(a, sizea, sizeb);
        } else if (instruction1 == 'L') {
            ans = L(a, sizea, sizeb);
        }

        for (int i = 0; i < ans.length; i++) {
            for (int j = 0; j < ans[i].length; j++) {
                System.out.print(ans[i][j] + " ");
            }
            System.out.println("");
        }
        /*int b[][] = new int[sizea][sizeb];
        int k = 0;
        for (int i = 0; i < b.length; i++) {                   //R
            for (int j = (b[i].length - 1); j >= 0; j--) {
                b[i][k] = a[j][i];
                k++;
            }
            k = 0;
        }

        int c[][] = new int[sizea][sizeb];                    //L
        int l = c.length - 1;
        for (int i = 0; i < c.length; i++) {
            for (int j = 0; j < c[i].length; j++) {
                c[i][j] = a[j][l];
            }
            l--;
        }*/
    }

}
