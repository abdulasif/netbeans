/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package dao;

import java.sql.Connection;
import model.Customer;
import model.Orders;
import model.Product;

/**
 *
 * @author bas200190
 */
public interface OrdersDAO {
    
    public int setOrderId(Connection connection);
    public void addOrder(KhataBookDAO kbd , Orders order , Connection connection);
    public void viewAllOrder(Connection connection);
    public void viewParticularOrder(int vpo_order_id, Connection connection) ;
    public Customer customerDetailsWRTPhoneNo(String customer_phone_no, Connection connection) ;
    public Customer customerDetailsWRTCustomerId(int customer_id, Connection connection) ;
    public Product productDetailsWRTProductId(int product_id , Connection connection) ;
    public void updateProductAfterPurchase(int product_id , int product_quantity , Connection connection) ;
    public void updateCustomerAfterPurchase(int customer_id , double latest_customer_debit , Connection connection) ;
    
}
