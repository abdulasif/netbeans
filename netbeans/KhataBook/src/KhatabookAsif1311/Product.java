/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhatabookAsif1311;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author bas200190
 */
public class Product {
    int product_id;
    String product_name;
    String product_weight;
    int product_buying_price;
    int product_selling_price;
    String product_brand;
    int product_quantity;
     Product[] product_details = new Product[5];
    Product(){
    
    }
    Design de2 = new Design();
    Product(int product_id,String product_name,String product_weight,int product_buying_price,int product_selling_price,String product_brand,int product_quantity){
       
      this.product_id = product_id;
      this.product_name = product_name;
      this.product_weight = product_weight;
      this.product_buying_price = product_buying_price;
      this.product_selling_price = product_selling_price;
      this.product_brand = product_brand;
      this.product_quantity = product_quantity;
    }
    
    public void addProduct(){
        String get_product_name = null;
        String get_product_weight  = null;
        int get_product_buying_price  = 0;
        int get_product_selling_price = 0;
        String get_product_brand = null;
        int get_product_quantity = 0;
        try{
        product_details = Arrays.copyOf(product_details,product_details.length+1 );
        System.out.println("Enter Product Name:");
        get_product_name = KhataBook.ob.br.readLine();
        System.out.println("Enter Product Weight:");
        get_product_weight = KhataBook.ob.br.readLine();
        System.out.println("Enter Product Buying Price:");
        get_product_buying_price = Integer.parseInt(KhataBook.ob.br.readLine());
        System.out.println("Enter Product Selling Price:");
        get_product_selling_price = Integer.parseInt(KhataBook.ob.br.readLine());
        System.out.println("Enter Product Brand:");
        get_product_brand = KhataBook.ob.br.readLine();
        System.out.println("Enter Product Quantity:");
        get_product_quantity = Integer.parseInt(KhataBook.ob.br.readLine());
        }
        catch(IOException ioe){
            
        }
        product_details[product_details.length-1] = new Product(product_details.length+500,get_product_name,get_product_weight,get_product_buying_price,get_product_selling_price,get_product_brand,get_product_quantity);
       
    }
    
    public void viewAllProducts(){
        de2.viewProductDesign();
        for (int i = 0; i < product_details.length; i++) {
            System.out.print(product_details[i]);
            System.out.print("\n+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");
            
        }
    }
    
    public void viewParticularProduct(){
        int vpp_product_id = 0;
        try{
        System.out.println("Enter the Product ID:");
        vpp_product_id = Integer.parseInt(KhataBook.ob.br.readLine());
        }
        catch(IOException ioe){
            
        }
        for (int i = 0; i < product_details.length; i++) {
            if (product_details[i].product_id==vpp_product_id) {
                de2.viewProductDesign();
                System.out.print(product_details[i]);
                System.out.print("\n+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");
                break;
            }
            
        }
        
    }
    
    public void updateProduct(){
        //Scanner sc = new Scanner(System.in);
        try{
        System.out.println("Enter the Product ID:");
        int up_product_id = Integer.parseInt(KhataBook.ob.br.readLine());
        for (int i = 0; i < product_details.length; i++) {
            if (product_details[i].product_id == up_product_id) {
                System.out.println("*****Enter Product's New Details*****");
                System.out.println("Enter the Product Name:");
                product_details[i].product_name = KhataBook.ob.br.readLine();
                System.out.println("Enter Product Weight:");
                product_details[i].product_weight =KhataBook.ob.br.readLine();
                System.out.println("Enter Product Buying Price:");
                product_details[i].product_buying_price = Integer.parseInt(KhataBook.ob.br.readLine());
                System.out.println("Enter Product Selling Price:");
                product_details[i].product_selling_price = Integer.parseInt(KhataBook.ob.br.readLine());
                System.out.println("Enter Product Brand:");
                product_details[i].product_brand = KhataBook.ob.br.readLine();
                System.out.println("Enter Product Quantity:");
                product_details[i].product_quantity = Integer.parseInt(KhataBook.ob.br.readLine());

            }
        }
        }
        catch(IOException ioe){
        
        }
    }
    
    @Override
    public String toString(){
      return String.format("\n|%-20d|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|", this.product_id, this.product_name, this.product_weight,this.product_buying_price,this.product_selling_price, this.product_brand, this.product_quantity);
    }
  
}
