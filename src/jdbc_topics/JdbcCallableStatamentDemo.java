package jdbc_topics;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JdbcCallableStatamentDemo {

    public static void main(String[] args) {

        String DB_URL = "jdbc:mysql://localhost/hari";
        String USER = "root";
        String PASS = "root";
        String QUERY = "{call getEmployeeByDesignation(?)}";
        try {
            // creating DB connection
            Connection connection = DriverManager.getConnection(DB_URL, USER, PASS);

            //creating callable statement
            CallableStatement prepareCall = connection.prepareCall(QUERY);

            //setting value for the parameters
            prepareCall.setString("des", "SENIOR DEVELOPER");

            //executing the query
            ResultSet resultSet = prepareCall.executeQuery();

            //processing the retrived data
            while (resultSet.next()) {
                System.out.println("\n---------------------------------------------------");
                System.out.println("EMPLOYEE ID : " + resultSet.getInt("EmployeeID"));
                System.out.println("FIRST NAME : " + resultSet.getString("FirstName"));
                System.out.println("LAST NAME : " + resultSet.getString("LastName"));
                System.out.println("TECHNOLOGY : " + resultSet.getString("Technology"));
                System.out.println("DESIGNATION : " + resultSet.getString("Designation"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(JdbcCallableStatamentDemo.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
