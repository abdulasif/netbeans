/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package main;

import bl.CustomerBL;
import bl.Design;
import bl.OrdersBL;
import bl.ProductBL;
import dao.KhataBookDAO;
import daoMysql.MySQLKhataBookDAOimpl;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.InputMismatchException;
import java.util.Objects;
import model.Customer;
import model.Orders;
import model.Product;

/**
 *
 * @author bas200190
 */
public class KhataBook {

    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) {
        KhataBookDAO kbdaoimpl = new MySQLKhataBookDAOimpl();

        String url = "jdbc:mysql://bassure.in:3306/abdulasif_b8_db";
        String user_name = "abdulasif";
        String password = "sibASdul@555";
        try (Connection connection = DriverManager.getConnection(url, user_name, password)) {
            boolean main_menu_loop = true;
            while (main_menu_loop) {
                Design.mainMenuDesign();
                //try {
                int main_menu_input = Integer.parseInt(br.readLine());
                switch (main_menu_input) {

                    case 1:
                        boolean customer_menu_loop = true;

                        while (customer_menu_loop) {
                            Design.customerMenuDesign();
                            int customer_menu_input = Integer.parseInt(br.readLine());

                            switch (customer_menu_input) {
                                case 1:
                                    Customer customer = CustomerBL.addCustomer(kbdaoimpl, connection);
                                    kbdaoimpl.getCustomerDAOimpl().addCustomer(connection, customer);
                                    break;
                                case 2:
                                    boolean view_customer_loop = true;
                                    while (view_customer_loop) {
                                        Design.viewCustomerMenuDesign();
                                        int view_customer_input = Integer.parseInt(br.readLine());

                                        switch (view_customer_input) {
                                            case 1:
                                                kbdaoimpl.getCustomerDAOimpl().viewParticularCustomer(connection);
                                                break;
                                            case 2:

                                                kbdaoimpl.getCustomerDAOimpl().viewAllCustomer(connection);
                                                break;
                                            case 3:
                                                view_customer_loop = false;
                                                break;

                                        }
                                    }
                                    break;
                                case 3:
                                    kbdaoimpl.getCustomerDAOimpl().updateCustomer(connection);
                                    break;
                                case 4:

                                    Orders order = OrdersBL.addOrder(kbdaoimpl, connection);
                                    kbdaoimpl.getOrdersDAOimpl().addOrder(kbdaoimpl, order, connection);
                                    
                                    break;
                                case 5:
                                    boolean view_order_loop = true;
                                    while (view_order_loop) {
                                        Design.viewOrderMenuDesign();
                                        int view_order_input = Integer.parseInt(br.readLine());
                                        switch (view_order_input) {
                                            case 1:
                                                try {
                                                System.out.println("Enter the Order ID:");
                                                int order_id = Integer.parseInt(br.readLine());
                                                kbdaoimpl.getOrdersDAOimpl().viewParticularOrder(order_id, connection);
                                            } catch (NumberFormatException ioe) {
                                                System.out.println("Please Enter Valid Input");
                                            }
                                            break;

                                            case 2:
                                                kbdaoimpl.getOrdersDAOimpl().viewAllOrder(connection);
                                                break;
                                            case 3:
                                                view_order_loop = false;
                                                break;

                                        }
                                    }
                                    break;
                                case 6:
                                    int pp_customer_id = 0;

                                    boolean make_payment = true;
                                    while (make_payment) {
                                        System.out.println("Enter Customer ID:");
                                        try {
                                            pp_customer_id = Integer.parseInt(br.readLine());
                                        } catch (InputMismatchException ime) {
                                            System.out.println("Please Enter Valid Customer ID : ");
                                        }

                                        Customer customer_details = kbdaoimpl.getOrdersDAOimpl().customerDetailsWRTCustomerId(pp_customer_id, connection);
                                        if (Objects.nonNull(customer_details)) {

                                            double previous_debit = kbdaoimpl.getPaymentDAOimpl().currentDebit(pp_customer_id, connection);
                                            kbdaoimpl.getPaymentDAOimpl().payPartially(pp_customer_id, connection);
                                            double current_debit = kbdaoimpl.getPaymentDAOimpl().currentDebit(pp_customer_id, connection);
                                            if (previous_debit != current_debit) {
                                                System.out.println("CURRENT DEBIT : " + current_debit);
                                                make_payment = false;
                                            }
                                        }
//                                            
                                    }

                                    break;
                                case 7:
                                    kbdaoimpl.getPaymentDAOimpl().showPaymentDetails(connection);
                                    break;
                                case 8:
                                    customer_menu_loop = false;
                                    break;
                            }
                        }
                        break;
                    case 2:
                        boolean inventory_menu_loop = true;
                        while (inventory_menu_loop) {
                            Design.inventoryMenuDesign();
                            int inventory_menu_input = Integer.parseInt(br.readLine());

                            switch (inventory_menu_input) {
                                case 1:
                                    boolean add_product_loop = true;
                                    while (add_product_loop) {
                                        Product product = ProductBL.addProduct(kbdaoimpl, connection);
                                        kbdaoimpl.getProductDAOimpl().addProduct(product, connection);

                                        System.out.println("SUCCESSFULLY ADDED PRODUCT DETAILS....");
                                        System.out.println("To add More Product Press '1' (or) Exit Press '2'");
//                                        

                                        int add_more_product_input = Integer.parseInt(br.readLine());

                                        switch (add_more_product_input) {
                                            case 1:
                                                add_product_loop = true;
                                                break;
                                            case 2:
                                                add_product_loop = false;
                                                break;
                                        }
//                                        
                                    }

                                    break;
                                case 2:

                                    boolean view_product_loop = true;
                                    while (view_product_loop) {
                                        Design.viewProductMenuDesign();
                                        int view_product_input = Integer.parseInt(br.readLine());
                                        switch (view_product_input) {
                                            case 1:
                                                kbdaoimpl.getProductDAOimpl().viewParticularProduct(connection);
                                                break;
                                            case 2:
                                                kbdaoimpl.getProductDAOimpl().viewAllProducts(connection);
                                                break;
                                            case 3:
                                                view_product_loop = false;
                                                break;
                                        }
                                    }

                                    break;
                                case 3:
                                    kbdaoimpl.getProductDAOimpl().topUpProduct(connection);
                                    break;
                                case 4:
                                    kbdaoimpl.getProductDAOimpl().updateProduct(connection);
                                    break;
                                case 5:
                                    inventory_menu_loop = false;
                                    break;
                            }
                        }

                        break;
                    case 3:
                        boolean statistics_menu_loop = true;
                        while (statistics_menu_loop) {
                            Design.statisticsMenu();
                            int statistics_menu_input = Integer.parseInt(br.readLine());
                            switch (statistics_menu_input) {
                                case 1:
                                    kbdaoimpl.getStatisticsDAOimpl().todaysCollection(connection);
                                    break;
                                case 2:
                                    kbdaoimpl.getStatisticsDAOimpl().highSellingProductsList(connection);
                                    break;
                                case 3:
                                    kbdaoimpl.getStatisticsDAOimpl().totalCustomerDebits(connection);
                                    break;
                                case 4:
                                    kbdaoimpl.getStatisticsDAOimpl().currentStockPrice(connection);
                                    break;
                                case 5:
                                    statistics_menu_loop = false;
                                    break;
                            }
                        }

                        break;
                    case 4:
                        main_menu_loop = false;
                        break;
                }
//                } catch (NumberFormatException e) {
//                    System.out.println("Number form excep is raised..");
//                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("connection is lost..");
        }
    }
}
