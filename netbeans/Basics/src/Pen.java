
import java.time.LocalDateTime;


public class Pen implements Cloneable {

    String brand;  
    String color;
    private double price;
    public final LocalDateTime MFG_DATE;
    public static String category; 
    {
      MFG_DATE = LocalDateTime.now();
    }
    
    static{
      category = "Writing Instruments";
    }

    public Pen() {
        this("cello");
//        brand = "cello";
//        color = "Black";
//        price = 20;
    }

    
    
    public void setPrice(double price) {
        if (price<5.0) {
            System.out.println("Illegal price value");
        }else
        {
          this.price = price;
        }
    }
    
    public double getPrice() {
        return price;
    }

    public Pen(String brand) {
        this(brand,"blue",10);
//        this.brand = brand;
//        color = "Black";
//        price = 10.0;
    }

    public Pen(String brand, String color) {
        this(brand,color,10);
//        this.brand = brand;
//        this.color = color;
//        price = 10.0;
    }

    public Pen(String brand, String color, double price) {
        this.brand = brand;
        this.color = color;
        this.price = price;
    }

    public static void main(String[] args) throws CloneNotSupportedException {
        Pen p1 = new Pen("cello","black",7);
        
    }
}
