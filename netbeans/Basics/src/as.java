import java.util.Scanner;
public class as {

    static boolean alternatesign(int[] arr) {
        boolean sign = false;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] > 0) {
                if (arr[i + 1] < 0) {
                    sign = true;
                } else {
                    sign = false;
                    break;
                }
            } 
            else if (arr[i] < 0) {
                if (arr[i + 1] > 0) {
                    sign = true;
                } else {
                    sign = false;
                    break;
                }
            } else {
                sign = false;
                break;
            }
        }
        return sign;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the size of the array:");
        int size =sc.nextInt();
        int[] a = new int[size];
        System.out.println("Enter the elements");
        for (int i = 0; i < a.length; i++) {
            a[i] = sc.nextInt();
        }
        System.out.println("The gn elements signs are "+alternatesign(a));
    }
}
