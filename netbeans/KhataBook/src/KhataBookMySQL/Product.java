/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhataBookMySQL;



/**
 *
 * @author bas200190
 */
public class Product {

    private int product_id;
    private String product_name;
    private String product_weight;
    private double product_buying_price;
    private double product_selling_price;
    private String product_brand;
    private int product_quantity;

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_weight() {
        return product_weight;
    }

    public void setProduct_weight(String product_weight) {
        this.product_weight = product_weight;
    }

    public double getProduct_buying_price() {
        return product_buying_price;
    }

    public void setProduct_buying_price(double product_buying_price) {
        this.product_buying_price = product_buying_price;
    }

    public double getProduct_selling_price() {
        return product_selling_price;
    }

    public void setProduct_selling_price(double product_selling_price) {
        this.product_selling_price = product_selling_price;
    }

    public String getProduct_brand() {
        return product_brand;
    }

    public void setProduct_brand(String product_brand) {
        this.product_brand = product_brand;
    }

    public int getProduct_quantity() {
        return product_quantity;
    }

    public void setProduct_quantity(int product_quantity) {
        this.product_quantity = product_quantity;
    }
    
    Product(int product_id,String product_name,String product_weight,double product_buying_price,double product_selling_price,String product_brand,int product_quantity){
       
      this.product_id = product_id;
      this.product_name = product_name;
      this.product_weight = product_weight;
      this.product_buying_price = product_buying_price;
      this.product_selling_price = product_selling_price;
      this.product_brand = product_brand;
      this.product_quantity = product_quantity;
    }
    
    

}
