/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhataBookAsif1611;


import java.io.IOException;
import java.util.Arrays;


/**
 *
 * @author bas200190
 */
public class Payment {
    
    public void payThisOrder(int pto_customer_id,int pto_order_index){
        //Scanner sc = new Scanner(System.in);
        try{
        for (int i = 0; i < KhataBook.ob.kd.customer_details.length; i++) {
            if (KhataBook.ob.kd.customer_details[i].customer_id==pto_customer_id) {
                System.out.println("Your This Order's Total Price is: "+KhataBook.ob.ord_det.order_details[pto_order_index].total_price);
                boolean correct_price_input_loop = true;
                while(correct_price_input_loop){
                    System.out.println("Enter The Amount:");
                    System.out.println("");
                    double pto_total_price_input = Double.parseDouble(KhataBook.ob.br.readLine());
                    if (pto_total_price_input==KhataBook.ob.ord_det.order_details[pto_order_index].total_price) {
                        KhataBook.ob.kd.customer_details[i].customer_debit = KhataBook.ob.kd.customer_details[i].customer_debit - pto_total_price_input;
                        KhataBook.ob.paydet.payment_details = Arrays.copyOf(KhataBook.ob.paydet.payment_details,KhataBook.ob.paydet.payment_details.length+1);
                        KhataBook.ob.paydet.payment_details[KhataBook.ob.paydet.payment_details.length-1] = new PaymentDetails(100+KhataBook.ob.paydet.payment_details.length,KhataBook.ob.kd.customer_details[i].customer_id,KhataBook.ob.ord_det.order_details[pto_order_index].total_price); 
                        System.out.println("PAID");
                        correct_price_input_loop = false;
                    }
                }
            }
        }
        }catch(IOException ioe){
        
        }
    }
    
    public void payPartially(int pp_customer_id){
        //Scanner sc = new Scanner(System.in);
        try{
        for (int i = 0; i < KhataBook.ob.kd.customer_details.length; i++) {
            if (KhataBook.ob.kd.customer_details[i].customer_id == pp_customer_id) {
                System.out.println("TOTAL DEBIT: " + KhataBook.ob.kd.customer_details[i].customer_debit);
                boolean correct_price_input_loop = true;
                while (correct_price_input_loop) {
                    System.out.println("Enter The Amount:");
                    double partial_amount_input = Double.parseDouble(KhataBook.ob.br.readLine());
                    if (partial_amount_input > 0) {
                        KhataBook.ob.kd.customer_details[i].customer_debit = KhataBook.ob.kd.customer_details[i].customer_debit - partial_amount_input;
                        KhataBook.ob.paydet.payment_details = Arrays.copyOf(KhataBook.ob.paydet.payment_details,KhataBook.ob.paydet.payment_details.length+1);
                        KhataBook.ob.paydet.payment_details[KhataBook.ob.paydet.payment_details.length-1] = new PaymentDetails(100+KhataBook.ob.paydet.payment_details.length,KhataBook.ob.kd.customer_details[i].customer_id,partial_amount_input); 
                        System.out.println("PAID");
                        correct_price_input_loop = false;
                    }
                }
            }
        }
        }
        catch(IOException ioe){
            
        }

        
    }
    
    
    public void currentDebit(int cd_customer_id) {
        for (int i = 0; i < KhataBook.ob.kd.customer_details.length; i++) {
            if (KhataBook.ob.kd.customer_details[i].customer_id == cd_customer_id) {
                System.out.println("CURRENT DEBIT : " + KhataBook.ob.kd.customer_details[i].customer_debit);
            }
        }

    }
    
    
}
