
public class intersection {
    public static void main(String[] args) {
        int[] uniarr = {10,20,30,20,40,10,50,60,70};
        int count=0;
        for (int r = 0; r < uniarr.length; r++) {           //giving 0s to rep elements
            for (int s = r; s < uniarr.length-1; s++) {
                
                  if(uniarr[r]==uniarr[s+1])
                  {
                    
                    if(uniarr[r]!=0 && uniarr[s+1]!=0)
                    {
                      count++;
                    }
                    uniarr[s+1]=0; 
                  }
            }
        }
   
       for(int q=0;q<count;q++)    
       {
         for (int g = 0; g < uniarr.length-1 ; g++) {     // swapping 0 with non zero  w.r.to how many zeros are there
                    if(uniarr[g]==0 && uniarr[g+1]!=0)
                    {
                      int temp1 =uniarr[g];
                      uniarr[g] = uniarr[g+1];
                      uniarr[g+1] = temp1;
                    }
          }
       }
        int[] array = new int[uniarr.length-count];        //creating new array	
        for (int x = 0; x < uniarr.length-count; x++) {
           array[x]=uniarr[x];
        }
       
        System.out.println("final output 'array':");       //creating new array without zeros
        for(int m:array)
            System.out.print(m+" ");
        System.out.println("");	        
    }
}
