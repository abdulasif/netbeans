/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhataBookMySQL;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.InputMismatchException;

/**
 *
 * @author bas200190
 */
public class KhataBook {

    public static ObjectsDatabase ob = new ObjectsDatabase();

    public static void main(String[] args) throws Exception {

        String url = "jdbc:mysql://bassure.in:3306/abdulasif_b8_db";
        String user_name = "abdulasif";
        String password = "sibASdul@555";
        try (Connection connection = DriverManager.getConnection(url, user_name, password)) {

            boolean main_menu_loop = true;
            while (main_menu_loop) {
                Design.mainMenuDesign();
                try {
                    int main_menu_input = Integer.parseInt(ob.br.readLine());
                    switch (main_menu_input) {

                        case 1:
                            boolean customer_menu_loop = true;

                            while (customer_menu_loop) {
                                Design.customerMenuDesign();
                                int customer_menu_input = Integer.parseInt(ob.br.readLine());

                                switch (customer_menu_input) {
                                    case 1:
                                        ob.addcus.addCustomer(connection);
                                        break;
                                    case 2:
                                        boolean view_customer_loop = true;
                                        while (view_customer_loop) {
                                            Design.viewCustomerMenuDesign();
                                            int view_customer_input = Integer.parseInt(ob.br.readLine());

                                            switch (view_customer_input) {
                                                case 1:
                                                    ob.addcus.viewParticularCustomer(connection);
                                                    break;
                                                case 2:
                                                    ob.addcus.viewAllCustomer(connection);
                                                    break;
                                                case 3:
                                                    view_customer_loop = false;
                                                    break;
                                            }
                                        }
                                        break;

                                    case 3:
                                        ob.addcus.updateCustomer(connection);
                                        break;

                                    case 4:
                                        ob.ord.addOrder(connection);
                                        break;

                                    case 5:
                                        boolean view_order_loop = true;
                                        while (view_order_loop) {
                                            Design.viewOrderMenuDesign();
                                            int view_order_input = Integer.parseInt(ob.br.readLine());
                                            switch (view_order_input) {
                                                case 1:
                                                    try {
                                                    System.out.println("Enter the Order ID:");
                                                    int order_id = Integer.parseInt(KhataBook.ob.br.readLine());
                                                    ob.ord_det.viewParticularOrder(order_id, connection);
                                                } catch (IOException ioe) {
                                                    System.out.println("Please Enter Valid Input");
                                                }
                                                break;
                                                case 2:
                                                    ob.ord_det.viewAllOrder(connection);
                                                    break;
                                                case 3:
                                                    view_order_loop = false;
                                                    break;

                                            }
                                        }
                                        break;
                                    case 6:
                                        int pp_customer_id = 0;

                                        boolean make_payment = true;
                                        while (make_payment) {
                                            System.out.println("Enter Customer ID:");
                                            try {
                                                pp_customer_id = Integer.parseInt(ob.br.readLine());
                                            } catch (InputMismatchException ime) {
                                                System.out.println("Please Enter Valid Customer ID : ");
                                            }
                                            double previous_debit = ob.pay.currentDebit(pp_customer_id, connection);
                                            ob.pay.payPartially(pp_customer_id, connection);
                                            double current_debit = ob.pay.currentDebit(pp_customer_id, connection);
                                            if (previous_debit != current_debit) {
                                                System.out.println("CURRENT DEBIT : " + current_debit);
                                                make_payment = false;
                                            }
//                                            
                                        }

                                        break;
                                    case 7:
                                        ob.pay.showPaymentDetails(connection);
                                        break;
                                    case 8:
                                        customer_menu_loop = false;
                                        break;
                                }
                            }

                            break;
                        case 2:
                            boolean inventory_menu_loop = true;
                            while (inventory_menu_loop) {
                                Design.inventoryMenuDesign();
                                int inventory_menu_input = Integer.parseInt(ob.br.readLine());

                                switch (inventory_menu_input) {
                                    case 1:
                                        boolean add_product_loop = true;
                                        while (add_product_loop) {
                                            ob.prod.addProduct(connection);

                                            System.out.println("SUCCESSFULLY ADDED PRODUCT DETAILS....");
                                            System.out.println("To add More Product Press '1' (or) Exit Press '2'");
//                                        

                                            int add_more_product_input = Integer.parseInt(ob.br.readLine());

                                            switch (add_more_product_input) {
                                                case 1:
                                                    add_product_loop = true;
                                                    break;
                                                case 2:
                                                    add_product_loop = false;
                                                    break;
                                            }
//                                        
                                        }

                                        break;
                                    case 2:

                                        boolean view_product_loop = true;
                                        while (view_product_loop) {
                                            Design.viewProductMenuDesign();
                                            int view_product_input = Integer.parseInt(ob.br.readLine());
                                            switch (view_product_input) {
                                                case 1:
                                                    ob.prod.viewParticularProduct(connection);
                                                    break;
                                                case 2:
                                                    ob.prod.viewAllProducts(connection);
                                                    break;
                                                case 3:
                                                    view_product_loop = false;
                                                    break;
                                            }
                                        }

                                        break;
                                    case 3:
                                        ob.prod.topUpProduct(connection);
                                        break;
                                    case 4:
                                        ob.prod.updateProduct(connection);
                                        break;
                                    case 5:
                                        inventory_menu_loop = false;
                                        break;
                                }
                            }

                            break;

                        case 3:
                            boolean statistics_menu_loop = true;
                            while (statistics_menu_loop) {
                                Design.statisticsMenu();
                                int statistics_menu_input = Integer.parseInt(ob.br.readLine());
                                switch (statistics_menu_input) {
                                    case 1:
                                        ob.stat.todaysCollection(connection);
                                        break;
                                    case 2:
                                        ob.stat.highSellingProductsList(connection);
                                        break;
                                    case 3:
                                        ob.stat.totalCustomerDebits(connection);
                                        break;
                                    case 4:
                                        ob.stat.currentStockPrice(connection);
                                        break;
                                    case 5:
                                        statistics_menu_loop = false;
                                        break;
                                }
                            }

                            break;
                        case 4:

                            main_menu_loop = false;
                            break;
                    }

                } catch (Exception e) {
                    System.out.println("Please Enter Valid Input");
                    e.printStackTrace();
                }
            }
        }
    }
}
