/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package khatabookasif;

/**
 *
 * @author bas200190
 */
import java.util.Arrays;
import java.util.Scanner;
public class Inventory {
    
    static int set_product_id = 706;
    int product_id;
    String product_name;
    String product_weight;
    int product_buying_price;
    int product_selling_price;
    String product_brand;
    String product_dealer;
    int product_quantity;
    
    Inventory[] product_details_arr = new Inventory[5];
    static int product_details_arr_index=5;
    Scanner sc = new Scanner(System.in);
    
    Inventory(){
    
    }
    
    Inventory(int product_id,String product_name,String product_weight,int product_buying_price,int product_selling_price,String product_brand,String product_dealer,int product_quantity){
       
      this.product_id = product_id;
      this.product_name = product_name;
      this.product_weight = product_weight;
      this.product_buying_price = product_buying_price;
      this.product_selling_price = product_selling_price;
      this.product_brand = product_brand;
      this.product_dealer = product_dealer;
      this.product_quantity = product_quantity;
    }
    public void addProduct(){
        product_details_arr = Arrays.copyOf(product_details_arr,product_details_arr.length+1 );
        System.out.println("Enter Product Name:");
        String get_product_name = sc.next();
        System.out.println("Enter Product Weight:");
        String get_product_weight = sc.next();
        System.out.println("Enter Product Buying Price:");
        int get_product_buying_price = sc.nextInt();
        System.out.println("Enter Product Selling Price:");
        int get_product_selling_price = sc.nextInt();
        System.out.println("Enter Product Brand:");
        String get_product_brand = sc.next();
        System.out.println("Enter Product Dealer:");
        String get_product_dealer = sc.next();
        System.out.println("Enter Product Quantity:");
        int get_product_quantity = sc.nextInt();
        product_details_arr[product_details_arr_index] = new Inventory(set_product_id,get_product_name,get_product_weight,get_product_buying_price,get_product_selling_price,get_product_brand,get_product_dealer,get_product_quantity);
        product_details_arr_index++;
        set_product_id++;
    }
    
    public void viewProducts(){
        for (int k = 0; k < product_details_arr.length; k++) {
            System.out.println(product_details_arr[k]);
        }
    }
    
    public void deleteProduct(){
        System.out.println("Enter the Product Id to Delete the Product:");
        int delete_product = sc.nextInt();
        for (int j = 0; j < product_details_arr.length; j++) {
            if (delete_product==product_details_arr[j].product_id) {
            }
        }
    }
    
    public void updateProduct(){
        
    }
    
    public String toString(){
       System.out.print("\n+"+"-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) +"+"+ "-".repeat(20)  + "+" + "-".repeat(20) + "+");
       System.out.format("\n|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|","PRODUCT ID", "PRODUCT NAME", "PRODUCT WEIGHT","PRODUCT BUY PRICE","CUSTOMER PRICE", "PRODUCT BRAND","PRODUCT DEALER", "PRODUCT QUANTITY");
       System.out.print("\n+"+"-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20)+ "+" + "-".repeat(20) + "+"  + "-".repeat(20)  + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");
       System.out.format("\n|%-20d|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|", this.product_id, this.product_name, this.product_weight,this.product_buying_price,this.product_selling_price, this.product_brand,this.product_dealer, this.product_quantity);
       System.out.print("\n+"+"-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) +"+" + "-".repeat(20) + "+" + "-".repeat(20) +"+"+ "-".repeat(20)  + "+" + "-".repeat(20) + "+");
       return "";
    }
}
