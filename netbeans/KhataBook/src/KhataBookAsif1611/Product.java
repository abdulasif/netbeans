/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package KhataBookAsif1611;



import java.io.IOException;
import java.util.Arrays;

/**
 *
 * @author bas200190
 */
public class Product {
    int product_id;
    String product_name;
    String product_weight;
    double product_buying_price;
    double product_selling_price;
    String product_brand;
    int product_quantity;
    
    Product(){
    
    }
    
    Product(int product_id,String product_name,String product_weight,double product_buying_price,double product_selling_price,String product_brand,int product_quantity){
       
      this.product_id = product_id;
      this.product_name = product_name;
      this.product_weight = product_weight;
      this.product_buying_price = product_buying_price;
      this.product_selling_price = product_selling_price;
      this.product_brand = product_brand;
      this.product_quantity = product_quantity;
    }
    
    public void addProduct(){
//        String get_product_name = null;
//        String get_product_weight  = null;
//        double get_product_buying_price  = 0;
//        double get_product_selling_price = 0;
//        String get_product_brand = null;
//        int get_product_quantity = 0;
        try{
        KhataBook.ob.kd.product_details = Arrays.copyOf(KhataBook.ob.kd.product_details,KhataBook.ob.kd.product_details.length+1 );
        System.out.println("Enter Product Name:");
        String get_product_name = KhataBook.ob.br.readLine();
        System.out.println("Enter Product Weight:");
        String get_product_weight = KhataBook.ob.br.readLine();
        System.out.println("Enter Product Buying Price:");
        double get_product_buying_price = Double.parseDouble(KhataBook.ob.br.readLine());
        System.out.println("Enter Product Selling Price:");
        double get_product_selling_price = Double.parseDouble(KhataBook.ob.br.readLine());
        System.out.println("Enter Product Brand:");
        String get_product_brand = KhataBook.ob.br.readLine();
        System.out.println("Enter Product Quantity:");
        int get_product_quantity = Integer.parseInt(KhataBook.ob.br.readLine());
        KhataBook.ob.kd.product_details[KhataBook.ob.kd.product_details.length-1] = new Product(KhataBook.ob.kd.product_details.length+500,get_product_name,get_product_weight,get_product_buying_price,get_product_selling_price,get_product_brand,get_product_quantity);
        }
        catch(IOException ioe){
            System.out.println("product not added");
        }
        
       
    }
    
    public void viewAllProducts(){
        Design.viewProductDesign();
        for (int i = 0; i < KhataBook.ob.kd.product_details.length; i++) {
            System.out.print(KhataBook.ob.kd.product_details[i]);
            System.out.print("\n+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");
            
        }
        System.out.println("");
        System.out.println("");
    }
    
    public void viewParticularProduct(){
        int vpp_product_id = 0;
        try{
        System.out.println("Enter the Product ID:");
        vpp_product_id = Integer.parseInt(KhataBook.ob.br.readLine());
        }
        catch(IOException ioe){
            
        }
        for (int i = 0; i < KhataBook.ob.kd.product_details.length; i++) {
            if (KhataBook.ob.kd.product_details[i].product_id==vpp_product_id) {
                Design.viewProductDesign();
                System.out.print(KhataBook.ob.kd.product_details[i]);
                System.out.print("\n+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+" + "-".repeat(20) + "+");
                break;
            }
            
        }
        System.out.println("");
        System.out.println("");
    }
    
    public void updateProduct(){
        
        try{
        System.out.println("Enter the Product ID:");
        int up_product_id = Integer.parseInt(KhataBook.ob.br.readLine());
        for (int i = 0; i < KhataBook.ob.kd.product_details.length; i++) {
            if (KhataBook.ob.kd.product_details[i].product_id == up_product_id) {
                System.out.println("*****Enter Product's New Details*****");
                System.out.println("Enter the Product Name:");
                KhataBook.ob.kd.product_details[i].product_name = KhataBook.ob.br.readLine();
                System.out.println("Enter Product Weight:");
                KhataBook.ob.kd.product_details[i].product_weight =KhataBook.ob.br.readLine();
                System.out.println("Enter Product Buying Price:");
                KhataBook.ob.kd.product_details[i].product_buying_price = Integer.parseInt(KhataBook.ob.br.readLine());
                System.out.println("Enter Product Selling Price:");
                KhataBook.ob.kd.product_details[i].product_selling_price = Integer.parseInt(KhataBook.ob.br.readLine());
                System.out.println("Enter Product Brand:");
                KhataBook.ob.kd.product_details[i].product_brand = KhataBook.ob.br.readLine();
                System.out.println("Enter Product Quantity:");
                KhataBook.ob.kd.product_details[i].product_quantity = Integer.parseInt(KhataBook.ob.br.readLine());

            }
        }
        }
        catch(IOException ioe){
        
        }
    }
    
    public void topUpProduct(){
        try{
        System.out.println("Enter the Product ID:");
        int up_product_id = Integer.parseInt(KhataBook.ob.br.readLine());
        for (int i = 0; i < KhataBook.ob.kd.product_details.length; i++) {
            if (KhataBook.ob.kd.product_details[i].product_id == up_product_id) {
                System.out.println("*****TOP UP PRODUCT*****");
                System.out.println("");
                System.out.println("Available Quantity: "+KhataBook.ob.kd.product_details[i].product_quantity);
                System.out.println();
                System.out.println("Enter Product Quantity:");
                KhataBook.ob.kd.product_details[i].product_quantity = KhataBook.ob.kd.product_details[i].product_quantity + Integer.parseInt(KhataBook.ob.br.readLine());

            }
        }
        }
        catch(IOException ioe){
        
        }
    }
    
    @Override
    public String toString(){
      return String.format("\n|%-20d|%-20s|%-20s|%-20s|%-20s|%-20s|%-20s|", this.product_id, this.product_name, this.product_weight,this.product_buying_price,this.product_selling_price, this.product_brand, this.product_quantity);
    }
  
}
