public class PangramChecker {

    public boolean isPangram(String input) {
        char[] alphabets = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
        String finalInput = input.toLowerCase();
        boolean output = false;
        out:for (int i = 0; i < alphabets.length; i++) {
            in:for (int j = 0; j < finalInput.length(); j++) {
                if (alphabets[i]==finalInput.charAt(j)) {
                    output = true;
                    break in;
                }
            }
            if (alphabets[i]!='z') {
                if (output) {
                    output = false;
                }
                else{
                  break out;
                }
            }
        }
        return output;
    }
    
    public static void main(String[] args) {
        PangramChecker ex1  = new PangramChecker();
        System.out.println(ex1.isPangram("The quick brown fox jumps over the lazy ."));
    }

}
