import java.util.Scanner;
public class NaveenCurrency {
    static String[] ones = {" ", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen",
        "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"};
     static String[] tens = {"", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};
    public static String Convert(int f){
        // number lesser than 20 (0 to 19)
        if(f<20){
            return ones[f];
        }
        //number lesser than 100 
        if(f<100){
        return tens[f/10]+ones[f%10];
        }
        if(f<1000){
            return ones[f/100]+"hundered"+Convert(f%100);
        }
        if(f<100000){
            return Convert(f/1000)+"thousand"+Convert(f%1000);
        }
       if(f<10000000){
           return Convert(f/100000)+"Lakh"+Convert(f%100000);
       }
        return Convert(f/10000000)+"crore"+Convert(f%10000000);
} 
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the number");
        int d=sc.nextInt();
        System.out.println(Convert(d));
    }
}
