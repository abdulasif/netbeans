/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package filelearning;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;

import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 *
 * @author bas200190
 */
public class FileClassDemo {
    public static void main2(String[] args) {
       //long epoch = System.currentTimeMillis()/1000;      //to get current epoch miilisec
        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        
        File pratheeban_file = new File("/home/bas200190/Documents/netbeans/Basics/manifest.mf");
        System.out.println("length : "+pratheeban_file.length());
    }

    public static void main(String[] args) throws IOException {
        File pratheeban_file = new File("/home/bas200190/Documents/Files/pratheeban.txt");
        pratheeban_file.createNewFile();

        File arun_file = new File("/home/bas200190/Documents/Files/arunkumar.txt");
        arun_file.createNewFile();

        File arunbawa_file = new File("/home/bas200190/Documents/Files/arunbawa.txt");
        arunbawa_file.createNewFile();

        File file_dir = new File("/home/bas200190/Documents/Files");        //list with (file name filter)
//        String[] list = temp_file_2_dir.list(new FilenameFilter() {
//            public boolean accept(File _1by1_file, String str) {
//                if (str.startsWith("pra")) {
//                    return true;
//                } else {
//                    return false;
//                }
//            }
//        });
//        System.out.println("#####list() with filenamefilter & prints files starts with name file#####");
//        for (String str : list) {
//            System.out.println(str);
//        }

//        File[] files = file_dir.listFiles();
//        for (File file : files) {
//            System.out.println(file);
//        }
        File[] files_with_filter = file_dir.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                if (file.getName().startsWith("file")) {
                    return true;
                } else {
                    return false;
                }
            }
        });
        System.out.println("####################################################################################");
        System.out.println("listFiles(FileFilter)");
        System.out.println("using FileFilter interface->accept method .  filter only file name started with 'file'");
        for (File fi : files_with_filter) {
            System.out.println(fi);
        }
        System.out.println("####################################################################################");

        File[] file_with_filter_arun = file_dir.listFiles(new FilenameFilter() {

            @Override
            public boolean accept(File file, String str) {
                if (str.startsWith("arun")) {
                    return true;
                } else {
                    return false;
                }
            }
        });

        System.out.println("####################################################################################");
        System.out.println("listFiles(FilenameFilter)");
        for (File fi : file_with_filter_arun) {
            System.out.println(fi);
        }
        System.out.println("####################################################################################");

        File[] file_roots = File.listRoots();
        System.out.println("####################################################################################");
        for (File fi_root : file_roots) {
            System.out.println(fi_root);
        }
        System.out.println("####################################################################################");

        File arun = new File("/home/bas200190/Documents/Files/arun");
        boolean arun_dir_created = arun.mkdir();
        System.out.println(arun_dir_created);

        File arun_kumar = new File("/home/bas200190/Documents/Files/arun/kumar/data");
        arun_kumar.mkdirs();

        File arun_kumar_file = new File("/home/bas200190/Documents/Files/arun/kumar/arun_kumar.txt");
        arun_kumar_file.createNewFile();

        File rename = new File("/home/bas200190/Documents/Files/arun/kumar/arun_kumar_pdkt.txt");
        arun_kumar_file.renameTo(rename);

        File rename1 = new File("/home/bas200190/Documents/Files/arun/kumar/arun_kumar_pdkt2.txt");
        rename.renameTo(rename1);

        //rename1.setExecutable(true,true);
        
        
        File set_last_mod = new File("/home/bas200190/Documents/Files/file4.txt");
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/YYYY  HH:mm");
        System.out.println(sdf.format(set_last_mod.lastModified()));
        
        
        
        set_last_mod.setLastModified(1669011265060l);
        System.out.println(sdf.format(set_last_mod.lastModified()));
        
        File f1 = new File("/home/bas200190/Documents/Files/file1.txt");
        System.out.println("f1 readable : "+f1.setReadable(true,true));
        System.out.println("f1 writable : "+f1.setWritable(true,true));

        System.out.println(f1);
        System.out.println(f1.toPath());
        System.out.println(f1.toURI());  //convert this into url not use below way
        System.out.println(f1.toURL());
        File important = new File("/home/bas200190/Downloads/.important");
        System.out.println("isHide : "+important.isHidden());
    }

    public static void main1(String[] args) {
        try {
            File f1 = new File("/home/bas200190/Documents/Files/file1.txt");     //constructor 1
            boolean createdf1 = f1.createNewFile();
            System.out.println("createdf1 : " + createdf1);

            File f2i = new File("/home/bas200190/Documents/Files");
            File f2ii = new File(f2i, "file2.txt");                               //constructor 2
            boolean createdf2 = f2ii.createNewFile();
            System.out.println("createdf2 : " + createdf2);

            File f3 = new File("/home/bas200190/Documents/Files", "file3.txt");    //constructor 3
            boolean createdf3 = f3.createNewFile();
            System.out.println("createdf3 : " + createdf3);

//            try{
//                URI uri = new URI("/home/bas200190/Documents/Files/file4.txt");  //constructor 4
//                File f4 = new File(uri);
//                boolean createdf4  =f4.createNewFile();
//            System.out.println("createdf4 : "+createdf4);
//            }catch(IllegalArgumentException iae){
//               iae.printStackTrace();
//            }catch(URISyntaxException urise ){
//                urise.getStackTrace();
//            }
            boolean exists = f1.exists();
            System.out.println("Exists : " + exists);

            boolean f1_can_execute = f1.canExecute();
            System.out.println("can execute : " + f1_can_execute);

            boolean f1_can_read = f1.canRead();
            System.out.println("can read : " + f1_can_read);

            System.out.println("can write : " + f1.canWrite());

            int compare = f2ii.compareTo(f3);
            System.out.println("compare f2 and f3 : " + compare);

            File temp_file = File.createTempFile("asif", "file");
            System.out.println("Temp File Exists : " + temp_file.exists());

            System.out.println("temp file path : " + temp_file.getPath());

            System.out.println("temp file getname : " + temp_file.getName());

            System.out.println("file 3 delete on exit : ");
            f3.deleteOnExit();

            temp_file.deleteOnExit();

            File temp_file_2_dir = new File("/home/bas200190/Documents/Files");
            File temp_file_2 = File.createTempFile("asif", ".txt", temp_file_2_dir);

            System.out.println("temp_file_2_exists : " + temp_file_2.exists());

            String get_path_of_temp_file_2 = temp_file_2.getPath();
            System.out.println("temp file 2 path : " + get_path_of_temp_file_2);

            System.out.println("temp file 2 getname : " + temp_file_2.getName());
            System.out.println("temp file 2 getParent : " + temp_file_2.getParent());

            temp_file_2.deleteOnExit();

            System.out.println("delete file 2 : " + f2ii.delete());
            System.out.println("f2 exists : " + f2ii.exists());

            File f4 = new File("/home/bas200190/Documents/Files/file4.txt");
            System.out.println("created f4 : " + f4.createNewFile());

            System.out.println("f4 getAbsolutePath : " + f4.getAbsolutePath());

            System.out.println("f4 getAbsoluteFile : " + f4.getAbsoluteFile());

            System.out.println("f4 getCanonicalPath : " + f4.getCanonicalPath());

            System.out.println("f4 getCanonicalFile : " + f4.getCanonicalFile());

            System.out.println("f4 canExecute :" + f4.canExecute());  //.exe

            System.out.println("f4 getFreeSpace : " + f4.getFreeSpace());

            System.out.println("f4 totalSpace : " + f4.getTotalSpace());

            System.out.println("f4 getUsableSpace : " + f4.getUsableSpace());

            System.out.println("f4 getParent : " + f4.getParentFile());

            System.out.println("f4 isAbsolute : " + f4.isAbsolute());

            System.out.println("f4 isFile :" + f4.isFile());

            System.out.println("f4 isDirecory : " + f4.isDirectory());

            System.out.println("f4 isHidden : " + f4.isHidden());

            File sample = new File("/home/bas200190/baggit/s8-java-basics/sample.txt");
            System.out.println("f4 lastModified : " + sample.lastModified());
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/YYYY  HH:mm");
            System.out.println(sdf.format(sample.lastModified()));

            System.out.println("sample file length : " + sample.length());

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
